package thesis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Vehicles.Conventional;
import Vehicles.EV;
import Vehicles.Occasion_EV;
import Vehicles.Occasion_conventional;
import Vehicles.vehicle;
import scenarios.scenario;
import kotlin.collections.EmptyList;
import repast.simphony.context.Context;
import repast.simphony.context.RepastElement;
import repast.simphony.context.space.grid.GridFactory;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.essentials.RepastEssentials;
import repast.simphony.parameter.Parameters;
import repast.simphony.query.space.grid.GridCell;
import repast.simphony.query.space.grid.GridCellNgh;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.space.grid.RandomGridAdder;
import repast.simphony.space.grid.SimpleGridAdder;
import repast.simphony.space.grid.StrictBorders;
import repast.simphony.space.grid.WrapAroundBorders;
import repast.simphony.ui.RSApplication;
import repast.simphony.ui.action.ResetRun;
import repast.simphony.util.SimUtilities;

/**
 * 
 * @author Dani Sibbel
 *
 */

public class Neighbourhood {
	Context<Object> context;
	Grid<Object> parkingspacesGrid;
	
	static double total_subsidy_occasion = 0;
	static double total_subsidy_lease = 0;
	static double total_subsidy_new = 0;
	
	/** User Panel values **/
	String scenario_string = UserPanel.scenario;
	double conversion_Rate_lp = UserPanel.conversion_Rate_lp;
	
	int added_loadPole = 0;
	
	static double gasoline_increase = 0;
	public static int year		  	= 0;
	
	static int nbr_tesla		= 0;
	static int nbr_volkswagen	= 0;
	static int nbr_renault		= 0;
	static int nbr_bmw			= 0;
	static int nbr_nissan		= 0;
	static int nbr_jaguar		= 0;
	static int nbr_toyota		= 0;
	
	int last_year_nbr_non_ev 	= 0;
	int initial_nbr_ev_owners	= 7;
	int evs			  			= 0;
	double old_EVs				= 7;
	double old_nbr_EV 			= 0;
	double new_nbr_EV 			= 0;
	
	double to_replace			= 0;
	double have_replaced_twice 	= 0;
	
	static List<Integer> list_ids = new ArrayList<Integer>();
	
	public static double need_random = 0;
	
	static List<Object> ev_buyers 						= new ArrayList<Object>();
	static List<Object> conventional_buyers 			= new ArrayList<Object>();
	
	static List<Object> occasion_bev_buyers				= new ArrayList<Object>();
	static List<Object> occasion_conventional_buyers	= new ArrayList<Object>();
	
	static List<Object> lease_bev_buyers				= new ArrayList<Object>();
	static List<Object> lease_conventional_buyers		= new ArrayList<Object>();
	
	static List<Object> buyers							= new ArrayList<Object>();
	
	static List<Double> happiness_BEV_over_time 		= new ArrayList<Double>();
	static List<Double> happiness_CI_over_time 			= new ArrayList<Double>();
	
	static List<Double> happines_CI_BEV 				= new ArrayList<Double>();
	
	int parkingSpaces 	= 130;//(Integer) params.getValue("parking_count");
	int loadPoles 		= 2;//(Integer) params.getValue("load_poles");

	static double used_subsidy_occasion 	= 0;
	static double used_subsidy_new 			= 0;
	static double used_subsidy_lease	 	= 0;
	
	/**
	 * Initialise Neighbourhood
	 * @param context
	 */
	public Neighbourhood(Context<Object> context) {
		String scenario_selected = null;
		if(scenario_string == " ")
			scenario_selected = "No scenario was selected";
		else
			scenario_selected = "The scenario selected is: " + scenario_string;
		
		if((UserPanel.multiple_scenarios).length <= 1){
			System.out.println("The model has been RESET, " + scenario_selected);
		}
		else{
			print_multiple_scenarios(UserPanel.multiple_scenarios);
		}
		
		reset_everything();
		this.context = context;

		GridFactory factory = GridFactoryFinder.createGridFactory(null);
		parkingspacesGrid = factory.createGrid("parkingspacesGrid", context, new GridBuilderParameters<Object>(
				new WrapAroundBorders(), new RandomGridAdder<Object>(), true, 50, 50));

		makeParkingSpots(context);
		makeHomes(context);
		buildNeighbourhood(context, parkingspacesGrid);
		moveIntoHomes(context);
	}

	/**
	 * Add the parking spotsto the grid
	 * @param context
	 */
	public void makeParkingSpots(Context<Object> context) {
		for (int i = 0; i < parkingSpaces - loadPoles; i++) {
			String type = "without_loadPole";
			boolean occupied = false;
			Parking_Space ps = new Parking_Space(parkingspacesGrid, type, occupied);
			context.add(ps);
		}
		
		for (int i = 0; i < loadPoles + 1; i++) {
			String type = "with_loadPole";
			boolean occupied = false;
			Parking_Space ps = new Parking_Space(parkingspacesGrid, type, occupied);
			context.add(ps);
		}	
	}
	
	/**
	 * Convert parking spots to LPs according to scenario  
	 */
	@ScheduledMethod(start = 1, interval = 4, priority = 2)
	public void MakeChargingStations() {
		//SCENARIO
		if(scenario.is_increase_lp_selelected()) {
			conversion_Rate_lp = scenario.lp_convergion_rate();
		}
		int total_evs = get_total_EV();
		int current_nbr_EVs = total_evs - initial_nbr_ev_owners;
		
		boolean new_evs_added 	= false;
		boolean reached_max_evs = false;
		boolean add_lp			= false;
		
		if(get_total_EV() - old_EVs != 0)
			add_lp = true;
		
		if(current_nbr_EVs > 0)	new_evs_added 	= true;
		
		if(total_evs - old_EVs >= conversion_Rate_lp)	reached_max_evs = true;		
		int nbr_loadpoles 	= get_nbr_loadpoles();
		
		if(reached_max_evs && new_evs_added && add_lp) {
			add_loadPole(current_nbr_EVs, nbr_loadpoles, 1);
			System.out.println("loadPole added " + (total_evs - old_EVs));
			add_lp = false;
			old_EVs = get_total_EV();
		}
		
	}
	
	/**
	 * Converts one random CS to LP
	 * @param cur_EVs
	 * @param nbr_loadpoles
	 * @param amount
	 */
	private void add_loadPole(int cur_EVs, int nbr_loadpoles, int amount) {

		for(Object prk : context.getObjects(Parking_Space.class)) {
			if(((Parking_Space) prk).getType().equals("without_loadPole") && (amount != 0)) {
				((Parking_Space) prk).setType("with_loadPole");
				amount --;
				added_loadPole ++;
			}
		}
	}
	
	/**
	 * Adds homes to the context
	 * @param context
	 */
	public void makeHomes(Context<Object> context) {
		int residences = context.getObjects(agents.class).size();
		for (int i = 0; i < residences + 1; i++) {
			String personality_type = "I don't know";
			boolean has_EV = false;
			Residence rsd = new Residence(parkingspacesGrid, personality_type, has_EV);
			context.add(rsd);
		}
	}
	
	/**
	 * Attributes each agent with a home
	 * @param context
	 */
	public void moveIntoHomes(Context<Object> context) {
		int f = 0;
		
		Iterable<Object> humans 			= context.getObjects(agents.class);
		Iterator<Object> humans_iterator 	= humans.iterator();
		Iterable<Object> houses 			= context.getObjects(Residence.class);
		Iterator<Object> houses_iterator 	= houses.iterator();
		
		while(humans_iterator.hasNext() ) {
			try {
				agents human 	= (agents) 		humans_iterator.next();
				Residence house = (Residence) 	houses_iterator.next();
				f += 1;
				house.moveIn(human);
			}
			catch(Exception e) {}
			finally {}
		}
	}

	/**
	 * Add houses residence and parking spaces to grid
	 * @param context
	 * @param parkingspacesGrid
	 */
	public void buildNeighbourhood(Context<Object> context, Grid<Object> parkingspacesGrid) {
	
		Iterable<Object> houses 			= context.getObjects(Residence.class);
		Iterator<Object> houses_iterator 	= houses.iterator();
		Iterable<Object> parking_spc 			= context.getObjects(Parking_Space.class);
		Iterator<Object> parking_spc_iterator 	= parking_spc.iterator();
		
		//Move houses to random location
		for (Object obj : context.getObjects(Residence.class)) {	
			GridPoint hs = parkingspacesGrid.getLocation(obj);
			parkingspacesGrid.moveTo(obj, (int) hs.getX(), (int) hs.getY());
		}
		
		//Move humans to random location
		for (Object obj : context.getObjects(agents.class)) {	
			GridPoint human = parkingspacesGrid.getLocation(obj);
			parkingspacesGrid.moveTo(obj, (int) human.getX(), (int) human.getY());
		}
		
		// Move parkingSpace to be around the house
		while(parking_spc_iterator.hasNext() && houses_iterator.hasNext()) {
			
			Parking_Space ps = (Parking_Space) parking_spc_iterator.next();
			Residence residence = (Residence) houses_iterator.next();
			
			// get the grid location of this House
			GridPoint hs = parkingspacesGrid.getLocation(residence);
		
			// use the GridCellNgh class to create GridCells for the surrounding houses
			GridCellNgh <GridPoint> nghCreator 				= new GridCellNgh <GridPoint>( parkingspacesGrid, hs, GridPoint.class, 1, 1);
			List <GridCell<GridPoint>> neighbouring_Cells 	= nghCreator.getNeighborhood(true);
			SimUtilities.shuffle (neighbouring_Cells, RandomHelper.getUniform ());
		
			for (GridCell<GridPoint> neighbour : neighbouring_Cells ) {
				Object spot = (Object) parkingspacesGrid.getObjectAt(neighbour.getPoint().getX(), neighbour.getPoint().getY());
				if(spot == null)
				{
					// If the neighbour is not full then place a parkingSpot on it
					parkingspacesGrid.moveTo(ps, (int) neighbour.getPoint().getX(), (int) neighbour.getPoint().getY());
				}
			}
		}
	}

	/**
	 * Keeps track of how many agents have replaced their vehicle
	 * @return
	 */
	public double replaced_vehicle() {
		double nbr = 0;
		for(Object a: context.getObjects(agents.class))
		{
			if(((agents) a).has_replaced_vehicle == true) {
				nbr++;
			}
		}
		nbr = 200 - nbr;
		return nbr;
	}
	
	/**
	 * Prints info in to the Console, Updates graphs in interface, 
	 */
	@ScheduledMethod(start = 1, interval = 1, priority = 1)
	public void newyear() {
		int current_year =  (int) (Calendar.getInstance().get(Calendar.YEAR) + Neighbourhood.year);
		if(RunEnvironment.getInstance().getCurrentSchedule().getTickCount() % 52 == 0) {
			reset_used_subsidy_each_year();
			update_vehicle_characteristics();
			year +=1;
			increase_gasoline_fuel_cost();
			EnvironmentBuilder.last_year_nbr_ev 				= ev_buyers.size() - EnvironmentBuilder.last_year_nbr_ev;
			EnvironmentBuilder.last_year_nbr_non_ev 			= conventional_buyers.size() - EnvironmentBuilder.last_year_nbr_non_ev;
			EnvironmentBuilder.last_year_nbr_occasion_bev 		= occasion_bev_buyers.size() - EnvironmentBuilder.last_year_nbr_occasion_bev;
			EnvironmentBuilder.last_year_nbr_occasion_non_ev 	= occasion_conventional_buyers.size() - EnvironmentBuilder.last_year_nbr_occasion_non_ev;
			EnvironmentBuilder.last_year_nbr_lease_bev 			= lease_bev_buyers.size() - EnvironmentBuilder.last_year_nbr_lease_bev;
			EnvironmentBuilder.last_year_nbr_lease_non_ev 		= lease_conventional_buyers.size() - EnvironmentBuilder.last_year_nbr_lease_non_ev;
			
			total_buyers();
			
			String s1 = String.format("Type:  %3s   %3s   %3s  %5s  %20s  %2s	%5s	%5s","BEV", "CI", "Total:", " " , "replaced twice",  have_replaced_twice, "loadPoles added:", added_loadPole);
			String s2 = String.format("New:   %3s   %3s   %3s  %3s  %17s  %10s	%5s	%5s",EnvironmentBuilder.last_year_nbr_ev, EnvironmentBuilder.last_year_nbr_non_ev, "BEV: " , get_total_EV(), "replaced" ,to_replace,  " nbr loadPoles: ", get_nbr_loadpoles());
			String s3 = String.format("Occ:   %3s   %3s   %3s  %5s  %20s  %7s	%5s	%5s",EnvironmentBuilder.last_year_nbr_occasion_bev, EnvironmentBuilder.last_year_nbr_occasion_non_ev, "CI: " ,  get_total_Conventional(), "not replaced",  (200 - to_replace), " non loadPoles: ", (context.getObjects(Parking_Space.class).size() - get_nbr_loadpoles()));
			String s4 = String.format("Lease:   %1s   %3s   %3s  %5s  %20s  %7s	%5s	%5s",EnvironmentBuilder.last_year_nbr_lease_bev, EnvironmentBuilder.last_year_nbr_lease_non_ev, " ", " ", " ", " ", " ", " ");
			
			System.out.println("--------------------------------------------------");
			System.out.println("Current year: " + current_year + " years past in simulation: " + year);
			System.out.println("--------------------------------------------------");
			System.out.println(s1);
			System.out.println(s2);
			System.out.println(s3);
			System.out.println(s4);
			added_loadPole = 0;
			
			EnvironmentBuilder.last_year_nbr_ev 				= ev_buyers.size();
			EnvironmentBuilder.last_year_nbr_non_ev 			= conventional_buyers.size();
			EnvironmentBuilder.last_year_nbr_occasion_bev 		= occasion_bev_buyers.size();
			EnvironmentBuilder.last_year_nbr_occasion_non_ev 	= occasion_conventional_buyers.size();
			EnvironmentBuilder.last_year_nbr_lease_bev 			= lease_bev_buyers.size();
			EnvironmentBuilder.last_year_nbr_lease_non_ev 		= lease_conventional_buyers.size();
			
			System.out.println(" ");
			
			if(RunEnvironment.getInstance().getCurrentSchedule().getTickCount() == 1092) {
				System.out.println(
						"lease: " + context.getObjects(Lease.class).size()+
						" occasion: " + context.getObjects(Occasion.class).size()+
						" new " + context.getObjects(Private_new.class).size()
						);
				RunEnvironment.getInstance().pauseRun();
			}
			
			if(have_replaced_twice > 50) {
				
				determine_agents_who_havent_replaced_vehicle();
				RunEnvironment.getInstance().pauseRun();}
				reset_chosen_vehicle_of_agents();
		}
	}
	
	/**
	 * Each year the fuel cost increases
	 */
	private void increase_gasoline_fuel_cost() {
		gasoline_increase = (double) UserPanel.increaseGasolineTax/100;
	}
	
	/**
	 * Increases the fuel cost in the environment according to the scenario
	 */
	public static void increase_gasoline_fuel_cost_from_scenario() {
		gasoline_increase = year * 0.2;
	}
	
	/**
	 * Updates the vehicle characteristics according to the technological scenario
	 */
	public void update_vehicle_characteristics() {
		if(scenario.is_technology_improvement_selected()) {
			if(scenario.is_range_technological_selected()) {
				for(Object v : context.getObjects(EV.class))
					((EV) v).updateRange(year);
				for(Object v : context.getObjects(Occasion_EV.class))
					((Occasion_EV) v).updateRange(year);
			}
			if(scenario.is_price_technological_selected()) {
				for(Object v : context.getObjects(EV.class)) {
					((EV) v).updatePrice(year);
				}
				for(Object v : context.getObjects(Occasion_EV.class))
					((Occasion_EV) v).updatePrice(year);
			}
		}
	}
	
	/**
	 * Determines the average age of vehicles
	 * @return average_age
	 */
	public double average_age_vehicles() {
		double average_age = 0;
		for(Object a : context.getObjects(agents.class)) {
			average_age = ((agents) a).car_age;
		}
		return average_age/context.getObjects(agents.class).size();
	}

	/**
	 * Determines the average parking happiness of agents
	 */
	public static void determine_average() {
		double average_bev = 0;
		double average_ci = 0;
		double amount_of_agents = (happiness_BEV_over_time.size() + happiness_CI_over_time.size());

		if(amount_of_agents >= 200) {
			for(Double i : happiness_BEV_over_time) {
				average_bev += i;
			}
			for(Double i : happiness_CI_over_time) {
				average_ci += i;
			}
			if(happines_CI_BEV.isEmpty()) {
				average_bev = average_bev/happiness_BEV_over_time.size();
				average_ci	= average_ci/happiness_CI_over_time.size();
			}
			else {
				average_bev = (average_bev/happiness_BEV_over_time.size() + happines_CI_BEV.get(1))/2;
				average_ci	= (average_ci/happiness_CI_over_time.size() + happines_CI_BEV.get(0))/2;
			}
			
		
			happines_CI_BEV = Arrays.asList(average_bev, average_ci);
		
			happiness_CI_over_time.removeAll(happiness_CI_over_time);
			happiness_BEV_over_time.removeAll(happiness_BEV_over_time);
		}
	}

	/**
	 * Updates the User Panel Values
	 */
	public void update_userPanelValues() {
		if(UserPanel.sliderChanged) {
		agents.subsidy_new 		= UserPanel.subsidyValueNew;
		agents.subsidy_occasion = UserPanel.subsidyValueOccasion;
		agents.mrb_value 		= UserPanel.mrbvalue;
		agents.bpm_value 		= UserPanel.bpmvalue;
		agents.diesel_tax 		= UserPanel.increaseDieselTax;
		agents.gasoline_tax 	= UserPanel.increaseGasolineTax;
		
		
		agents.UP_N_U_S = UserPanel.uncertainty_social/10;
		agents.UP_N_U_F = UserPanel.uncertainty_financial/10;
		agents.UP_N_U_I = UserPanel.uncertainty_infrastructure/10;
		
		agents.UP_N_S_S = UserPanel.satisfaction_social/10;
		agents.UP_N_S_F = UserPanel.satisfaction_financial/10;
		agents.UP_N_S_I = UserPanel.satisfaction_infrastructure/10;
		
		//LEASE
		agents.UP_L_U_S = UserPanel.lease_uncertainty_social/10;
		agents.UP_L_U_F = UserPanel.lease_uncertainty_financial/10;
		agents.UP_L_U_I = UserPanel.lease_uncertainty_infrastructure/10;
		
		agents.UP_L_S_S = UserPanel.lease_satisfaction_social/10;
		agents.UP_L_S_F = UserPanel.lease_satisfaction_financial/10;
		agents.UP_L_S_I = UserPanel.lease_satisfaction_infrastructure/10;
		
		//OCCASION
		agents.UP_O_U_S = UserPanel.occasion_uncertainty_social/10;
		agents.UP_O_U_F = UserPanel.occasion_uncertainty_financial/10;
		agents.UP_O_U_I = UserPanel.occasion_uncertainty_infrastructure/10;
			
		agents.UP_O_S_S	= UserPanel.occasion_satisfaction_social/10;
		agents.UP_O_S_F	= UserPanel.occasion_satisfaction_financial/10;
		agents.UP_O_S_I	= UserPanel.occasion_satisfaction_infrastructure/10;
		
		UserPanel.reset_sliderChanged();
		}
	}


	/*** BUYERS */
	
	public int ev_buyers() 						{	return ev_buyers.size()						- EnvironmentBuilder.last_year_nbr_ev;				}
	
	public int non_ev_buyers() 					{	return conventional_buyers.size() 			- EnvironmentBuilder.last_year_nbr_non_ev;			}
	
	public int occasion_ev_buyers() 			{	return occasion_bev_buyers.size() 			- EnvironmentBuilder.last_year_nbr_occasion_bev;	}
	
	public int occasion_non_ev_buyers() 		{	return occasion_conventional_buyers.size() 	- EnvironmentBuilder.last_year_nbr_occasion_non_ev;	}
	
	public int lease_ev()			 			{	return lease_bev_buyers.size() 				- EnvironmentBuilder.last_year_nbr_lease_bev;		}
	
	public int lease_non_ev() 					{	return lease_conventional_buyers.size() 	- EnvironmentBuilder.last_year_nbr_lease_non_ev;	}
	
	public void total_buyers() {
		List<Object> all_buyers = new ArrayList<Object>();
		
		all_buyers.addAll(ev_buyers);
		all_buyers.addAll(conventional_buyers);
		all_buyers.addAll(occasion_bev_buyers);
		all_buyers.addAll(occasion_conventional_buyers);
		all_buyers.addAll(lease_bev_buyers);
		all_buyers.addAll(lease_conventional_buyers);
	
		to_replace 			= detect_duplicates(all_buyers).get(0);
		have_replaced_twice = detect_duplicates(all_buyers).get(1);

	}
	
	
	/*** UNABLE BUYERS */
	
	public static List<Integer> detect_duplicates(List<Object> all_buyers) {

        Set<Integer> uniqueSet = new HashSet<Integer>();
        List<Integer> dupesList = new ArrayList<Integer>();
        for (Object a : all_buyers) {
            if (uniqueSet.contains(((agents) a).getId()))
                dupesList.add(((agents) a).getId());
            else {
                uniqueSet.add(((agents) a).getId());
            }
        }
        list_ids.addAll(uniqueSet);
        //need the ids which are not in the unique list
        return Arrays.asList(uniqueSet.size(), dupesList.size());
    }
	
	public List<Object> unable_agents(){
		List<Object> unable_agents = new ArrayList<Object>();
		for(Object agent: context.getObjects(agents.class)) {
			if(!list_ids.contains(((agents) agent).getId())){
				unable_agents.add(agent);
			} 
		}
		return unable_agents;
	}
	
	private void determine_agents_who_havent_replaced_vehicle() {
		List<Object> unable_agents = new ArrayList<Object>();
		for(Object a: context.getObjects(agents.class)) {
			if(!((agents) a).has_replaced_vehicle) {
				unable_agents.add(a);
			}	
		}
		System.out.println(unable_agents.size());
	}


	/*** ADD FUNCTIONS */
	

	public static void add_ev_buyers(agents agent) {
		ev_buyers.add(agent);
		buyers.add(agent);
	}
	
	public static void add_conventional_buyers(agents agent) {
		conventional_buyers.add(agent);
		buyers.add(agent);
	}
	
	public static void add_occasion_conventional_buyers(agents agent) {
		occasion_conventional_buyers.add(agent);
		buyers.add(agent);
	}
	
	public static void add_lease_ev_buyers(agents agent) {
		lease_bev_buyers.add(agent);
		buyers.add(agent);
	}
	
	public static void add_lease_conventional_buyers(agents agent) {
		lease_conventional_buyers.add(agent);
		buyers.add(agent);
	}
	
	public static void add_occasion_ev_buyers(agents agent) {
		occasion_bev_buyers.add(agent);
		buyers.add(agent);
	}
	
	public static void add_to_total_subsidy_lease(double subsidy_lease) {
		total_subsidy_lease += subsidy_lease;
		used_subsidy_lease += subsidy_lease;
	}

	public static void add_to_total_subsidy_new(double subsidy_new) {
		total_subsidy_new += subsidy_new;
		used_subsidy_new += subsidy_new;
	}

	public static void add_to_total_subsidy_occasion(double subsidy_occasion) {
		total_subsidy_occasion += subsidy_occasion;
		used_subsidy_occasion += subsidy_occasion;
	}


	/*** GET SUBSIY */
	
	public static double get_used_subsidy_occasions() {
		return used_subsidy_occasion;//get_occasion_ev_buyers() * agents.subsidy_occasion;
	}

	public static double get_used_subsidy_new() {
		return used_subsidy_new;//get_ev_buyers()* agents.subsidy_new;
	}
	
	public static double get_used_subsidy_lease() {
		return used_subsidy_lease;
	}

	
	/*** GET BUYERS */
	
	public static int get_ev_buyers() {
		return ev_buyers.size();
	}
	
	public static int get_non_ev_buyers() {
		return conventional_buyers.size();
	}
	
	public static int get_occasion_ev_buyers() {
		return occasion_bev_buyers.size();
	}
	
	public static int get_occasion_conventional_buyers() {
		return occasion_conventional_buyers.size();
	}
	
	public static int get_lease_ev_buyers() {
		return lease_bev_buyers.size();
	}
	
	public static int get_lease_conventional_buyers() {
		return lease_conventional_buyers.size();
	}

	
	/*** GET RATIOS AND PERCENTAGES */

	public double ratio(String type) {
		double occasion_BEV	= 0;
		double occasion_CI	= 0;
		double lease_BEV	= 0;
		double lease_CI		= 0;
		double new_BEV		= 0;
		double new_CI		= 0;
		
		for(Object a: context.getObjects(agents.class)) {
			if(a instanceof Occasion) {
				if(((agents) a).get_car_combustion_type().equals("Electric")) occasion_BEV ++;
				if(((agents) a).get_car_combustion_type().equals("Gasoline")) occasion_CI ++;
			}
			if(a instanceof Private_new) {
				if(((agents) a).get_car_combustion_type().equals("Electric")) new_BEV ++;
				if(((agents) a).get_car_combustion_type().equals("Gasoline")) new_CI ++;
			}
			if(a instanceof Lease) {
				if(((agents) a).get_car_combustion_type().equals("Electric")) lease_BEV ++;
				if(((agents) a).get_car_combustion_type().equals("Gasoline")) lease_CI ++;
			}
		}
		
		if(type.equals("Occasion_BEV"))	return occasion_BEV;
		if(type.equals("Occasion_CI"))	return occasion_CI;
		if(type.equals("New_BEV"))		return new_BEV;
		if(type.equals("New_CI"))		return new_CI;
		if(type.equals("Lease_BEV"))	return lease_BEV;
		if(type.equals("Lease_CI"))		return lease_CI;
		return 0;
	}
	
	public double ratio_occasion_BEV() {
		return ratio("Occasion_BEV");
	}
	
	public double ratio_occasion_CI() {
		return ratio("Occasion_CI");
	}
	
	public double ratio_new_BEV() {
		return ratio("New_BEV");
	}
	
	public double ratio_new_CI() {
		return ratio("New_CI");
	}
	
	public double ratio_lease_BEV() {
		return ratio("Lease_BEV");
	}
	
	public double ratio_lease_CI() {
		return ratio("Lease_CI");
	}
	
	public double perc_BEV_occ() {
		double occ = ratio("Occasion_BEV");
		double total_agents = 200;
		double perc = (occ/total_agents)*100;
		return perc;
	}
	
	public double perc_BEV_lease() {
		double lease = ratio("Lease_BEV");
		double total_agents = 200;
		double perc = (lease/total_agents)*100;
		return perc;
	}

	public double perc_BEV_new() {
		double newa = ratio("New_BEV");
		double total_agents = 200;
		double perc = (newa/total_agents)*100;
		return perc;
	}
	
	
	/****** GET PERCENTAGES ******/
	
	public double BEV_buyers_percentage()			{	
		double bev_buyers 		= ev_buyers.size();
		double ci_buyers 		= conventional_buyers.size();
		double occ_bev_buyers 	= occasion_bev_buyers.size();
		double occ_CI_buyers 	= occasion_conventional_buyers.size();
		double leasers_bev 		= lease_bev_buyers.size();
		double leasers_ci		= lease_conventional_buyers.size();
		double total_buyers		= bev_buyers + ci_buyers + occ_bev_buyers + occ_CI_buyers + leasers_bev + leasers_ci;
		if(total_buyers < 200)
			total_buyers = 200;
		double percentage_bev 	= (double)(bev_buyers/total_buyers);
		return percentage_bev;
	}
	
	public double CI_buyers_percentage()			{
		double bev_buyers 		= ev_buyers.size();
		double ci_buyers 		= conventional_buyers.size();
		double occ_bev_buyers 	= occasion_bev_buyers.size();
		double occ_CI_buyers 	= occasion_conventional_buyers.size();
		double leasers_bev 		= lease_bev_buyers.size();
		double leasers_ci		= lease_conventional_buyers.size();
		double total_buyers		= bev_buyers + ci_buyers + occ_bev_buyers + occ_CI_buyers + leasers_bev + leasers_ci;
		if(total_buyers < 200)
			total_buyers = 200;
		double percentage_ci 	= (double)(ci_buyers/total_buyers);
		return percentage_ci;
	}
	
	public double occasion_BEV_buyers_percentage()	{
		double bev_buyers 		= ev_buyers.size();
		double ci_buyers 		= conventional_buyers.size();
		double occ_bev_buyers 	= occasion_bev_buyers.size();
		double occ_CI_buyers 	= occasion_conventional_buyers.size();
		double leasers_bev 		= lease_bev_buyers.size();
		double leasers_ci		= lease_conventional_buyers.size();
		double total_buyers		= bev_buyers + ci_buyers + occ_bev_buyers + occ_CI_buyers + leasers_bev + leasers_ci;
		if(total_buyers < 200)
			total_buyers = 200;
		double percentage_occ_bev 	= (double)(occ_bev_buyers/total_buyers);
		return percentage_occ_bev;
	}
	
	public double occasion_CI_buyers_percentage()	{
		double bev_buyers 		= ev_buyers.size();
		double ci_buyers 		= conventional_buyers.size();
		double occ_bev_buyers 	= occasion_bev_buyers.size();
		double leasers_bev 		= lease_bev_buyers.size();
		double leasers_ci		= lease_conventional_buyers.size();
		double occ_CI_buyers 	= occasion_conventional_buyers.size();
		double total_buyers		= bev_buyers + ci_buyers + occ_bev_buyers + occ_CI_buyers + leasers_bev + leasers_ci;
		if(total_buyers < 200)
			total_buyers = 200;
		double percentage_occ_CI 	= (double)(occ_CI_buyers/total_buyers);
		return percentage_occ_CI;
	}
	
	public double lease_BEV_buyers_percentage()	{
		double bev_buyers 		= ev_buyers.size();
		double ci_buyers 		= conventional_buyers.size();
		double occ_bev_buyers 	= occasion_bev_buyers.size();
		double occ_CI_buyers 	= occasion_conventional_buyers.size();
		double leasers_bev 		= lease_bev_buyers.size();
		double leasers_ci		= lease_conventional_buyers.size();
		double total_buyers		= bev_buyers + ci_buyers + occ_bev_buyers + occ_CI_buyers + leasers_bev + leasers_ci;
		if(total_buyers < 200)
			total_buyers = 200;
		double percentage_lease_bev 	= (double)(leasers_bev/total_buyers);
		return percentage_lease_bev;
	}
	
	public double lease_CI_buyers_percentage()	{
		double bev_buyers 		= ev_buyers.size();
		double ci_buyers 		= conventional_buyers.size();
		double occ_bev_buyers 	= occasion_bev_buyers.size();
		double leasers_bev 		= lease_bev_buyers.size();
		double leasers_ci		= lease_conventional_buyers.size();
		double occ_CI_buyers 	= occasion_conventional_buyers.size();
		double total_buyers		= bev_buyers + ci_buyers + occ_bev_buyers + occ_CI_buyers + leasers_bev + leasers_ci;
		if(total_buyers < 200)
			total_buyers = 200;
		double percentage_lease_CI 	= (double)(leasers_ci/total_buyers);
		return percentage_lease_CI;
	}
	
	
	/****** GET AMOUNTS ******/
	
	public double amount_new_bev() {
		double bev_percentage = 0;
		for(Object o : context.getObjects(Private_new.class)) {
			if (((agents) o).get_car_combustion_type().equals("Electric"))
				bev_percentage +=1;
		}	
		return bev_percentage/context.getObjects(Private_new.class).size();
	}
	
	public double amount_new_CI() {
		double CI_percentage = 0;
		for(Object o : context.getObjects(Private_new.class)) {
			if (((agents) o).get_car_combustion_type().equals("Gasoline"))
				CI_percentage +=1;	
		}
		return CI_percentage/context.getObjects(Private_new.class).size();
	}
	
	public double amount_occasion_BEV() {
		double BEV_percentage = 0;
		for(Object o : context.getObjects(Occasion.class)) {
			if (((agents) o).get_car_combustion_type().equals("Electric"))
				BEV_percentage +=1;
		}
		return BEV_percentage/context.getObjects(Occasion.class).size();
	}
	
	public double amount_occasion_CI() {
		double CI_percentage = 0;
		for(Object o : context.getObjects(Occasion.class)) {
			if (((agents) o).get_car_combustion_type().equals("Gasoline"))
				CI_percentage +=1;
		}
		return CI_percentage/context.getObjects(Occasion.class).size();
	}
	
	public double amount_lease_BEV() {
		double CI_percentage = 0;
		for(Object o : context.getObjects(Lease.class)) {
			if (((agents) o).get_car_combustion_type().equals("Electric"))
				CI_percentage +=1;
		}
		return CI_percentage/context.getObjects(Lease.class).size();
	}
	
	public double amount_lease_CI() {
		double CI_percentage = 0;
		for(Object o : context.getObjects(Lease.class)) {
			if (((agents) o).get_car_combustion_type().equals("Gasoline"))
				CI_percentage +=1;
		}
		return CI_percentage/context.getObjects(Lease.class).size();
	}
	
	public static void add_bev_occasion_bought(vehicle chosen_vehicle) {
		if(chosen_vehicle.getBrand().equals("Tesla(model S)")) 		nbr_tesla++;
		if(chosen_vehicle.getBrand().equals("Volkswagen(e-Golf)"))	nbr_volkswagen++;
		if(chosen_vehicle.getBrand().equals("Renault(Zoe)"))		nbr_renault++;
		if(chosen_vehicle.getBrand().equals("BMW(i3)"))				nbr_bmw++;
		if(chosen_vehicle.getBrand().equals("Nissan(Leaf)"))		nbr_nissan++;
		if(chosen_vehicle.getBrand().equals("Jaguar(I-Pace)"))		nbr_jaguar++;
		if(chosen_vehicle.getBrand().equals("Toyota(iQ)"))			nbr_toyota++;
	}

	/*** GET AMOUNTS */
	
	public int get_nbr_loadpoles() {
		int lps = 0;
		for(Object prk : context.getObjects(Parking_Space.class)) {
		if(((Parking_Space) prk).getType().equals("with_loadPole"))
			lps ++;
		}
		return lps;
	}

	public double get_loadPoles() {
		int lp = 0;
		for(Object i : context.getObjects(Parking_Space.class)) {
			if(((Parking_Space) i).getType().equals("with_loadPole")) {
				lp++;
			}
		}
		return lp;
	}
	
	public double get_non_loadPoles() {
			int ps = 0;
			for(Object i : context.getObjects(Parking_Space.class)) {
				if(((Parking_Space) i).getType().equals("without_loadPole")) {
					ps++;
				}
			}
			return ps;
		}
	
	public int get_total_EV() {
		int ev = 0;
		for(Object obj : context.getObjects(agents.class)) {
			if (((agents) obj).car_combustion_type.equals("Electric")) {
				ev +=1;
			}
		}
		return ev;
	}
	
	public int get_total_Conventional() {
		int ev = 0;
		for(Object obj : context.getObjects(agents.class)) {
			if (((agents) obj).car_combustion_type.equals("Gasoline")) {
				ev +=1;
			}
		}
		return ev;
	}
	
	/**
	 * Returns the vehicles with the smallest age
	 * @return min_age
	 */
	private double get_smallest_car_year() {
		double min_age = Integer.MAX_VALUE;
		for(Object o : context.getObjects(vehicle.class)) {
			if(((vehicle) o) instanceof Occasion_conventional || ((vehicle) o) instanceof Occasion_EV)
			{
				double year_o = ((vehicle) o).getYear();
				if(year_o < min_age)
					min_age = year_o;
			}
		}
		return min_age;
	}
	
	
	/*** PRINT FUNCTIONS */
	
	private void print_out_subsidy() {
		if(scenario.is_subsidy_scenario_selected())
			System.out.println("Subsidy in working:"+ scenario_string + " new " + agents.subsidy_new + " occasion: " + agents.subsidy_occasion);
		if(scenario.is_MRB_BPM_scenario_selected())
			System.out.println("MRB and BPM scenario selected");
		if(scenario.is_increase_lp_selelected())
			System.out.println("Maximum loadPole addition selected");
	}
	
	public void print_multiple_scenarios(int[] multi_scenario) {
		System.out.println("The model has been RESET, the selected scenarios are: " );
		for(int i = 0; i < multi_scenario.length; i++) {
			if(multi_scenario[i] == 0) {	System.out.println("Implement limited amount of subsidy");	}
			if(multi_scenario[i] == 1) {	System.out.println("Gradually decreasing the BEV subsidy till 0 in 2025");	}
			if(multi_scenario[i] == 2) {	System.out.println("Increase taxes for Gasoline vehicles each year");	}
			if(multi_scenario[i] == 3) {	System.out.println("Do not abolish tax exemptions for BEVs");	}
			if(multi_scenario[i] == 4) {	System.out.println("Increase the rate at which CS are added");	}
			if(multi_scenario[i] == 5) {	System.out.println("Each year BEVs become cheaper");	}
			if(multi_scenario[i] == 6) {	System.out.println("Each year the range of BEVs increases");	}
		}
	}

	
	
	/*** RESET FUNCTIONS */
	
	public static void reset_buyers_lists_after_one_year() {
		conventional_buyers 			= null;
		occasion_conventional_buyers 	= null;
		occasion_bev_buyers 			= null;
		ev_buyers 						= null;
		lease_bev_buyers				= null;
		lease_conventional_buyers		= null;
	}
	
	public void reset_used_subsidy_each_year() {
		System.out.println(
				"Subsidy used occasion: " + 
				used_subsidy_occasion		+
				" new: " +
				used_subsidy_new + 
				" lease: " +
				used_subsidy_lease
				);
		used_subsidy_occasion 	= 0;
		used_subsidy_lease 		= 0;
		used_subsidy_new 		= 0;
	}
	
	private void reset_everything() {
		update_userPanelValues();
		
		to_replace			= 0;
		have_replaced_twice = 0;
		need_random 		= 0;
		year 				= 0;
		
		list_ids 						= new ArrayList<Integer>();
		ev_buyers 						= new ArrayList<Object>();
		conventional_buyers 			= new ArrayList<Object>();
		occasion_bev_buyers				= new ArrayList<Object>();
		occasion_conventional_buyers	= new ArrayList<Object>();
		lease_bev_buyers				= new ArrayList<Object>();
		lease_conventional_buyers		= new ArrayList<Object>();
		buyers							= new ArrayList<Object>();
		
		EnvironmentBuilder.last_year_nbr_non_ev 			= 0;
		EnvironmentBuilder.last_year_nbr_occasion_bev 		= 0;
		EnvironmentBuilder.last_year_nbr_occasion_non_ev 	= 0;
		EnvironmentBuilder.last_year_nbr_non_ev 			= 0;
	}

	public void reset_chosen_vehicle_of_agents() {
		for(Object obj : context.getObjects(agents.class)) {
			((agents) obj).a_chosen_vehicle = null;
		}
	}
}
