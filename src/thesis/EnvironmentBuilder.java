package thesis;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import Vehicles.EV;
import Vehicles.Occasion_EV;
import Vehicles.Occasion_conventional;
import Vehicles.vehicle;
import au.com.bytecode.opencsv.CSVWriter;
import Vehicles.Conventional;
import repast.simphony.context.Context;
import repast.simphony.context.space.grid.GridFactory;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.dataLoader.SubContextCreator;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.environment.RunListener;
import repast.simphony.parameter.Parameters;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.space.grid.RandomGridAdder;
import repast.simphony.space.grid.WrapAroundBorders;
import repast.simphony.ui.RSApplication;

/**
 * 
 * @author Dani Sibbel
 *
 */

public class EnvironmentBuilder implements ContextBuilder<Object> {

	String car_combustion_type 					= " ";
	String a_education 							= " ";
	String a_sex 								= " ";
	String car_ownership_type 					= " ";
	int a_income 								= 0;
	int days_working_outside_house				= 0;
	String household_composition				= null;
    String a_work_situation						= null;
    String a_prof_situation 					= null;
    String parking_home							= null;	
    String parking_at_work						= null;
    String a_satisfaction_commute				= null;
    String a_reason_to_go_with_car				= null;
    Boolean a_drivers_license 					= false;
    Boolean a_has_car 							= false;
    Boolean a_knowledge_BEV						= false;
    Boolean needs_charging						= true;
	int car_age 								= 0;
	int car_weight								= 0;
	float daily_distance						= 0.0f;
	int knowledge_EV							= 0;
	int knowledge_environmental					= 0;
	int participation_environmental				= 0;
	String EV_reason							= " ";
	String choice_without_f_aid					= " ";
	String EV_positive_surprise					= " ";
	String EV_negative_surprise					= " "; 
	String used_shared_car						= " ";
	Boolean a_priv_loadPole						= false;
	String a_happy_walk							= " ";
	String a_batt_suff							= " ";
	String a_insuf_batt							= " ";
	
	List<String> reason_electric				= null;
	List<String> overview_facilities_used 		= null;
	List<String> reason_to_use_shared_car		= null;
	List<String> reason_to_not_use_shared_car 	= null;
	List<String> neg_elec						= null;
	List<String> pos_elec						= null;
	List<String> work_incentives				= null;
	
	public static List<String> mrb_gasoline_list		= new ArrayList<String>();
	static List<String> mrb_diesel_list			= new ArrayList<String>();
	static List<String> mrb_LPG_list			= new ArrayList<String>();
	
	static List<Double> list_of_bev_ranges		= new ArrayList<Double>();
	
	GridPoint home_location;
	double happiness							= 0.5;
	int age										= 0;
	double random_nbr							= 0;
	double propensity_for_buying_bev			= 0;
	
	double nbr_evs_in_data						= 0;
	
	static double nbr_agents_in_simulation		= 0;
	
	//NEW
	double satisfaction_utility					= 0.5;	double uncertainty_utility			= 0.5;
	double satisfaction_financial				= 0.5;	double uncertainty_financial		= 0.5;
	double satisfaction_infrastructure			= 0.5; 	double uncertainty_infrastructure	= 0.5;
	double satisfaction_social					= 0.5;	double uncertainty_social			= 0.5;
	
	//	OCCASIONS
	double satisfaction_occasion_utility		= 0.5;	double uncertainty_occasion_utility			= 0.5;
	double uncertainty_occasion_financial 		= 0.5; 	double satisfaction_occasion_financial 		= 0.5;
	double uncertainty_occasion_infrastructure	= 0.5; 	double satisfaction_occasion_infrastructure = 0.5;
	double uncertainty_occasion_social 			= 0.5; 	double satisfaction_occasion_social 		= 0.5;
	
	//LEASE
	double uncertainty_financial_lease			= 0.5;	double satisfaction_financial_lease			= 0.5;

	
	double nbr_small_bev				= 0;
	double nbr_medium_bev				= 0;
	double nbr_large_bev				= 0;
	double nbr_executive_bev			= 0;
	double nbr_luxury_bev				= 0;
	double nbr_small_conventional		= 0;
	double nbr_medium_conventional		= 0;
	double nbr_large_conventional		= 0;
	double nbr_executive_conventional	= 0;
	double nbr_luxury_conventional		= 0;
	
	
	static List<Double> weights_list	= new ArrayList<Double>();
	static int the_nth_agent			= 0;
	Boolean not_company_or_shared_car 	= true; 

	static int last_year_nbr_ev 				= 0;
	static int last_year_nbr_non_ev 			= 0;
	static int last_year_nbr_occasion_bev 		= 0;
	static int last_year_nbr_occasion_non_ev 	= 0;
	static int last_year_nbr_lease_bev 			= 0;
	static int last_year_nbr_lease_non_ev 		= 0;
	
	Boolean has_replaced_vehicle = false;
	vehicle chosen_vehicle = null;
	int agent_nbr = 1;
	int occ_ev = 0;
	int lease_ev = 0;
	int new_ev = 0;
	
	
	/**
	 * Loads the agents, the vehicles, and initialises the neighbourhood.
	 * Also prints the characteristics
	 * @see {EnvironmentBuilder#initialise_csv_print_file}, @see {EnvironmentBuilder#load_MRB}, @see {EnvironmentBuilder#loadVehiclesFromCSV}, and @see {EnvironmentBuilder#loadAgentsFromCSV}
	 */
	@Override
	public Context build(Context<Object> context) {

		agent_nbr = 1;
		UserPanel up 		= new UserPanel();
		initialise_csv_print_file();
		load_MRB(context);
		loadVehiclesFromCSV(context);
		loadAgentsFromCSV(context);
		Neighbourhood nbh = new Neighbourhood(context);
		context.add(nbh);
		print_neighbourhood_characteristics(context);
		print_line(context);
		return context;
	}

	/**
	 * Prints the agents characteristics
	 */
	private void initialise_csv_print_file() {
		File file = new File("data/agents_R_evaluation.csv"); 
		try {  
	        FileWriter outputfile = new FileWriter(file);  
	        CSVWriter writer = new CSVWriter(outputfile); 
	        List<String[]> data = new ArrayList<String[]>(); 
	        data.add(new String[] { "ID", "Age", "Income", "Working days", "Education", "Household composition", "Sex", "Province", "Work situation", "professional situation", "Knowledge of BEV", "Parking at home",
	        		"Car combustion type", "Car age", "Car weight", "Distance travelled", "CO2_emissions_traveled", "CO2_emissions_per_km", "property_type", "satisfaction_commute", "reason_to_go_with_car",
	        		"knowledge_EV", "knowledge_environmental", "participation_environmental", "EV_reason", "choice_without_f_aid", "EV_positive_surprise", "EV_negative_surprise", "used_shared_car", "reason_to_use_shared_car",
	        		"reason_to_not_use_shared_car", "priv_loadPole", "happy_walk", "batt_suff", "insuf_batt", "overview_facilities_used", "pos_elec", "neg_elec", "EV_happiness", "nbr_peop_organisation",
	        		"work_incentives"}); 
	        writer.writeAll(data); 
	        writer.close(); 
	    } 
	    catch (IOException e) { 
	        e.printStackTrace(); 
	    } 
	}
	
	/**
	 * Prints the information of the environments in the Console
	 * @param context
	 */
	private void print_line(Context<Object> context) {
		List<Integer> bevs_in_each_segment = get_evs_in_each_segment(context);
		
		String s1 = String.format("Nbr Occasions:  		%1s			%1s		%1s  %9s  %3s  ", context.getObjects(Occasion.class).size()	, "BEV:",	bevs_in_each_segment.get(0), "CI:",context.getObjects(Occasion.class).size() - bevs_in_each_segment.get(0));
		String s3 = String.format("Nbr Lease:   		%10s		%12s	%9s  %9s  %3s  ", context.getObjects(Lease.class).size()		, "BEV:",	bevs_in_each_segment.get(1), "CI:",context.getObjects(Lease.class).size() - bevs_in_each_segment.get(1));
		String s5 = String.format("Nbr Private new:   	%10s		%12s	%9s  %9s  %3s  ", context.getObjects(Private_new.class).size(), "BEV:",	bevs_in_each_segment.get(2), "CI:",context.getObjects(Private_new.class).size() - bevs_in_each_segment.get(2));
		String s7 = String.format("Total:			   	%1s			%1s		%1s  %9s  %3s  ", context.getObjects(agents.class).size()	, "BEV:",	get_total_EV(context), "CI:",(context.getObjects(agents.class).size() - get_total_EV(context)));
		
		System.out.println("------------------------------------------------------------------------------------------------");
		System.out.println(s1);
		System.out.println("-------------------------------------------------------------------------------------------------");
		System.out.println(s3);
		System.out.println("-------------------------------------------------------------------------------------------------");
		System.out.println(s5);
		System.out.println("--------------------------------------------------------------------------------------------------");
		System.out.println(s7);
		System.out.println("--------------------------------------------------------------------------------------------------");
		
		double average_distance = 0;
		double max = Double.MIN_VALUE;
		for(Object a : context.getObjects(agents.class)) {
			double agent_distance = ((agents) a).distance_traveled;
			average_distance += agent_distance;
			if(agent_distance> max)
				max = agent_distance;
		}
		average_distance = average_distance/context.getObjects(agents.class).size();
	}
	
	/**
	 * Prints the Neighbourhood characteristics
	 * @param context
	 */
	public void print_neighbourhood_characteristics(Context<Object> context) {
		double nbr_high_income = 0;
		double nbr_above_average_income = 0;
		double nbr_average_income = 0;
		double nbr_below_average = 0;
		double average_income = 0;
		
		for(Object a : context.getObjects(Occasion.class)) {
			double income = ((Occasion) a).get_income();
			String fuel_vehicle = ((Occasion) a).get_car_combustion_type();
			average_income += income;
			if(income == 12500)
				nbr_below_average++;
			if(income == 24250)
				nbr_average_income++;
			if(income == 54000)
				nbr_above_average_income++;
			if(income == 80000)
				nbr_high_income++;
		}
		average_income = average_income/context.getObjects(Occasion.class).size();
		System.out.println("In the neighbourhood " +
				nbr_below_average + " have below average income, " +
				nbr_average_income + " have an average income, " +
				nbr_above_average_income + " have an above average income, and " + 
				nbr_high_income + " have an high income. The average agents have an average income of " +
				average_income + "."
				);
	}

	/**
	 * Returns the number of BEV owners within each segment: occasion, new, or lease
	 * @param context
	 * @return list(number occasion BEV, number lease BEV, number new BEV)
	 */
	private List<Integer> get_evs_in_each_segment(Context<Object> context){
		for(Object a : context.getObjects(agents.class)) {
			if((a instanceof Lease) &&  ((agents) a).car_combustion_type.equals("Electric")) {
				lease_ev++;
			}
			if((a instanceof Occasion) &&  ((agents) a).car_combustion_type.equals("Electric")) {
				occ_ev++;
			}
			if((a instanceof Private_new) &&  ((agents) a).car_combustion_type.equals("Electric")) {
				new_ev++;
			}
		}
		return Arrays.asList(occ_ev,  lease_ev, new_ev);
	}
	
	/**
	 * Initialises the each agent with a probability to acquire a vehicle
	 * @param context
	 * @param nbr
	 * @return list of weights
	 */
	private List<Double> loadWeightsForAgents(Context<Object> context, int nbr) {
		File values 			= new File("data/readyness_to_buy_vehicle.csv");
		String row 				= null;
		BufferedReader reader 	= null;
		int j = 0;
		try {
			reader = new BufferedReader(new FileReader(values));
			
			while ((row = reader.readLine()) != null && j != nbr) {
				weights_list = Arrays.asList( Double.parseDouble(row));
				j++;
				
			}
		}
		
		catch (IOException e)
		{
			System.out.println("hierzo gaat het fout!!!");
			e.printStackTrace();
		}
		finally {
			try {
				if (reader != null)	reader.close();
			}
			catch (IOException e) {System.out.println("hierzo gaat het fout!!!");}
		}
		return weights_list;
	}
	
	/**
	 * Extracts the MRB tax values from a text file
	 * @param context
	 */
	private void load_MRB(Context<Object> context) {
		File MRB_data_file 	= new File("data/MRB.csv");
		String row 				= null;
		BufferedReader reader 	= null;
		try {
			reader = new BufferedReader(new FileReader(MRB_data_file));
			List<String> labels = Arrays.asList(reader.readLine().split(","));
			//System.out.println(labels);
			
			while ((row = reader.readLine()) != null) {
				List<String> mrb_per_fuel = Arrays.asList(row.split(","));
				
				String mrb_gasoline = mrb_per_fuel.get(labels.indexOf("Gasoline"));
				String mrb_diesel 	= mrb_per_fuel.get(labels.indexOf("Diesel"));
				String mrb_LPG 	 	= mrb_per_fuel.get(labels.indexOf("LPG"));
				
				mrb_gasoline_list.add(mrb_gasoline);
				mrb_diesel_list.add(mrb_diesel);
				mrb_LPG_list.add(mrb_LPG);
			}
		}
		catch (IOException e)
		{
			System.out.println("hierzo gaat het fout!!!");
			e.printStackTrace();
		}
		finally {
			try {
				if (reader != null)	reader.close();
			}
			catch (IOException e) {System.out.println("hierzo gaat het fout!!!");}
		}
	}
	
	/**
	 * Loads the vehicles from the vehicles from the dataset
	 * @param context
	 */
	public void loadVehiclesFromCSV(Context<Object> context) 
	{
		File vehicles_data_file = new File("data/vehicles.csv");
		
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(vehicles_data_file));
			String row;
			
			List<String> labels = Arrays.asList(reader.readLine().split(";"));

			while ((row = reader.readLine()) != null) {
				List<String> vehicle_attributes = Arrays.asList(row.split(";"));
				String 	fuel_type 		= vehicle_attributes.get(labels.indexOf("Type"));
				String 	type 			= vehicle_attributes.get(labels.indexOf("Type"));
				String 	model 			= vehicle_attributes.get(labels.indexOf("Model"));
				String 	brand			= vehicle_attributes.get(labels.indexOf("Brand"));
				double 	price 			= Double.parseDouble(vehicle_attributes.get(labels.indexOf("Price")));
				double	range			= Double.parseDouble(vehicle_attributes.get(labels.indexOf("Range")));
				double 	emissions 		= Double.parseDouble(vehicle_attributes.get(labels.indexOf("Emissions")));
				double	km 				= Double.parseDouble(vehicle_attributes.get(labels.indexOf("Km")));
				int		year			= Integer.parseInt(vehicle_attributes.get(labels.indexOf("Year")));
				double  lease_price		= Double.parseDouble(vehicle_attributes.get(labels.indexOf("leasePrice")));

				if(fuel_type.equals("ELECTRIC_OCCASION")) {
					Occasion_EV occasion_electric_vehicle = new Occasion_EV(fuel_type, model, brand, year, price, km, emissions, range, type, lease_price);
					context.add(occasion_electric_vehicle);
				}

				if(fuel_type.equals("GASOLINE_OCCASION")) {
					Occasion_conventional occasion_conventional_vehicle = new Occasion_conventional(fuel_type, model, brand, year, price, km, emissions, range, type, lease_price);
					context.add(occasion_conventional_vehicle);
				}
				
				if (fuel_type.equals("ELECTRIC")) {
					int charging_time 	= Integer.parseInt(vehicle_attributes.get(labels.indexOf("ChargingTime")));
					EV electric_vehicle = new EV(fuel_type, model, brand, price, emissions, charging_time, 0, range, type, lease_price);
					context.add(electric_vehicle);
				}
				
				if (fuel_type.equals("GASOLINE")) {
					Conventional conventional = new Conventional(fuel_type, model, brand, price, emissions, 0, range, type, lease_price) ;
					context.add(conventional);
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally {
			try {
				if (reader != null)	reader.close();
			}
			catch (IOException e) {}
		}
		nbr_agents_in_simulation			= context.getObjects(agents.class).size();
	}

	/**
	 * Loads the agents by initialising them with data from the dataset
	 * @param context
	 */
	private void loadAgentsFromCSV(Context<Object> context) 
	{
		File agents_data_file 	= new File("data/DataAgents.csv");
		String row 				= null;
		BufferedReader reader 	= null;
		try {
			reader = new BufferedReader(new FileReader(agents_data_file));

			int i = 1;
			int current_nbr = 0;
			int total_nbr_agents = 200;
			int amount_ev = 0, amount_diesel = 0, amount_gasoline = 0, amount_phev = 0;
			
			List<String> labels = Arrays.asList(reader.readLine().split(";"));

			while ((row = reader.readLine()) != null) {

					List<String> agent_attributes = Arrays.asList(row.split(";"));
					
					// Skip the ones with incomplete data 
					if (agent_attributes.get(labels.indexOf("A5")).equals(" ") 			||
						agent_attributes.get(labels.indexOf("SA5")).equals(" ")			||
						agent_attributes.get(labels.indexOf("SA4")).equals("6")			|| 
						agent_attributes.get(labels.indexOf("Autokms2019")).equals(" ") ||
						agent_attributes.get(labels.indexOf("P1A1")).equals(" ")		||
						agent_attributes.get(labels.indexOf("P2")).equals(" ")			||
						agent_attributes.get(labels.indexOf("A5")).equals("5")			||
						agent_attributes.get(labels.indexOf("A5")).equals("6")			||
						agent_attributes.get(labels.indexOf("SA5")).equals("22")
							) continue;
					
					String dst = agent_attributes.get(labels.indexOf("Autokms2019"));
					String ems = agent_attributes.get(labels.indexOf("CO2KgWeek2019"));
					String emissions = agent_attributes.get(labels.indexOf("CO2perkm2019"));
					dst = dst.replace(',', '.');
					ems = ems.replace(',', '.');
					emissions = emissions.replace(',', '.');
					int knowledge_BEV = 2;
					
					/*
					 * Agent attributes
					 */
					int id						= Integer.parseInt(agent_attributes.get(labels.indexOf("﻿ID")));
					List<Double> weights		= loadWeightsForAgents(context, agent_nbr);
					    age 					= Integer.parseInt(agent_attributes.get(labels.indexOf("A2")));
					int sex 					= Integer.parseInt(agent_attributes.get(labels.indexOf("A3")));
					int education 				= Integer.parseInt(agent_attributes.get(labels.indexOf("A4")));
					int income 					= Integer.parseInt(agent_attributes.get(labels.indexOf("A5")));
					int household				= Integer.parseInt(agent_attributes.get(labels.indexOf("A6")));
					String province				= agent_attributes.get(labels.indexOf("WoonProvincie2019"));
				    int drivers_license			= Integer.parseInt(agent_attributes.get(labels.indexOf("A1A2")));
				    int has_car					= Integer.parseInt(agent_attributes.get(labels.indexOf("M1_1A1")));
				    float distance_traveled 	= Float.parseFloat(dst);

				    // Work attributes of the agent
				    int work_situation			= Integer.parseInt(agent_attributes.get(labels.indexOf("W1")));
				    int prof_sector				= Integer.parseInt(agent_attributes.get(labels.indexOf("W2")));	
				    
					// Parking facilities at home
				    int private_parking			= Integer.parseInt(agent_attributes.get(labels.indexOf("P1A1")));
				    int neighbourhood_parking	= Integer.parseInt(agent_attributes.get(labels.indexOf("P1A2")));
				    int somewhere_else_parking	= Integer.parseInt(agent_attributes.get(labels.indexOf("P1A3")));	
				    int neighbourhood_parking_$	= Integer.parseInt(agent_attributes.get(labels.indexOf("P1A4")));
				    int somewhere_else_parking_$= Integer.parseInt(agent_attributes.get(labels.indexOf("P1A5")));	
				    
					// Parking facilities at work
				    int at_work					= Integer.parseInt(agent_attributes.get(labels.indexOf("P2")));
				    int at_work_$				= Integer.parseInt(agent_attributes.get(labels.indexOf("P3")));
				    
				    /*
				     * Vehicle Attributes
				     */
					int bouwjaar			 	= Integer.parseInt(agent_attributes.get(labels.indexOf("SA5")));
					int gewichtsklasse			= Integer.parseInt(agent_attributes.get(labels.indexOf("SA4")));
					int fuelType 				= Integer.parseInt(agent_attributes.get(labels.indexOf("SA3")));
					int carType					= Integer.parseInt(agent_attributes.get(labels.indexOf("SA2")));
					float CO2_emissions_traveled= Float.parseFloat(ems);
					float CO2_emissions_per_km	= Float.parseFloat(emissions);
					
					
					if(agent_attributes.get(labels.indexOf("E1")).equals(" ") ) {
						if(fuelType == 6) 	knowledge_BEV = 1;
						else 				knowledge_BEV = 2;
					} 
					else knowledge_BEV			= Integer.parseInt(agent_attributes.get(labels.indexOf("E1")));
				
					int nbr_peop_organisation 	= -1;
					if (!agent_attributes.get(labels.indexOf("W7")).equals(" "))	nbr_peop_organisation 	= Integer.parseInt(agent_attributes.get(labels.indexOf("W7")));
					
					
					/*
					 * We will loop through each variable to see if they are not empty
					 */
					List<String>	stim_work	= Arrays.asList("W9A1", "W9A2", "W9A3", "W9A4", "W9A5", "W9A6");
					List<String> 	tactics 	= Arrays.asList("B5A6", "B5A7","B5A8","B5A9","B5A13","B5A14","B5A15","B5A16","B5A17");
					List<String> 	not_shared 	= Arrays.asList("AR5A1", "AR5A2","AR5A3","AR5A4","AR5A5","AR5A6","AR5A7","AR5A8","AR5A9","AR5A10","AR5A11","AR5A12");
					List<String> 	shared 		= Arrays.asList("AR8A1", "AR8A2","AR8A3","AR8A4","AR8A5","AR8A6","AR8A7","AR8A8","AR8A9","AR8A10");
					List<String> 	EV_reason	= Arrays.asList("V9A1", "V9A2", "V9A3", "V9A4", "V9A5", "V9A6");
					List<String> 	EV_pos		= Arrays.asList("V12A1", "V12A2", "V12A3", "V12A4", "V12A5", "V12A6", "V12A7", "V12A8", "V12A9", "V12A10", "V12A11");
					List<String> 	EV_neg		= Arrays.asList("V13A13", "V13A14", "V13A15", "V13A16", "V13A17", "V13A18", "V13A19", "V13A20", "V13A21", "V13A22");
					List<String> 	EV_fac		= Arrays.asList("E11_1", "E11_2", "E11_3", "E11_4", "E11_5");
					List<Integer>	work_stimuli= new ArrayList<Integer>();
					List<Integer>   work_days	= new ArrayList<Integer>();
					List<Integer> 	env_tact 	= new ArrayList<Integer>();
					List<Integer> 	re_not_sh 	= new ArrayList<Integer>();
					List<Integer> 	re_use_sh 	= new ArrayList<Integer>();
					List<Integer> 	reason_EV 	= new ArrayList<Integer>();
					List<Integer> 	pos_EV 		= new ArrayList<Integer>();
					List<Integer> 	neg_EV 		= new ArrayList<Integer>();
					List<Integer> 	fac_EV 		= new ArrayList<Integer>();

					int work_day = 0;
					List<Integer> 	at_workplace	= new ArrayList<Integer>();
					List<Integer> 	somewhere_else 	= new ArrayList<Integer>();
					
					int d = 0;
					List<String>	days_work_commute	= Arrays.asList("W4_1A1", "W4_1A4", "W4_2A1", "W4_2A4", "W4_3A1", "W4_3A4", "W4_4A1", "W4_4A4", "W4_5A1", "W4_5A4", "W4_6A1", "W4_6A4", "W4_7A1", "W4_7A4");
					for ( String days : days_work_commute){
						work_days.add(Integer.parseInt(agent_attributes.get(labels.indexOf(days))));
						
						if(d%2 == 0) {
							at_workplace.add(Integer.parseInt(agent_attributes.get(labels.indexOf(days))));
						}
						else 
							somewhere_else.add(Integer.parseInt(agent_attributes.get(labels.indexOf(days))));
						d +=1;
					}
					int t= 0;
					while(t < 7) {
						if(at_workplace.get(t) != somewhere_else.get(t) || (at_workplace.get(t) == somewhere_else.get(t) && somewhere_else.get(t) == 1))
							work_day+=1;
						t ++;
					}
					days_working_outside_house = work_day;
					
					int commute_days			= days_working_outside_house; 
				    daily_distance 				= distance_traveled/commute_days;
					
					/*
					 * The incentives someone gets from their work provider
					*/ 
					for ( String s : stim_work){
						if (!agent_attributes.get(labels.indexOf(s)).equals(" ")) {
							work_stimuli.add(Integer.parseInt(agent_attributes.get(labels.indexOf(s))));
						}  
						else work_stimuli.add(-1);
					}
					
					/*
					 * Agent Participation of arrangements
					 */
					for ( String s : tactics){
						if (!agent_attributes.get(labels.indexOf(s)).equals(" ")) {
							if (s == "B5A17" && Integer.parseInt(agent_attributes.get(labels.indexOf(s)))== 1) env_tact.add(1);	// omdat B5A17 geeft aan dat iemand nooit aan een environmental actie meedoet
							else env_tact.add(Integer.parseInt(agent_attributes.get(labels.indexOf(s))));
						}  
						else env_tact.add(0);
					}
					
					
					/*
					 *  Why someone would not use a shared car
					*/ 
					
					for ( String s : not_shared){
						if (!agent_attributes.get(labels.indexOf(s)).equals(" ")) re_not_sh.add(Integer.parseInt(agent_attributes.get(labels.indexOf(s))));  
						else re_not_sh.add(Integer.parseInt("-1"));
					}
					
					/*
					 *  Why someone would use a shared car
					 */
					for ( String s : shared){
						if (!agent_attributes.get(labels.indexOf(s)).equals(" ")) re_use_sh.add(Integer.parseInt(agent_attributes.get(labels.indexOf(s))));  
						else re_use_sh.add(Integer.parseInt("-1"));
					}

					/*
					 * Agent Motivations/Barriers
					 */
					int reason_commute			= Integer.parseInt(agent_attributes.get(labels.indexOf("C1")));
					int reason_to_go_with_car	= Integer.parseInt(agent_attributes.get(labels.indexOf("MB2_1")));
					
					/*
					 * Agent Knowledge of arrangements
					*/ 
					int knowledge_tactics_1		= Integer.parseInt(agent_attributes.get(labels.indexOf("B4A6")));
					int knowledge_tactics_2		= Integer.parseInt(agent_attributes.get(labels.indexOf("B4A7")));
					int knowledge_tactics_3		= Integer.parseInt(agent_attributes.get(labels.indexOf("B4A8")));
					int knowledge_tactics_4		= Integer.parseInt(agent_attributes.get(labels.indexOf("B4A9")));
					int knowledge_tactics_5		= Integer.parseInt(agent_attributes.get(labels.indexOf("B4A11")));
					int knowledge_tactics_6		= Integer.parseInt(agent_attributes.get(labels.indexOf("B4A13")));
					int knowledge_tactics_7		= Integer.parseInt(agent_attributes.get(labels.indexOf("B4A14")));
					int knowledge_tactics_8		= Integer.parseInt(agent_attributes.get(labels.indexOf("B4A15")));
					int knowledge_tactics_9		= Integer.parseInt(agent_attributes.get(labels.indexOf("B4A16")));
					int knowledge_tactics_10	= Integer.parseInt(agent_attributes.get(labels.indexOf("B4A17")));
					
					/*
					 *  EV_reason
					 */
					for ( String s : EV_reason){
						if (!agent_attributes.get(labels.indexOf(s)).equals(" ")) reason_EV.add(Integer.parseInt(agent_attributes.get(labels.indexOf(s))));  
						else reason_EV.add(Integer.parseInt("-1"));
					}
					
					/*
					 * EV owners happiness about electric driving
					 */
					
					int EV_happiness = 0; 
					if(agent_attributes.get(labels.indexOf("E7")).equals(" ") )  EV_happiness = 0;
					else  EV_happiness = Integer.parseInt(agent_attributes.get(labels.indexOf("E7")));

					/*
					 * Choice_without_F_aid_EV
					 */ 
					int choice_without_F	= 0;
					if(agent_attributes.get(labels.indexOf("V10")).equals(" ") )  choice_without_F = -1;
					else  choice_without_F = Integer.parseInt(agent_attributes.get(labels.indexOf("V10")));
					
					/*
					 * What EV owners where positively surprised about
					*/ 
					
					for ( String s : EV_pos){
						if (!agent_attributes.get(labels.indexOf(s)).equals(" ")) pos_EV.add(Integer.parseInt(agent_attributes.get(labels.indexOf(s))));  
						else pos_EV.add(Integer.parseInt("-1"));
					}
					
					/*
					 * What EV owners where negatively surprised about
					 */
					for ( String s : EV_neg){
						if (!agent_attributes.get(labels.indexOf(s)).equals(" ")) neg_EV.add(Integer.parseInt(agent_attributes.get(labels.indexOf(s))));  
						else neg_EV.add(Integer.parseInt("-1"));
					}
					
					/*
					 * Whether someone has ever used a shared car
					 */
					int used_shared_car_1	= Integer.parseInt(agent_attributes.get(labels.indexOf("AR4A1")));
					int used_shared_car_2	= Integer.parseInt(agent_attributes.get(labels.indexOf("AR4A2")));
					int used_shared_car_3	= Integer.parseInt(agent_attributes.get(labels.indexOf("AR4A3")));
					
					/*
					 * Ev variables exclusively
					 */
					
					int priv_loadPole = 0;
					if(agent_attributes.get(labels.indexOf("E8")).equals(" ") )  priv_loadPole = -1;
					else  priv_loadPole = Integer.parseInt(agent_attributes.get(labels.indexOf("E8")));
					
					// How long they have to walk to the nearest loadPole
					int walk_loadPole = 0;
					if(agent_attributes.get(labels.indexOf("E9")).equals(" ") )  walk_loadPole = -1;
					else  priv_loadPole = Integer.parseInt(agent_attributes.get(labels.indexOf("E9")));
					
					// Happiness with this distance
					int happy_walk = 0;
					if(agent_attributes.get(labels.indexOf("E9")).equals(" ") )  happy_walk = -1;
					else  happy_walk = Integer.parseInt(agent_attributes.get(labels.indexOf("E10")));
					
					// Battery is sufficient
					int batt_suf = 0;
					if(agent_attributes.get(labels.indexOf("E12")).equals(" ") )  batt_suf = -1;
					else  batt_suf = Integer.parseInt(agent_attributes.get(labels.indexOf("E12")));
					
					// Fear for insufficient battery
					int fear_insuf_batt = 0;
					if(agent_attributes.get(labels.indexOf("E13")).equals(" ") )  fear_insuf_batt = -1;
					else  fear_insuf_batt = Integer.parseInt(agent_attributes.get(labels.indexOf("E13")));

					
					// Facilities used by EV owners
					for ( String s : EV_fac){
						if (!agent_attributes.get(labels.indexOf(s)).equals(" ")) fac_EV.add(Integer.parseInt(agent_attributes.get(labels.indexOf(s))));  
						else fac_EV.add(Integer.parseInt("-1"));
					}
					
					
					// Convert the variables into the right type
					convert_sex(sex);
					convert_education(education);
					convert_income(income);
					convert_household(household);
					convert_car_age(bouwjaar);
					convert_car_weight(gewichtsklasse);
					convert_car_combustion_type(fuelType);
					convert_car_ownership_type(carType);
					convert_work_situation(work_situation);
					convert_prof_sector(prof_sector);
					convert_nbr_peop_organisation(nbr_peop_organisation);
					convert_stim_work_provider(work_stimuli);
					convert_drivers_license(drivers_license);
					convert_has_car(has_car);
					convert_knowledge_BEV(knowledge_BEV);
					convert_home_parking(private_parking, neighbourhood_parking, neighbourhood_parking_$, somewhere_else_parking, somewhere_else_parking_$);
					convert_parking_at_work(at_work, at_work_$);
					convert_satisfaction_commute(reason_commute);
					convert_reason_to_go_with_car(reason_to_go_with_car);
					convert_knowledge_EV_regulations(knowledge_tactics_7,knowledge_tactics_8,knowledge_tactics_9);
					convert_knowledge_environmental(knowledge_tactics_1,knowledge_tactics_2,knowledge_tactics_3, knowledge_tactics_4, knowledge_tactics_5, knowledge_tactics_6, knowledge_tactics_10);
					convert_choice_without_f(choice_without_F);
					convert_used_shared_car(used_shared_car_1, used_shared_car_2, used_shared_car_3);
					convert_priv_loadPole(priv_loadPole);
					convert_happy_walk(happy_walk);
					convert_batt_suf(batt_suf);
					convert_fear_insuf_batt(fear_insuf_batt);
					
					work_incentives				= convert_stim_work_provider(work_stimuli);
					overview_facilities_used 	= conver_fac_EV(fac_EV);
					reason_to_use_shared_car 	= convert_reason_to_use_shared_car(re_use_sh);
					reason_to_not_use_shared_car= convert_reason_not_to_use_shared_car(re_not_sh);
					reason_electric				= convert_EV_reason(reason_EV);
					neg_elec					= convert_EV_positive_surprise(pos_EV);
					pos_elec					= convert_EV_negative_surprise(neg_EV);
					propensity_for_buying_bev = compute_propensity();
					convert_participation_environmental(env_tact);
				
					// Makes sure that only private, private lease and company lease are included in the model
					if (car_ownership_type == "shared_car" || car_ownership_type == "company_car" || car_ownership_type == " " || car_ownership_type == "company_lease") {
						not_company_or_shared_car = false;
					}
					else not_company_or_shared_car = true;

					if(not_company_or_shared_car && car_combustion_type.equals("Gasoline") && amount_gasoline < 0.965 * total_nbr_agents) {
						add_agents(context,
								id,
								weights_list,
								age,
								a_income,
								days_working_outside_house,
								a_education,
								household_composition,
								a_sex,
								province,
								a_work_situation,
								a_prof_situation,
								a_knowledge_BEV,
								parking_home,
								parking_at_work,
								car_combustion_type,
								car_age,
								car_weight,
								distance_traveled,
								CO2_emissions_traveled,
								CO2_emissions_per_km,
								car_ownership_type,
								
								a_satisfaction_commute,
								a_reason_to_go_with_car,
								knowledge_EV,
								knowledge_environmental,
								participation_environmental,
								reason_electric,
								choice_without_f_aid,
								EV_positive_surprise,
								EV_negative_surprise,
								used_shared_car,
								reason_to_use_shared_car,
								reason_to_not_use_shared_car,
								a_priv_loadPole,
								a_happy_walk,
								a_batt_suff,
								a_insuf_batt,
								overview_facilities_used,
								pos_elec,
								neg_elec,
								EV_happiness,
								nbr_peop_organisation,
								work_incentives,
								needs_charging,
								home_location,
								happiness,
								propensity_for_buying_bev,
								satisfaction_utility,
								uncertainty_utility,
								satisfaction_financial,
								uncertainty_financial,
								satisfaction_infrastructure, 
								uncertainty_infrastructure,
								satisfaction_social,
								uncertainty_social,
								
								satisfaction_occasion_utility,
								uncertainty_occasion_utility,
								satisfaction_occasion_financial,
								uncertainty_occasion_financial,
								satisfaction_occasion_infrastructure, 
								uncertainty_occasion_infrastructure,
								satisfaction_occasion_social,
								uncertainty_occasion_social,
								satisfaction_financial_lease,
								uncertainty_financial_lease,
								has_replaced_vehicle,
								chosen_vehicle
								);
						amount_gasoline +=1;
					/*add_agent_to_file(context, id, weights_list, age, a_income, days_working_outside_house, a_education,
							household_composition, a_sex, province, a_work_situation, a_prof_situation, a_knowledge_BEV,
							parking_home, parking_at_work, car_combustion_type, car_age, car_weight, distance_traveled,
							CO2_emissions_traveled, CO2_emissions_per_km, car_ownership_type, a_satisfaction_commute,
							a_reason_to_go_with_car, knowledge_EV, knowledge_environmental, participation_environmental,
							reason_electric, choice_without_f_aid, EV_positive_surprise, EV_negative_surprise,
							used_shared_car, reason_to_use_shared_car, reason_to_not_use_shared_car, a_priv_loadPole,
							a_happy_walk, a_batt_suff, a_insuf_batt, overview_facilities_used, pos_elec, neg_elec,
							EV_happiness, nbr_peop_organisation, work_incentives, needs_charging, home_location,
							happiness, propensity_for_buying_bev, satisfaction_utility, uncertainty_utility,
							satisfaction_financial, uncertainty_financial, satisfaction_infrastructure,
							uncertainty_infrastructure, satisfaction_social, uncertainty_social,
							satisfaction_occasion_utility, uncertainty_occasion_utility,
							satisfaction_occasion_financial, uncertainty_occasion_financial,
							satisfaction_occasion_infrastructure, uncertainty_occasion_infrastructure,
							satisfaction_occasion_social, uncertainty_occasion_social, satisfaction_financial_lease,
							uncertainty_financial_lease, has_replaced_vehicle, chosen_vehicle);*/
						}
						if(not_company_or_shared_car && car_combustion_type.equals("Electric") && amount_ev < 0.035 * total_nbr_agents ) {
							add_agents(context,
									id,
									weights_list,
									age,
									a_income,
									days_working_outside_house,
									a_education,
									household_composition,
									a_sex,
									province,
									a_work_situation,
									a_prof_situation,
									a_knowledge_BEV,
									parking_home,
									parking_at_work,
									car_combustion_type,
									car_age,
									car_weight,
									distance_traveled,
									CO2_emissions_traveled,
									CO2_emissions_per_km,
									car_ownership_type,
									
									a_satisfaction_commute,
									a_reason_to_go_with_car,
									knowledge_EV,
									knowledge_environmental,
									participation_environmental,
									reason_electric,
									choice_without_f_aid,
									EV_positive_surprise,
									EV_negative_surprise,
									used_shared_car,
									reason_to_use_shared_car,
									reason_to_not_use_shared_car,
									a_priv_loadPole,
									a_happy_walk,
									a_batt_suff,
									a_insuf_batt,
									overview_facilities_used,
									pos_elec,
									neg_elec,
									
									EV_happiness,
									nbr_peop_organisation,
									work_incentives,
									needs_charging,
									home_location,
									happiness,
									propensity_for_buying_bev,
									
									satisfaction_utility,
									uncertainty_utility,
									satisfaction_financial,
									uncertainty_financial,
									satisfaction_infrastructure, 
									uncertainty_infrastructure,
									satisfaction_social,
									uncertainty_social,
									
									satisfaction_occasion_utility,
									uncertainty_occasion_utility,
									satisfaction_occasion_financial,
									uncertainty_occasion_financial,
									satisfaction_occasion_infrastructure, 
									uncertainty_occasion_infrastructure,
									satisfaction_occasion_social,
									uncertainty_occasion_social,
									satisfaction_financial_lease,
									uncertainty_financial_lease,
									has_replaced_vehicle,
									chosen_vehicle
									);
							amount_ev+=1;
							/*add_agent_to_file(context, id, weights_list, age, a_income, days_working_outside_house, a_education,
									household_composition, a_sex, province, a_work_situation, a_prof_situation, a_knowledge_BEV,
									parking_home, parking_at_work, car_combustion_type, car_age, car_weight, distance_traveled,
									CO2_emissions_traveled, CO2_emissions_per_km, car_ownership_type, a_satisfaction_commute,
									a_reason_to_go_with_car, knowledge_EV, knowledge_environmental, participation_environmental,
									reason_electric, choice_without_f_aid, EV_positive_surprise, EV_negative_surprise,
									used_shared_car, reason_to_use_shared_car, reason_to_not_use_shared_car, a_priv_loadPole,
									a_happy_walk, a_batt_suff, a_insuf_batt, overview_facilities_used, pos_elec, neg_elec,
									EV_happiness, nbr_peop_organisation, work_incentives, needs_charging, home_location,
									happiness, propensity_for_buying_bev, satisfaction_utility, uncertainty_utility,
									satisfaction_financial, uncertainty_financial, satisfaction_infrastructure,
									uncertainty_infrastructure, satisfaction_social, uncertainty_social,
									satisfaction_occasion_utility, uncertainty_occasion_utility,
									satisfaction_occasion_financial, uncertainty_occasion_financial,
									satisfaction_occasion_infrastructure, uncertainty_occasion_infrastructure,
									satisfaction_occasion_social, uncertainty_occasion_social, satisfaction_financial_lease,
									uncertainty_financial_lease, has_replaced_vehicle, chosen_vehicle);*/
							
						}
						double p = compute_propensity();
						random_nbr += p;
						agent_nbr ++;
			}
		}
		catch (IOException e)
		{
			System.out.println("hierzo gaat het fout!!!");
			e.printStackTrace();
		}
		finally {
			try {
				if (reader != null)	reader.close();
			}
			catch (IOException e) {System.out.println("hierzo gaat het fout!!!");}
		}
	}
	
	/**
	 * Adds the agents to the contexts with the initialised parameters from data
	 * @param context
	 * @param id
	 * @param weights_list
	 * @param age
	 * @param a_income
	 * @param days_working_outside_house
	 * @param a_education
	 * @param household_composition
	 * @param a_sex
	 * @param province
	 * @param a_work_situation
	 * @param a_prof_situation
	 * @param a_knowledge_BEV
	 * @param parking_home
	 * @param parking_at_work
	 * @param car_combustion_type
	 * @param car_age
	 * @param car_weight
	 * @param distance_traveled
	 * @param CO2_emissions_traveled
	 * @param CO2_emissions_per_km
	 * @param car_ownership_type
	 * @param a_satisfaction_commute
	 * @param a_reason_to_go_with_car
	 * @param knowledge_EV
	 * @param knowledge_environmental
	 * @param participation_environmental
	 * @param reason_electric
	 * @param choice_without_f_aid
	 * @param EV_positive_surprise
	 * @param EV_negative_surprise
	 * @param used_shared_car
	 * @param reason_to_use_shared_car
	 * @param reason_to_not_use_shared_car
	 * @param a_priv_loadPole
	 * @param a_happy_walk
	 * @param a_batt_suff
	 * @param a_insuf_batt
	 * @param overview_facilities_used
	 * @param pos_elec
	 * @param neg_elec
	 * @param EV_happiness
	 * @param nbr_peop_organisation
	 * @param work_incentives
	 * @param needs_charging
	 * @param home_location
	 * @param happiness
	 * @param propensity_for_buying_bev
	 * @param satisfaction_utility
	 * @param uncertainty_utility
	 * @param satisfaction_financial
	 * @param uncertainty_financial
	 * @param satisfaction_infrastructure
	 * @param uncertainty_infrastructure
	 * @param satisfaction_social
	 * @param uncertainty_social
	 * @param satisfaction_occasion_utility
	 * @param uncertainty_occasion_utility
	 * @param satisfaction_occasion_financial
	 * @param uncertainty_occasion_financial
	 * @param satisfaction_occasion_infrastructure
	 * @param uncertainty_occasion_infrastructure
	 * @param satisfaction_occasion_social
	 * @param uncertainty_occasion_social
	 * @param satisfaction_financial_lease
	 * @param uncertainty_financial_lease
	 * @param has_replaced_vehicle
	 * @param chosen_vehicle
	 */
	public void add_agents(Context<Object>	context,
			int	id,
			List<Double> weights_list,
			int age,
			int a_income,
			int days_working_outside_house,
			String a_education,
			String household_composition,
			String a_sex,
			String province,
			String a_work_situation,
			String a_prof_situation,
			Boolean a_knowledge_BEV,
			String parking_home,
			String parking_at_work,
			String car_combustion_type,
			int car_age,
			int car_weight,
			float distance_traveled,
			float CO2_emissions_traveled,
			float CO2_emissions_per_km,
			String car_ownership_type,
			
			String a_satisfaction_commute,
			String a_reason_to_go_with_car,
			int knowledge_EV,
			int knowledge_environmental,
			int participation_environmental,
			List<String> reason_electric,
			String choice_without_f_aid,
			String EV_positive_surprise,
			String EV_negative_surprise,
			String used_shared_car,
			List<String> reason_to_use_shared_car,
			List<String> reason_to_not_use_shared_car,
			Boolean a_priv_loadPole,
			String a_happy_walk,
			String a_batt_suff,
			String a_insuf_batt,
			List<String> overview_facilities_used,
			List<String> pos_elec,
			List<String> neg_elec,
			
			int EV_happiness,
			int nbr_peop_organisation,
			List<String> work_incentives,
			Boolean needs_charging,
			GridPoint home_location,
			double happiness,
			double propensity_for_buying_bev,
			
			double satisfaction_utility,
			double uncertainty_utility,
			double satisfaction_financial,
			double uncertainty_financial,
			double satisfaction_infrastructure, 
			double uncertainty_infrastructure,
			double satisfaction_social,
			double uncertainty_social,
			
			double satisfaction_occasion_utility,
			double uncertainty_occasion_utility,
			double satisfaction_occasion_financial,
			double uncertainty_occasion_financial,
			double satisfaction_occasion_infrastructure, 
			double uncertainty_occasion_infrastructure,
			double satisfaction_occasion_social,
			double uncertainty_occasion_social,
			double satisfaction_financial_lease,
			double uncertainty_financial_lease,
			Boolean has_replaced_vehicle,
			vehicle chosen_vehicle) {
		
		if(car_ownership_type == "private_lease" ) {
			context.add(new Lease(
					context,
					id,
					weights_list,
					age,
					a_income,
					days_working_outside_house,
					a_education,
					household_composition,
					a_sex,
					province,
					a_work_situation,
					a_prof_situation,
					a_knowledge_BEV,
					parking_home,
					parking_at_work,
					car_combustion_type,
					car_age,
					car_weight,
					distance_traveled,
					CO2_emissions_traveled,
					CO2_emissions_per_km,
					car_ownership_type,
					
					a_satisfaction_commute,
					a_reason_to_go_with_car,
					knowledge_EV,
					knowledge_environmental,
					participation_environmental,
					reason_electric,
					choice_without_f_aid,
					EV_positive_surprise,
					EV_negative_surprise,
					used_shared_car,
					reason_to_use_shared_car,
					reason_to_not_use_shared_car,
					a_priv_loadPole,
					a_happy_walk,
					a_batt_suff,
					a_insuf_batt,
					overview_facilities_used,
					pos_elec,
					neg_elec,
					
					EV_happiness,
					nbr_peop_organisation,
					work_incentives,
					needs_charging,
					home_location,
					happiness,
					propensity_for_buying_bev,
					
					satisfaction_utility,
					uncertainty_utility,
					satisfaction_financial,
					uncertainty_financial,
					satisfaction_infrastructure, 
					uncertainty_infrastructure,
					satisfaction_social,
					uncertainty_social,
					satisfaction_occasion_utility,
					uncertainty_occasion_utility,
					satisfaction_occasion_financial,
					uncertainty_occasion_financial,
					satisfaction_occasion_infrastructure, 
					uncertainty_occasion_infrastructure,
					satisfaction_occasion_social,
					uncertainty_occasion_social,
					satisfaction_financial_lease,
					uncertainty_financial_lease,
					has_replaced_vehicle,
					chosen_vehicle
					));
		}
		if(car_ownership_type == "private_new") {
			if(car_age > 10 && this.a_income < 54001) {
				context.add(new Occasion(
						context,
						id,
						weights_list,
						age,
						a_income,
						days_working_outside_house,
						a_education,
						household_composition,
						a_sex,
						province,
						a_work_situation,
						a_prof_situation,
						a_knowledge_BEV,
						parking_home,
						parking_at_work,
						car_combustion_type,
						car_age,
						car_weight,
						distance_traveled,
						CO2_emissions_traveled,
						CO2_emissions_per_km,
						"occasion",
						
						a_satisfaction_commute,
						a_reason_to_go_with_car,
						knowledge_EV,
						knowledge_environmental,
						participation_environmental,
						reason_electric,
						choice_without_f_aid,
						EV_positive_surprise,
						EV_negative_surprise,
						used_shared_car,
						reason_to_use_shared_car,
						reason_to_not_use_shared_car,
						a_priv_loadPole,
						a_happy_walk,
						a_batt_suff,
						a_insuf_batt,
						overview_facilities_used,
						pos_elec,
						neg_elec,
						
						EV_happiness,
						nbr_peop_organisation,
						work_incentives,
						needs_charging,
						home_location,
						happiness,
						propensity_for_buying_bev,
						
						satisfaction_utility,
						uncertainty_utility,
						satisfaction_financial,
						uncertainty_financial,
						satisfaction_infrastructure, 
						uncertainty_infrastructure,
						satisfaction_social,
						uncertainty_social,
						satisfaction_occasion_utility,
						uncertainty_occasion_utility,
						satisfaction_occasion_financial,
						uncertainty_occasion_financial,
						satisfaction_occasion_infrastructure, 
						uncertainty_occasion_infrastructure,
						satisfaction_occasion_social,
						uncertainty_occasion_social,
						satisfaction_financial_lease,
						uncertainty_financial_lease,
						has_replaced_vehicle,
						chosen_vehicle
						));
			}
			else {
				context.add(new Private_new(
						context,
						id,
						weights_list,
						age,
						a_income,
						days_working_outside_house,
						a_education,
						household_composition,
						a_sex,
						province,
						a_work_situation,
						a_prof_situation,
						a_knowledge_BEV,
						parking_home,
						parking_at_work,
						car_combustion_type,
						car_age,
						car_weight,
						distance_traveled,
						CO2_emissions_traveled,
						CO2_emissions_per_km,
						car_ownership_type,
						
						a_satisfaction_commute,
						a_reason_to_go_with_car,
						knowledge_EV,
						knowledge_environmental,
						participation_environmental,
						reason_electric,
						choice_without_f_aid,
						EV_positive_surprise,
						EV_negative_surprise,
						used_shared_car,
						reason_to_use_shared_car,
						reason_to_not_use_shared_car,
						a_priv_loadPole,
						a_happy_walk,
						a_batt_suff,
						a_insuf_batt,
						overview_facilities_used,
						pos_elec,
						neg_elec,
						
						EV_happiness,
						nbr_peop_organisation,
						work_incentives,
						needs_charging,
						home_location,
						happiness,
						propensity_for_buying_bev,
						
						satisfaction_utility,
						uncertainty_utility,
						satisfaction_financial,
						uncertainty_financial,
						satisfaction_infrastructure, 
						uncertainty_infrastructure,
						satisfaction_social,
						uncertainty_social,
						satisfaction_occasion_utility,
						uncertainty_occasion_utility,
						satisfaction_occasion_financial,
						uncertainty_occasion_financial,
						satisfaction_occasion_infrastructure, 
						uncertainty_occasion_infrastructure,
						satisfaction_occasion_social,
						uncertainty_occasion_social,
						satisfaction_financial_lease,
						uncertainty_financial_lease,
						has_replaced_vehicle,
						chosen_vehicle
						));
			}	
		}
	}

	/**
	 * Computes someones propensity to buy a bev
	 * {@link calculate_infrastructure_propensity} and {@link calculate_propensity_dependend_on_income} and {@link fits_innovator_profile} and {@link fits_possible_adopter_profile}
	 * and {@link unable_profile}
	 * @return
	 */
	public double compute_propensity() {
		
		double propensity 	= 0;
		
		double financial_propensity 		= calculate_propensity_dependend_on_income();
		double infrastructure_propensity 	= calculate_infrastructure_propensity();
		double environmental_propensity		= environmentally_aware();

		if(fits_innovator_profile()) {
			if(a_income > 72000) 	propensity += 1;
			else
				if(36000 < a_income && a_income < 72.000) propensity += 0.5;
		}
		if(fits_possible_adopter_profile()) propensity += 0.1;
		if(unable_profile()) propensity -= 1;
		propensity = 1/(1+Math.exp(propensity)); 
		return propensity;
	}
	

	/**
	 * Determines the propensity to buy an bev depending on the type of available parking
	 * @return infrastructure_propensity
	 */
	private double calculate_infrastructure_propensity() {
		double infrastructure_propensity = 0;
		// At home
		if(parking_home == "private")				infrastructure_propensity += 0.5;
		if(parking_home == "neighbourhood paid")	infrastructure_propensity -= 0.3;
		if(parking_home == "neighbourhood free")	infrastructure_propensity += 0.25;
		if(parking_home == "somewhere else free")	infrastructure_propensity += 0.25;
		// At work
		if(parking_at_work == "Paid, on private property at work")	infrastructure_propensity -= 0.25;
		if(parking_at_work == "Free, on private property at work")	infrastructure_propensity += 0.5;
		
		if(parking_at_work == "Paid, in the vicinity of work")		infrastructure_propensity -= 0.25;
		if(parking_at_work == "Free, in the vicinity of work")		infrastructure_propensity += 0.25;
		
		if(parking_at_work == "Paid, in a public parking lot")		infrastructure_propensity -= 0.25;
		if(parking_at_work == "Free, in a public parking lot")		infrastructure_propensity += 0.25;
		
		if(parking_at_work == "Paid, park and ride terrain")		infrastructure_propensity -= 0.25;
		if(parking_at_work == "Free, park and ride terrain")		infrastructure_propensity += 0.25;
		
		return infrastructure_propensity;
	}
	
	/**
	 * The financial propensity follows a Gompertz function dependent on the income
	 * @param a_income
	 * @return the propensity to buy an bev dependent solemnly on someones income
	 */
	private double calculate_propensity_dependend_on_income() {
		int x = a_income;
		double propensity = Math.exp(-Math.exp(-1/12*x+2));
		return propensity;
	}

	/*** PROFILE EXAMINATION OF EACH AGENT *****************************************/
	
	/**
	 * Determines whether the agent fits the typical innovator profile indicated by RVO
	 * @return true or false
	 */
	public Boolean fits_innovator_profile() {
		Boolean prof = false;
		Boolean work = true;
		if (a_prof_situation == "Construction/industry" || a_prof_situation == "Commercial service") 	prof = true;
		if (a_work_situation == "fulltime/parttime" 	|| a_work_situation == "entrepeneur") 			work = true;
		
		if (	age > 24 &&
				age < 46 &&
				a_sex == "male" &&
				a_education == "Hoger beroeps- of wetenschappelijk onderwijs" &&
				prof &&
				work
				)
		return true;
		else return false;
	}

	/**
	 * Determines whether the agent fits the possible adopter profile
	 * @return true or false
	 */
	public Boolean fits_possible_adopter_profile() {
		boolean middle_aged, young, prof, work, multi_person_family, small_family, well_educated, high_income;
		middle_aged = young = prof = work = multi_person_family = small_family = well_educated = high_income = false;

		
		if(40 < age && age < 60) middle_aged = true;
		if(19 < age && age < 30) young = true;
		if (a_prof_situation == "Construction/industry" || a_prof_situation == "Commercial service") 	prof = true;
		if (a_work_situation == "fulltime/parttime" 	|| a_work_situation == "entrepeneur") 			work = true;
		if (household_composition == "Family with all children older than 12 years" || household_composition == "Family with at least 1 child of 12 years or younger") multi_person_family = true;
		if (household_composition == "Single-person household" || household_composition == "Partners without children") small_family = true;
		if (a_education == "Hoger beroeps- of wetenschappelijk onderwijs") 
		if(40000 < a_income ) high_income  = true;	
			
		// First profile	
		if(a_sex == "male" && prof && work && middle_aged && multi_person_family) return true;
		// Second profile
		if(middle_aged && well_educated && small_family && high_income) return true;
		// Third profile
		if(young && high_income && environmentally_aware()> 0.6 && work ) 	return true;
		else return false;
	
	}
	
	/**
	 * Determines whether the agent fits the laggard profile
	 * @return true or false
	 */
	public boolean unable_profile() {
		if(a_work_situation == "job seeker" && a_income < 36000)
			return true;
		else return false;
	}
	
	
	/*** CONVERT DATA FUNCTIONS *****************************************/
	
	/**
	 * Calculates the propensity someone has to acquire a BEV based on their knowledge and participation in environmentally frindly behaviour
	 * @return propensity_environment
	 */
	public double environmentally_aware() {
		double propensity_environment = 0.2*knowledge_environmental/6 + 0.8*participation_environmental/10;
		return propensity_environment;
		}
	
	public String convert_sex(int sex) {
		if(sex == 1) a_sex = "male";
		if(sex == 2) a_sex = "female";
		if(sex == 3) a_sex = "different";
		if(sex == 4) a_sex = "private";
		return a_sex;
	}
	
	public String convert_education(int education) {
		if(education == 1) a_education = "none";
		if(education == 2) a_education = "Lager onderwijs/ basisonderwijs";
		if(education == 3) a_education = "Lager of middelbaar algemeen voortgezet onderwijs"; //(LAVO, V(G)LO, MULO, MAVO);
		if(education == 4) a_education = "Hoger algemeen voortgezet onderwijs"; // (HAVO, VWO, HBS, MMS, lyceum, gymnasium);
		if(education == 5) a_education = "Lager beroepsonderwijs"; //(ambachts/huishoudschool, LTS, VBO, LHNO, LEAO, V(M)BO);
		if(education == 6) a_education = "Middelbaar beroepsonderwijs"; //(UTS, MTS, (K)MBO, MEAO, INAS, ROC, leerlingwezen);
		if(education == 7) a_education = "Hoger beroeps- of wetenschappelijk onderwijs"; //(HTS, HEAO, hogeschool, universiteit);
		if(education == 8) a_education = " ";
		return a_education;
	}
	
	public void convert_income(int income) {
		if(income == 1) a_income = 12500;
		if(income == 2) a_income = 24250;
		if(income == 3) a_income = 54000;
		if(income == 4) a_income = 80000;
	}
	
	public String convert_household(int household) {
		if(household == 1) household_composition = "Single-person household";
		if(household == 2) household_composition = "Partners without children";
		if(household == 3) household_composition = "Family with all children older than 12 years";
		if(household == 4) household_composition = "Family with at least 1 child of 12 years or younger";
		if(household == 5) household_composition = "Living group (students or other)";
		if(household == 6 || household == 7) household_composition = "unknown";
		return household_composition;
	}
	
	public int convert_car_age(int bouwjaar) {
		if(bouwjaar == 1)	car_age = 2019;
		if(bouwjaar == 2) 	car_age = 2018;
		if(bouwjaar == 3) 	car_age = 2017;
		if(bouwjaar == 4) 	car_age = 2016;
		if(bouwjaar == 5) 	car_age = 2015;
		if(bouwjaar == 6) 	car_age = 2014;
		if(bouwjaar == 7) 	car_age = 2013;
		if(bouwjaar == 8) 	car_age = 2012;
		if(bouwjaar == 9) 	car_age = 2011;
		if(bouwjaar == 10) 	car_age = 2010;
		if(bouwjaar == 11) 	car_age = 2009;
		if(bouwjaar == 12) 	car_age = 2008;
		if(bouwjaar == 13) 	car_age = 2007;
		if(bouwjaar == 14) 	car_age = 2006;
		if(bouwjaar == 15) 	car_age = 2005;
		if(bouwjaar == 16) 	car_age = 2004;
		if(bouwjaar == 17) 	car_age = 2003;
		if(bouwjaar == 18) 	car_age = 2002;
		if(bouwjaar == 19) 	car_age = 2001;
		if(bouwjaar == 20) 	car_age = 2000;
		if(bouwjaar == 21) 	car_age = 1999;
		if(bouwjaar == 22) 	car_age = -1;
		
		return car_age;
	}
	
	public int convert_car_weight(int gewichtsklasse) {
		if(gewichtsklasse == 1) car_weight = 950;	// Max 950 kg
		if(gewichtsklasse == 2) car_weight = 1150;	// 951-1.150 kg
		if(gewichtsklasse == 3) car_weight = 1350;	// 1.151-1.350 kg
		if(gewichtsklasse == 4) car_weight = 1550;	// 1.350-1.550 kg
		if(gewichtsklasse == 5) car_weight = 1600;	// > 1.550 kg
		if(gewichtsklasse == 6) car_weight = 0;		// unknown
		return car_weight;
	}
	
	public String convert_car_combustion_type(int fuelType) {
		if(fuelType == 1) 	car_combustion_type = "Gasoline";
		if(fuelType == 2) 	car_combustion_type = "Diesel";
		if(fuelType == 3)  	car_combustion_type = "LPG";
		if(fuelType == 4)  	car_combustion_type = "Hybride";
		if(fuelType == 5)  	car_combustion_type = "PHEV";
		if(fuelType == 6)	car_combustion_type = "Electric";
		if(fuelType == 7)  	car_combustion_type = "Hydrogen";
		
		return car_combustion_type;
	}

	public String convert_car_ownership_type(int carType) {
		if(carType == 1) car_ownership_type = "private_new";
		if(carType == 2) car_ownership_type = "private_lease";
		if(carType == 3) car_ownership_type = "company_lease";
		if(carType == 4) car_ownership_type = "company_car";
		if(carType == 5) car_ownership_type = "shared_car";
		if(carType == 6) car_ownership_type = " ";
		
		return car_ownership_type;
	}

	public String convert_work_situation(int work_situation) {
		if(work_situation == 0)		a_work_situation = "Onbekend";
		if(work_situation == 1)		a_work_situation = "fulltime/parttime"; 	//Baan in loondienst
		if(work_situation == 2)		a_work_situation = "zzp/freelancer";		//Zelfstandig ondernemer zonder personeel
		if(work_situation == 3)		a_work_situation = "entrepeneur";		//Zelfstandig ondernemer met personeel
		if(work_situation == 9)		a_work_situation = "job seeker"; 			//werkzoekend
		if(work_situation == 10)	a_work_situation = "non job seeker"; 		//Niet werkzoekend
		return a_work_situation;
	}

	public String convert_prof_sector(int prof_sector) {
		if(prof_sector == 0)		a_prof_situation = "Onbekend";
		if(prof_sector == 1)		a_prof_situation = "Construction/industry"; 			//landbouw en visserij, industrie, bouwnijverheid, nutsbedrijven
		if(prof_sector == 2)		a_prof_situation = "Commercial service ";				//handel, horeca, vervoer, ICT, financi�le dienstverlening, zakelijke dienstverlening
		if(prof_sector == 3)		a_prof_situation = "Non-commercial services"; 			//openbaar bestuur, onderwijs, gezondheid- en welzijnszorg, cultuur, overige dienstverlening
		return a_prof_situation;
	}

	public int convert_nbr_peop_organisation(int nbr_peop_organisation) {
		if (nbr_peop_organisation == 1) nbr_peop_organisation = 90;
		if (nbr_peop_organisation == 2) nbr_peop_organisation = 250;
		if (nbr_peop_organisation == 3) nbr_peop_organisation = 500;
		if (nbr_peop_organisation == 4) nbr_peop_organisation = 0;
		return  nbr_peop_organisation;
	}

	public List<String> convert_stim_work_provider(List<Integer> work_stimuli) {
		List<String> list_str = Arrays.asList(
				"Majority of employees travel with OV or bike to work",
				"For business travel the employer encourages to use OV or bike",
				"My employer gives personal advice when it comes to sustainable alternatives to using the car for commuting",
				"The management sets a good example by traveling by car as little as possible",
				"My employer has an active policy in the field of sustainable mobility",
				"No choice"
				);
		int u = 0;
		List<String> work_incentives = new ArrayList<String>();
		for(int i : work_stimuli) {
			if(i == 1) work_incentives.add(list_str.get(u));
			u+=1;
		}
		return work_incentives;
	}
	
	public Boolean convert_drivers_license(int drivers_license) {
		if(drivers_license == 0)	a_drivers_license = false;
		if(drivers_license == 1)	a_drivers_license = true;
		return a_drivers_license;
	}
	
	public Boolean convert_has_car(int has_car) {
		if(has_car == 0)	a_has_car = false;
		if(has_car == 1)	a_has_car = true;
		return a_has_car;
	}

	public String convert_home_parking(int private_parking, int neighbourhood_parking, int neighbourhood_parking_$, int somewhere_else_parking, int somewhere_else_parking_$ ) {
	if(private_parking 			== 1)		{	parking_home = "private";	}
	
	if(neighbourhood_parking 	== 1)		{	
		if(neighbourhood_parking_$ == 1)	parking_home = "neighbourhood paid";
		else parking_home = "neighbourhood free";
	}
	
	if(somewhere_else_parking 	== 1)		{	
		if(somewhere_else_parking_$ == 1)	parking_home = "somewhere else paid";
		else parking_home = "somewhere else free";
	}
	return parking_home;
	}	

	public String convert_parking_at_work(int at_work, int at_work_$) { 
		if(at_work == 1) {
			if(at_work_$ == 1)	parking_at_work = "Paid, on private property at work";
			else				parking_at_work = "Free, on private property at work";
		}
		if(at_work == 2){
			if(at_work_$ == 1)	parking_at_work = "Paid, in the vicinity of work";
			else				parking_at_work = "Free, in the vicinity of work";
		} 
		if(at_work == 3){
			if(at_work_$ == 1)	parking_at_work = "Paid, in a public parking lot";
			else				parking_at_work = "Free, in a public parking lot";
		}  
		if(at_work == 4){
			if(at_work_$ == 1)	parking_at_work = "Paid, park and ride terrain";
		}
			else				parking_at_work = "Free, park and ride terrain";
		if(at_work == 5) parking_at_work = " ";
		  
		return parking_at_work;
	} 
	
	public Boolean convert_knowledge_BEV(int knowledge_BEV) {
		if(knowledge_BEV == 1) a_knowledge_BEV = true;
		if(knowledge_BEV == 2) a_knowledge_BEV = false;
		return a_knowledge_BEV;
	}

	public String convert_satisfaction_commute(int satisfaction_commute) {
		if(satisfaction_commute == 1) a_satisfaction_commute = "Very unsatisfied";
		if(satisfaction_commute == 2) a_satisfaction_commute = "Unsatisfied";
		if(satisfaction_commute == 3) a_satisfaction_commute = "Indifferent";
		if(satisfaction_commute == 4) a_satisfaction_commute = "Satisfied";
		if(satisfaction_commute == 5) a_satisfaction_commute = "Very satisfied";
		return a_satisfaction_commute;
	}
	
	public String convert_reason_to_go_with_car(int reason_to_go_with_car) {
		if(reason_to_go_with_car == 1) 	a_reason_to_go_with_car = "Enough parking space at work";
		if(reason_to_go_with_car == 2) 	a_reason_to_go_with_car = "Free parking space at work";
		if(reason_to_go_with_car == 3) 	a_reason_to_go_with_car = "Being able to drive others";
		if(reason_to_go_with_car == 4) 	a_reason_to_go_with_car = "Combi of activities";
		if(reason_to_go_with_car == 5) 	a_reason_to_go_with_car = "Baggage transport";
		if(reason_to_go_with_car == 6) 	a_reason_to_go_with_car = "Being able to perform phone calls during commute";
		if(reason_to_go_with_car == 7) 	a_reason_to_go_with_car = "Compensation travel expenses";
		if(reason_to_go_with_car == 8) 	a_reason_to_go_with_car = "Having a company lease or company car";
		if(reason_to_go_with_car == 9) 	a_reason_to_go_with_car = "Need car for business appointments";
		if(reason_to_go_with_car == 10) a_reason_to_go_with_car = "Charging facility at work";
		if(reason_to_go_with_car == 11) a_reason_to_go_with_car = "Privacy";
		if(reason_to_go_with_car == 12) a_reason_to_go_with_car = "Status";
		return a_reason_to_go_with_car;
	}
	
	public int convert_knowledge_EV_regulations(int a, int b, int c) {
		knowledge_EV = a + b + c;
		return knowledge_EV;
	}
	
	public int convert_knowledge_environmental(int a, int b, int c, int d, int e, int f, int g) {
		knowledge_environmental = a + b + c + d + e + f;
		return knowledge_environmental;
	}

	public int convert_participation_environmental(List<Integer> env_tact) {
		int sum = 0;
		for (int i : env_tact)
			sum +=i;
		participation_environmental = sum;
		return participation_environmental;
	}
	
	public List<String> convert_EV_reason(List<Integer> reason_EV) {
		List<String> list_str = Arrays.asList(
				"Financial benefit",
				"Driving characteristics",
				"Environmentally friendly",
				"Cheaper in use",
				"Car brand",
				"Encouraged by employer"
				);
		int u = 0;
		List<String> reason_electric = new ArrayList<String>();
		for(int i : reason_EV) {
			if(i == 1) reason_electric.add(list_str.get(u));
			u+=1;
		}		
		return 	reason_electric;
	}

	public String convert_choice_without_f(int choice_without_F) {
		if(choice_without_F == 1) choice_without_f_aid = "No new car"; 
		if(choice_without_F == 2) choice_without_f_aid = "A cheaper electric vehicle"; 
		if(choice_without_F == 3) choice_without_f_aid = "A sustainable vehicle (hybrid/hydrogen)"; 
		if(choice_without_F == 4) choice_without_f_aid = "A gasoline or diesel vehicle"; 
		if(choice_without_F == 5) choice_without_f_aid = "The same choice"; 
		return choice_without_f_aid;
	}
	
	public List<String> convert_EV_positive_surprise(List<Integer> EV_pos) {
		List<String> list_str = Arrays.asList(
				"Sufficient charging options in the vicinity of my home;",
				"Sufficient charging options at my work location",
				"Sufficient public charging points",
				"The action radius",
				"Charging is fast enough",
				"Loading is less of a hassle than refueling",
				"The technology is reliable",
				"The annual costs are not too bad",
				"Even long distances are easy to do",
				"The safety of the car",
				"Nothing"
				);
		int u = 0;
		List<String> pos_elect = new ArrayList<String>();
		for(int i : EV_pos) {
			if(i == 1) pos_elect.add(list_str.get(u));
			u+=1;
		}		
		return 	pos_elect;
	}
	
	public List<String> convert_EV_negative_surprise(List<Integer> EV_neg) {
		List<String> list_str = Arrays.asList(
				"Nothing",
				"Insufficient charging options in the vicinity of my home",
				"Insufficient charging options at my work location",
				"Insufficient public charging points",
				"Charging takes too long",
				"You need to charge the care more often than refueling",
				"The annual costs are too high",
				"The technology is unreliable",
				"You can't go on vacation with a BEV",
				"The safety of the car"
				);
		int u = 0;
		
		List<String> neg_elect = new ArrayList<String>();
		for(int i : EV_neg) {
			if(i == 1) neg_elect.add(list_str.get(u));
			u+=1;
		}		
		return 	neg_elect;
		
	}
	
	public String convert_used_shared_car(int a, int b, int c) {
		if (a == 1) used_shared_car = "No";
		if (a == 2) used_shared_car = "Yes, with acquaintences";
		if (a == 3) used_shared_car = "Yes, I'm a member of a car sharing service";
		return used_shared_car;
	}

	public List<String> convert_reason_not_to_use_shared_car(List<Integer> re_not_sh) {
		List<String> list_str = Arrays.asList(
				"I have my own car",
				"I need a car too often",
				"I never need a car",
				"I still know too little about the use of shared cars",
				"The costs are too high",
				"I have a lease car",
				"I have no certainty about availability", 
				"I am bound by fixed times",
				"It seems too much hassle to me",
				"There are too few shared cars in the vicinity of my home",
				"Specific requirements for cars (seats, trunk, etc)",
				"I never seriously considered it"
				);
		int u = 0;
		List<String> reason_not_to_use_shared_car = new ArrayList<String>();
		for(int i : re_not_sh) {
			if(i == 1) reason_not_to_use_shared_car.add(list_str.get(u));
			u+=1;
		}		
		return 	reason_not_to_use_shared_car;
	}
	
	public List<String> convert_reason_to_use_shared_car(List<Integer> re_use_sh) {
		List<String> list_str = Arrays.asList(
				"Cost savings compared to your own car",
				"I cannot have my own car",
				"Less hassle than your own car",
				"Handy as a second car",
				"Always parking space",
				"Nice to drive electrically",
				"Faster than public transport",
				"Better for the environment",
				"Fewer cars on the street",
				"Matches my image"
				);
		int u = 0;
		List<String> reason_to_use_shared_car = new ArrayList<String>();
		for(int i : re_use_sh) {
			if(i == 1) reason_to_use_shared_car.add(list_str.get(u));
			u+=1;
		}	
		return 	reason_to_use_shared_car;
	}

	public Boolean convert_priv_loadPole(int priv_loadPole) {
		if (priv_loadPole == 1) {
			a_priv_loadPole = true;
		}
		else a_priv_loadPole = false;
		return a_priv_loadPole;
	}
	
	public String convert_happy_walk(int happy_walk) {
		if( happy_walk == 1) a_happy_walk = "Very happy";
		if( happy_walk == 2) a_happy_walk = "Happy";
		if( happy_walk == 3) a_happy_walk = "Indifferent";
		if( happy_walk == 4) a_happy_walk = "Unhappy";
		if( happy_walk == 5) a_happy_walk = "Very unhappy";
		return	a_happy_walk;
	}
	
	public String convert_batt_suf(int batt_suf) {
			if( batt_suf == 1) a_batt_suff = "Always";
			if( batt_suf == 2) a_batt_suff = "Often";
			if( batt_suf == 3) a_batt_suff = "Sometimes";
			if( batt_suf == 4) a_batt_suff = "Rarely";
			if( batt_suf == 5) a_batt_suff = "Never";
			return 	a_batt_suff;
		}
	
	public String convert_fear_insuf_batt(int insuf_batt) {
				if( insuf_batt == 1) a_insuf_batt = "Never";
				if( insuf_batt == 2) a_insuf_batt = "1 to 2 times a month";
				if( insuf_batt == 3) a_insuf_batt = "1 to 2 times a week";
				if( insuf_batt == 4) a_insuf_batt = "Daily";
				if( insuf_batt == 5) a_insuf_batt = "I don't know";
				return 	a_insuf_batt;
			}
	
	public List<String> conver_fac_EV(List<Integer> fac_EV){
		List<String> list_str = Arrays.asList(
				"Own private loadPole",
				"Public loadPole",
				"Fast Charger on highway",
				"At work",
				"Other",
				"Total"
				);
		int u = 0;
		List<String> overview_facilities_used = new ArrayList<String>();
		for(int i : fac_EV) {
			overview_facilities_used.add(list_str.get(u)+ ";" + i);
			u+=1;
		}
		return overview_facilities_used;
	}

	/*** GET FUNCTIONS */
	
	public int get_total_EV(Context<Object> context) {
		int ev = 0;
		for(Object obj : context.getObjects(agents.class)) {
			if (((agents) obj).car_combustion_type.equals("Electric")) {
				ev +=1;
			}
		}
		return ev;
	}
	
	public static int get_the_nth_agent() {
		return the_nth_agent;
	}
	
	/*** SET FUNCTION */
	
	public static void set_the_nth_agent(){
		if(the_nth_agent < 199)
			the_nth_agent ++;
		else the_nth_agent = 0;
	}
	
	/**
	 * Write the agent parameters to a file
	 * @param context
	 * @param id
	 * @param weights_list
	 * @param age
	 * @param income
	 * @param days_working_outside_house
	 * @param education
	 * @param household_composition
	 * @param sex
	 * @param province
	 * @param a_work_situation
	 * @param a_prof_situation
	 * @param a_knowledge_BEV
	 * @param parking_home
	 * @param parking_at_work
	 * @param car_combustion_type
	 * @param car_age
	 * @param car_weight
	 * @param distance_traveled
	 * @param CO2_emissions_traveled
	 * @param CO2_emissions_per_km
	 * @param property_type
	 * @param satisfaction_commute
	 * @param reason_to_go_with_car
	 * @param knowledge_EV
	 * @param knowledge_environmental
	 * @param participation_environmental
	 * @param EV_reason
	 * @param choice_without_f_aid
	 * @param EV_positive_surprise
	 * @param EV_negative_surprise
	 * @param used_shared_car
	 * @param reason_to_use_shared_car
	 * @param reason_to_not_use_shared_car
	 * @param priv_loadPole
	 * @param happy_walk
	 * @param batt_suff
	 * @param insuf_batt
	 * @param overview_facilities_used
	 * @param pos_elec
	 * @param neg_elec
	 * @param EV_happiness
	 * @param nbr_peop_organisation
	 * @param work_incentives
	 * @param needs_charging
	 * @param home_location
	 * @param happiness
	 * @param propensity_for_buying_bev
	 * @param satisfaction_utility
	 * @param uncertainty_utility
	 * @param satisfaction_financial
	 * @param uncertainty_financial
	 * @param satisfaction_infrastructure
	 * @param uncertainty_infrastructure
	 * @param satisfaction_social
	 * @param uncertainty_social
	 * @param satisfaction_occasion_utility
	 * @param uncertainty_occasion_utility
	 * @param satisfaction_occasion_financial
	 * @param uncertainty_occasion_financial
	 * @param satisfaction_occasion_infrastructure
	 * @param uncertainty_occasion_infrastructure
	 * @param satisfaction_occasion_social
	 * @param uncertainty_occasion_social
	 * @param satisfaction_financial_lease
	 * @param uncertainty_financial_lease
	 * @param has_replaced_vehicle
	 * @param chosen_vehicle
	 */
	public void add_agent_to_file(Context<Object> context,
			int id, List<Double> weights_list, int age, int income,
			int days_working_outside_house, String education, String household_composition, String sex,
			String province, String a_work_situation, String a_prof_situation, Boolean a_knowledge_BEV,
			String parking_home, String parking_at_work, String car_combustion_type, int car_age,
			int car_weight, float distance_traveled, float CO2_emissions_traveled, float CO2_emissions_per_km,
			String property_type, String satisfaction_commute, String reason_to_go_with_car, int knowledge_EV,
			int knowledge_environmental, int participation_environmental, List<String> EV_reason, String choice_without_f_aid,
			String EV_positive_surprise, String EV_negative_surprise, String used_shared_car, List<String> reason_to_use_shared_car,
			List<String> reason_to_not_use_shared_car, Boolean priv_loadPole, String happy_walk, String batt_suff, String insuf_batt,
			List<String> overview_facilities_used, List<String> pos_elec, List<String> neg_elec, int EV_happiness, int nbr_peop_organisation,
			List<String> work_incentives, Boolean needs_charging, GridPoint home_location, double happiness, double propensity_for_buying_bev, double satisfaction_utility,
			double uncertainty_utility, double satisfaction_financial, double uncertainty_financial, double satisfaction_infrastructure,  double uncertainty_infrastructure,
			double satisfaction_social, double uncertainty_social, double satisfaction_occasion_utility, double uncertainty_occasion_utility, double satisfaction_occasion_financial,
			double uncertainty_occasion_financial, double satisfaction_occasion_infrastructure,  double uncertainty_occasion_infrastructure, double satisfaction_occasion_social,
			double uncertainty_occasion_social, double satisfaction_financial_lease, double uncertainty_financial_lease, Boolean has_replaced_vehicle, vehicle chosen_vehicle) {
		
		File file = new File("data/agents_R_evaluation.csv"); 
		  
	    try { 
	        FileWriter outputfile = new FileWriter(file, true); 
	        CSVWriter writer = new CSVWriter(outputfile); 
	  
	        writer.writeNext(new String[] { Integer.toString(id),  Integer.toString(age), Integer.toString(a_income), Integer.toString(days_working_outside_house), a_education,
					household_composition, a_sex, province, a_work_situation, a_prof_situation, Boolean.toString(a_knowledge_BEV),
					parking_home, parking_at_work, car_combustion_type, Integer.toString(car_age), Integer.toString(car_weight), Float.toString(distance_traveled),
					Float.toString(CO2_emissions_traveled), Float.toString(CO2_emissions_per_km), car_ownership_type, a_satisfaction_commute,
					a_reason_to_go_with_car, Integer.toString(knowledge_EV), Integer.toString(knowledge_environmental), Integer.toString(participation_environmental),
					reason_electric.toString(), choice_without_f_aid, EV_positive_surprise, EV_negative_surprise,
					used_shared_car, reason_to_use_shared_car.toString(), reason_to_not_use_shared_car.toString(), a_priv_loadPole.toString(),
					a_happy_walk, a_batt_suff, a_insuf_batt, overview_facilities_used.toString(), pos_elec.toString(), neg_elec.toString(),
					Integer.toString(EV_happiness), Integer.toString(nbr_peop_organisation), work_incentives.toString()}); 

	        writer.close(); 
	    } 
	    catch (IOException e) { 
	        e.printStackTrace(); 
	    } 
	}


}

