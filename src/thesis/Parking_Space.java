package thesis;

import java.awt.Color;

import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;

/**
 * 
 * @author Dani Sibbel
 *
 */

public class Parking_Space {
	private Grid<Object> grid;
	private Context<Object> context;
	String type;
	boolean occupied;

	/**
	 * Initialised the parking spaces in the grid
	 * They are of type lp or normal and they can either be occupied or not
	 * @param grid
	 * @param type
	 * @param occupied
	 */
	public Parking_Space(Grid<Object> grid, String type, boolean occupied) {
		this.grid = grid;
		this.type = type;
		this.occupied = false;
	}

	/*** GET FUNCTIONS */
	
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean getOccupied() {
		return this.occupied;
	}

	public int get_with_lp_or_not() {
		if(this.type == "with_loadPole")
			return 1;
		else
			return 2;
	}
	
	/*** SET FUNCTIONS */
	
	public void setOccupied(boolean occupied) {
		this.occupied = occupied;
	}

}
