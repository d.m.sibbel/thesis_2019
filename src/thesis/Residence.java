package thesis;

import java.util.List;
import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;

/**
 * 
 * @author Dani Sibbel
 *
 */

public class Residence {
		private Grid<Object> grid;
		private Context<agents> context;
		int numbr_people;
		String personality_type;
		Boolean has_EV;
		private List<Object> residents;
		
		/**
		 * Initialise the home location of the agents
		 * @param grid
		 * @param personality_type
		 * @param has_EV
		 */
		public Residence(Grid<Object> grid, String personality_type, boolean has_EV) {
			this.grid = grid;
			this.personality_type = personality_type;
			this.has_EV = has_EV;
		}

		/**
		 * Attributes each agent with exactly one home location
		 * @param human
		 */
		public void moveIn(agents human) {
			human.setHomeLocation(this);
			residents.add(human);
			
		}
		
		/*** GET FUNCTIONS */
		
		public String get_personality_type() {
			return this.personality_type;
		}
		
		public void get_numbr_people(int numbr_people) {
			this.numbr_people = residents.size();
		}

		public boolean get_has_EV() {
			return this.has_EV;
		}
		
		public GridPoint getLocation() {
			return grid.getLocation(this);
		}
		
		/*** SET FUNCTION */
		
		public void set_has_EV(boolean has_EV) {
			this.has_EV = has_EV;
		}
}
