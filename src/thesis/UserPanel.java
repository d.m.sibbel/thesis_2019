package thesis;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import repast.simphony.engine.environment.RunEnvironmentBuilder;
import repast.simphony.scenario.ModelInitializer;
import repast.simphony.scenario.Scenario;
import repast.simphony.ui.RSApplication;

public class UserPanel implements ModelInitializer {

	static Boolean sliderChanged = false;
	Boolean checkbox_selected = false;
	static int subsidyValueNew = 4000;
	static int subsidyValueOccasion = 2000;
	static int mrbvalue = 0;
	static int bpmvalue = 0;
	static int increaseDieselTax = 0;
	static int increaseGasolineTax = 1;
	static int conversion_Rate_lp = 10;

	// LEASE
	static int lease_cost_S = 0;
	static int lease_fuel_S = 0;
	static int lease_tax_S = 0;
	static int lease_know_S = 0;

	static int lease_cost_U = 0;
	static int lease_fuel_U = 0;
	static int lease_tax_U = 0;
	static int lease_know_U = 0;

	// NEW FINANCIAL
	static int new_cost_S = 0;
	static int new_fuel_S = 0;
	static int new_tax_S = 0;
	static int new_know_S = 0;
	static int new_incen_S = 0;

	static int new_cost_U = 0;
	static int new_fuel_U = 0;
	static int new_tax_U = 0;
	static int new_know_U = 0;
	static int new_incen_U = 0;

	// NEW INFRASTRUCTURE
	static int new_tech_S = 0;
	static int new_knowledge_BEV_S = 0;
	static int new_charging_S = 0;
	static int new_parking_S = 0;
	static int new_commute_S = 0;

	static int new_tech_U = 0;
	static int new_knowledge_BEV_U = 0;
	static int new_charging_U = 0;
	static int new_parking_U = 0;
	static int new_commute_U = 0;

	// NEW SOCIAL
	static int new_parking_av_S = 0;
	static int new_conformity_S = 0;
	static int new_environment_S = 0;

	static int new_parking_av_U = 0;
	static int new_conformity_U = 0;
	static int new_environment_U = 0;

	// OCCASION FINANCIAL
	static int occ_cost_S = 0;
	static int occ_fuel_S = 0;
	static int occ_tax_S = 0;
	static int occ_know_S = 0;
	static int occ_incen_S = 0;

	static int occ_cost_U = 0;
	static int occ_fuel_U = 0;
	static int occ_tax_U = 0;
	static int occ_know_U = 0;
	static int occ_incen_U = 0;

	// OCCASION INFRASTRUCTURE
	static int occ_age_S = 0;
	static int occ_battery_S = 0;
	static int occ_parking_S = 0;

	static int occ_age_U = 0;
	static int occ_battery_U = 0;
	static int occ_parking_U = 0;

	// OCCASION SOCIAL
	static int occ_status_S = 0;
	static int occ_conformity_S = 0;
	static int occ_income_acc_S = 0;

	static int occ_status_U = 0;
	static int occ_conformity_U = 0;
	static int occ_income_acc_U = 0;

	// ***************************
	static int uncertainty_social = 4;
	static int uncertainty_infrastructure = 2;
	static int uncertainty_financial = 10;

	static int satisfaction_social = 6;
	static int satisfaction_infrastructure = 2;
	static int satisfaction_financial = 10;

	static int lease_uncertainty_social = 0;
	static int lease_uncertainty_infrastructure = 0;
	static int lease_uncertainty_financial = 10;

	static int lease_satisfaction_social = 0;
	static int lease_satisfaction_infrastructure = 0;
	static int lease_satisfaction_financial = 10;

	static int occasion_uncertainty_social = 3;
	static int occasion_uncertainty_infrastructure = 2;
	static int occasion_uncertainty_financial = 10;

	static int occasion_satisfaction_social = 6;
	static int occasion_satisfaction_infrastructure = 3;
	static int occasion_satisfaction_financial = 10;

	static boolean button_CS_subsidy;
	static boolean credit_for_charging;
	static public String scenario = "";
	static public int[] multiple_scenarios = {};
	
	/*** USER PANEL */
	
	/**
	 * Adds sliders buttons and windows to the User Panel
	 */
	public UserPanel() {
		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
		addConversionRateLP(up);
		addSubsidySliderNew(up);
		addSubsidySliderOccasion(up);
		add_MRB_exemption(up);
		add_BPM_exemption(up);
		addIncreaseGasolineTax(up);
		RSApplication.getRSApplicationInstance().addCustomUserPanel(up);
	}

	private void addConversionRateLP(JPanel up) {
		JLabel label = new JLabel("Parking Spaces are converted to loadPoles with every x BEV owners added");
		up.add(label);
		JSlider conversion_lp_slider = new JSlider();
		conversion_lp_slider.setMinimum(0);
		conversion_lp_slider.setMaximum(100);
		conversion_lp_slider.setValue(conversion_Rate_lp);
		conversion_lp_slider.setPaintTicks(true);
		conversion_lp_slider.setMajorTickSpacing(10);
		conversion_lp_slider.setPaintLabels(true);
		conversion_lp_slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				conversion_Rate_lp = conversion_lp_slider.getValue();
			}

		});
		up.add(conversion_lp_slider);
	}
	
	private void addSubsidySliderNew(JPanel up) {
		JLabel label = new JLabel("Government subsidies for new buys");
		up.add(label);
		JSlider new_slider = new JSlider();
		new_slider.setMinimum(0);
		new_slider.setMaximum(20000);
		new_slider.setValue(subsidyValueNew);
		new_slider.setPaintTicks(true);
		new_slider.setMajorTickSpacing(5000);
		new_slider.setPaintLabels(true);
		new_slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				subsidyValueNew = new_slider.getValue();
				sliderChanged = true;
			}
			

		});
		up.add(new_slider);
	}
	
	private void addSubsidySliderOccasion(JPanel up) {
		JLabel label = new JLabel("Government subsidies for occasions");
		up.add(label);
		JSlider occasion_slider = new JSlider();
		occasion_slider.setMinimum(0);
		occasion_slider.setMaximum(20000);
		occasion_slider.setValue(subsidyValueOccasion);
		occasion_slider.setPaintTicks(true);
		occasion_slider.setMajorTickSpacing(5000);	
		occasion_slider.setPaintLabels(true);
		occasion_slider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				subsidyValueOccasion = occasion_slider.getValue();
				sliderChanged = true;
			}

		});
		up.add(occasion_slider);
	}
	
	private void add_MRB_exemption(JPanel up) {
		JLabel label = new JLabel("MRB exemption");
		up.add(label);
		JSlider slider = new JSlider();
		slider.setMinimum(0);
		slider.setMaximum(100);
		slider.setValue(mrbvalue);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(10);
		slider.setPaintLabels(true);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				mrbvalue = slider.getValue();
				sliderChanged = true;
			}

		});
		up.add(slider);
	}
	
	private void add_BPM_exemption(JPanel up) {
		JLabel label = new JLabel("BPM exemption");
		up.add(label);
		JSlider slider = new JSlider();
		slider.setMinimum(0);
		slider.setMaximum(100);
		slider.setValue(bpmvalue);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(10);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				bpmvalue = slider.getValue();
				sliderChanged = true;
			}

		});
		up.add(slider);
	}
	
	private void addIncreaseGasolineTax(JPanel up) {
		JLabel label = new JLabel("Increase the Gasoline Fuel Tax");
		up.add(label);
		JSlider slider = new JSlider();
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(increaseGasolineTax);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setValue(1);
		slider.setPaintLabels(true);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				increaseGasolineTax = slider.getValue();
				sliderChanged = true;
			}

		});
		up.add(slider);
	}
	
	private void addSubsidyCS(JPanel up) {
		JLabel label = new JLabel("Subsidy CS");
		up.add(label);

		JPanel t = new JPanel();
		t.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		JButton subsidy_CS = new JButton("Subsidy CS");
		subsidy_CS.setPreferredSize(new Dimension(100, 50));
		subsidy_CS.setForeground(Color.BLACK);
		subsidy_CS.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (button_CS_subsidy) {
					subsidy_CS.setText("Subsidy for CS");
					button_CS_subsidy = false;
					subsidy_CS.setForeground(Color.BLACK);
					subsidy_CS.repaint();
				} else {
					subsidy_CS.setText("No subsidy for CS");
					button_CS_subsidy = true;
					subsidy_CS.setForeground(Color.GRAY);
					subsidy_CS.repaint();
				}
				System.out.println(button_CS_subsidy);
			}

		});
		c.gridx = 0;
		c.gridy = 0;

		t.add(subsidy_CS, c);
		t.setPreferredSize(new Dimension(200, 3 * 50));
		up.add(t);
	}
	
	private void add_Charging_credit(JPanel up) {
		JLabel label = new JLabel("Charging credit");
		up.add(label);

		JPanel t = new JPanel();
		t.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		JButton charging_credit = new JButton("Charging credit");
		charging_credit.setPreferredSize(new Dimension(100, 50));
		charging_credit.setForeground(Color.BLACK);
		charging_credit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (button_CS_subsidy) {
					charging_credit.setText("Charging credit");
					credit_for_charging = false;
					charging_credit.setForeground(Color.BLACK);
					charging_credit.repaint();
				} else {
					charging_credit.setText("No charging credit");
					credit_for_charging = true;
					charging_credit.setForeground(Color.GRAY);
					charging_credit.repaint();
				}
				System.out.println(credit_for_charging);
			}

		});
		c.gridx = 0;
		c.gridy = 0;

		t.add(charging_credit, c);
		t.setPreferredSize(new Dimension(200, 3 * 50));
		up.add(t);
	}


	/*** WINDOW */
	
	/**
	 * Builds the four windows to be used by the user
	 */
	@Override
	public void initialize(Scenario scen, RunEnvironmentBuilder builder) {	
		add_private_new_sliders_window();
		add_lease_sliders_window();
		add_occasion_sliders_window();
		add_scenarios_window();
	}
	
	private void add_private_new_sliders_window() {
		JFrame priv_window = new JFrame("Load sliders for PRIVATE NEW agents");
		priv_window.setBounds(50, 100, 400, 400);  
		priv_window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		priv_window.setVisible(true);  
		priv_window.setLayout(new GridLayout(10, 0));
		
	    add_satisfaction_financial(priv_window);
		add_satisfaction_social(priv_window);
		add_satisfaction_infrastructure(priv_window);
		
		add_uncertainty_financial(priv_window);
		add_uncertainty_social(priv_window);
		add_uncertainty_infrastructure(priv_window);
		
	}
	
	private void add_lease_sliders_window() {
		JFrame lease_window = new JFrame("Load sliders for LEASED agents");
		lease_window.setBounds(500, 100, 400, 400); 
		lease_window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		lease_window.setVisible(true);
		lease_window.setLayout(new GridLayout(10, 0));

		add_lease_satisfaction_financial(lease_window);
		add_lease_satisfaction_social(lease_window);
		add_lease_satisfaction_infrastructure(lease_window);
		
		add_lease_uncertainty_financial(lease_window);
		add_lease_uncertainty_social(lease_window);
		add_lease_uncertainty_infrastructure(lease_window);
	}
	
	private void add_occasion_sliders_window() {
		JFrame occasion_window = new JFrame("Load sliders for OCCASION agents");
		occasion_window.setBounds(1000, 100, 400, 400);
		occasion_window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		occasion_window.setVisible(true);
		occasion_window.setLayout(new GridLayout(10, 0));

		add_occasion_satisfaction_financial(occasion_window);
		add_occasion_satisfaction_social(occasion_window);
		add_occasion_satisfaction_infrastructure(occasion_window);

		add_occasion_uncertainty_financial(occasion_window);
		add_occasion_uncertainty_social(occasion_window);
		add_occasion_uncertainty_infrastructure(occasion_window);
	}
	
	private void add_scenarios_window() {
		JFrame Scenario = new JFrame("Load Policy Scenarios");
		Scenario.setBounds(50, 500, 800, 400);
		Scenario.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Scenario.setVisible(true);
		Scenario.setLayout(new GridLayout(2, 1));
	    
		JCheckBox checkbox = new JCheckBox("English");
	    Scenario.add(checkbox, JFrame.BOTTOM_ALIGNMENT);
	    checkbox.setSelected(false);
	    
	    String[] scenario_names = {
	    		"Implementeer eindige hoeveelheid subsidy",
	    		 "Subsidie verminderen tot 0 in 2025",
	    		 "Verhoging acijns voor Benzine autos",
	    		 "MRB en BPM voordeel voor BEVs behouden",
	    		 "Verhoogde aanbouw laadpalen",
	    		 "Elektrische auto wordt elk jaar goedkoper",
	    		 "Elektrische range wordt elk jaar groter"};
	    String[] scenario_en = {
	    		 "Implement limited amount of subsidy",
	    		 "Gradually decreasing the BEV subsidy till 0 in 2025",
	    		 "Increase taxes for Gasoline vehicles each year",
	    		 "Do not abolish tax exemptions for BEVs",
	    		 "Increase the rate at which CS are added",
	    		 "Each year BEVs become cheaper",
	    		 "Each year the range of BEVs increases"
		    		};
	    final JList scenario_list = new JList(scenario_names);
	    scenario_list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	    
	    JPanel list_panel2 = new JPanel();
	    list_panel2.add(scenario_list);
	    list_panel2.setSize(200, 100);
	    
	    final JList scenario_list_EN = new JList(scenario_en);
	    scenario_list_EN.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	    JPanel list_panel3 = new JPanel();
	    list_panel3.add(scenario_list_EN);
	    list_panel3.setSize(200, 100);
	   
	    // Initialize buttons
	    final JButton select_button = new JButton("Select");
	    final JButton reset_button = new JButton("Reset");
	    reset_button.setVisible(false);
	    
		checkbox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (checkbox.isSelected()) {
					checkbox_selected = true;
					System.out.println("checkbox selected");
					reset_button.setVisible(false);
					select_button.setVisible(true);
					scenario = "";
					Scenario.add(list_panel3);
					scenario_list_EN.setVisible(true);
					scenario_list.setVisible(false);

					select_button.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(ActionEvent e) {
							if (scenario_list_EN.getSelectedIndices() != null) {
								multiple_scenarios = scenario_list_EN.getSelectedIndices();
							}
							if (scenario_list_EN.getSelectedValue() != null)
								scenario = scenario_list_EN.getSelectedValue().toString();
							scenario_list_EN.setVisible(false);
							select_button.setVisible(false);
							reset_button.setVisible(true);
						}
					});

					reset_button.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(ActionEvent e) {
							scenario = "";
							scenario_list_EN.setVisible(true);
							scenario_list.setVisible(false);
							select_button.setVisible(true);
							reset_button.setVisible(false);
						}
					});

				} else {
					checkbox_selected = false;
					System.out.println("checkbox unselected");
					scenario_list_EN.setVisible(false);
					scenario_list.setVisible(true);

					select_button.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(ActionEvent e) {
							if (scenario_list.getSelectedIndices() != null) {
								multiple_scenarios = scenario_list_EN.getSelectedIndices();
							}
							if (scenario_list.getSelectedValue() != null)
								scenario = scenario_list.getSelectedValue().toString();
							scenario_list.setVisible(false);
							scenario_list_EN.setVisible(false);
							select_button.setVisible(false);
							reset_button.setVisible(true);
						}
					});

					reset_button.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(ActionEvent e) {
							scenario = "";
							scenario_list.setVisible(true);
							scenario_list_EN.setVisible(false);
							select_button.setVisible(true);
							reset_button.setVisible(false);
						}
					});

				}
			}
		});
	   
	    
	    select_button.addActionListener(new java.awt.event.ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	    	  
	    	  if (scenario_list_EN.getSelectedIndices() != null) {
					multiple_scenarios = scenario_list.getSelectedIndices();
				}
	    	  if (scenario_list.getSelectedValue() != null)
	    		  scenario = scenario_list.getSelectedValue().toString();
	    	  scenario_list.setVisible(false);
	    	  select_button.setVisible(false);
	    	  reset_button.setVisible(true);
	      }
	    });
	    
	    reset_button.addActionListener(new java.awt.event.ActionListener() {
		      public void actionPerformed(ActionEvent e) {
		    	scenario = "";
		        scenario_list.setVisible(!checkbox_selected);
		        scenario_list_EN.setVisible(checkbox_selected);
		        System.out.println("UUUUUUUUUUUUUUUUU " + checkbox_selected);
		        select_button.setVisible(true);
		        reset_button.setVisible(false);
		      }
		    });
	    


		Scenario.add(select_button);
		Scenario.add(reset_button); 
	    Scenario.add(list_panel2);
	     
		//***************************************************************//
	}


	/*** NEW SLIDERS */
	
	private void add_uncertainty_financial(JFrame window) {
		JLabel label = new JLabel("Uncertainty Financial");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(uncertainty_financial);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(10);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				uncertainty_financial = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}
	
	private void add_uncertainty_social(JFrame window) {
		JLabel label = new JLabel("Uncertainty Social");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(uncertainty_social);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(4);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				uncertainty_social = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}
	
	private void add_uncertainty_infrastructure(JFrame window) {
		JLabel label = new JLabel("Uncertainty Infrastructure");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(uncertainty_infrastructure);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(2);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				uncertainty_infrastructure = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}

	private void add_satisfaction_financial(JFrame window) {
		JLabel label = new JLabel("Satisfaction Financial");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(satisfaction_financial);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(10);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				satisfaction_financial = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}
	
	private void add_satisfaction_social(JFrame window) {
		JLabel label = new JLabel("Satisfaction Social");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(satisfaction_social);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(6);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				satisfaction_social = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}
	
	private void add_satisfaction_infrastructure(JFrame window) {
		JLabel label = new JLabel("Satisfaction Infrastructure");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(satisfaction_infrastructure);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(2);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				satisfaction_infrastructure = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}
	
	/*** LEASE SLIDERS */
	
	private void add_lease_uncertainty_financial(JFrame window) {
		JLabel label = new JLabel("Financial Uncertainty");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(lease_uncertainty_financial);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(10);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				lease_uncertainty_financial = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}
	
	private void add_lease_uncertainty_social(JFrame window) {
		JLabel label = new JLabel("Social Uncertainty");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(lease_uncertainty_social);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				lease_uncertainty_social = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}
	
	private void add_lease_uncertainty_infrastructure(JFrame window) {
		JLabel label = new JLabel("Infrastructure uncertainty ");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(lease_uncertainty_infrastructure);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				lease_uncertainty_infrastructure = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}
	
	private void add_lease_satisfaction_financial(JFrame window) {
		JLabel label = new JLabel("Financial satisfaction ");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(lease_satisfaction_financial);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(10);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				lease_satisfaction_financial = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}
	
	private void add_lease_satisfaction_social(JFrame window) {
		JLabel label = new JLabel("Social satisfaction");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(lease_satisfaction_social);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				lease_satisfaction_social = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}
	
	private void add_lease_satisfaction_infrastructure(JFrame window) {
		JLabel label = new JLabel("Infrastructure satisfaction");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(lease_satisfaction_infrastructure);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				lease_satisfaction_infrastructure = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}

	/*** OCCASION SLIDERS */
	
	private void add_occasion_uncertainty_financial(JFrame window) {
		JLabel label = new JLabel("Financial Uncertainty");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occasion_uncertainty_financial);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(10);
		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				occasion_uncertainty_financial = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}

	private void add_occasion_uncertainty_social(JFrame window) {
		JLabel label = new JLabel("Social Uncertainty");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occasion_uncertainty_social);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(3);
		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				occasion_uncertainty_social = slider.getValue();
				sliderChanged =true;
			}

		});
		window.add(slider);
	}

	private void add_occasion_uncertainty_infrastructure(JFrame window) {
		JLabel label = new JLabel("Infrastructure uncertainty ");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occasion_uncertainty_infrastructure);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(2);
		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				occasion_uncertainty_infrastructure = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}

	private void add_occasion_satisfaction_financial(JFrame window) {
		JLabel label = new JLabel("Financial satisfaction ");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occasion_satisfaction_financial);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(10);
		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				occasion_satisfaction_financial = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}

	private void add_occasion_satisfaction_social(JFrame window) {
		JLabel label = new JLabel("Social satisfaction");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occasion_satisfaction_social);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(6);
		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				occasion_satisfaction_social = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}

	private void add_occasion_satisfaction_infrastructure(JFrame window) {
		JLabel label = new JLabel("Infrastructure satisfaction");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occasion_satisfaction_infrastructure);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(3);
		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				occasion_satisfaction_infrastructure = slider.getValue();
				sliderChanged = true;
			}

		});
		window.add(slider);
	}


	/*** GET FUNCTIONS */
	
	public static int get_mrb_value() {
		return mrbvalue;
	}
	
	public static int get_bpm_value() {
		return bpmvalue;
	}

	/*** RESET FUNCTION */
	
	public static void reset_sliderChanged() {
		sliderChanged = false;
	}
	
	/***************************************************************************************************************/
	
	/*** ADDITIONAL SLIDERS FOR LATER */
	
	private void add_slider_Satisfaction_LF_cost_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the difference in lease cost between BEV and CI on the satisfaction of the lease agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(lease_cost_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(8);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				lease_cost_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Satisfaction_LF_fuel_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the difference in fuel cost between BEV and CI on the satisfaction of the lease agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(100);
		slider.setValue(lease_fuel_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(10);
		slider.setPaintLabels(true);
		slider.setValue(9);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				lease_fuel_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Satisfaction_LF_tax_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the difference in tax cost between BEV and CI on the satisfaction of the lease agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(100);
		slider.setValue(lease_tax_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(10);
		slider.setPaintLabels(true);
		slider.setValue(1);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				lease_tax_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Satisfaction_LF_knowledge_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the agents knowledge of the BEV incentives on the satisfaction of the lease agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(lease_know_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(1);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				lease_know_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	
	private void add_slider_Uncertainty_LF_cost_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the difference in lease cost between BEV and CI on the uncertainty of the lease agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(lease_cost_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(6);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				lease_cost_U = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Uncertainty_LF_fuel_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the difference in fuel cost between BEV and CI on the uncertainty of the lease agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(lease_fuel_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(3);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				lease_fuel_U = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Uncertainty_LF_tax_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the difference in tax cost between BEV and CI on the uncertainty of the lease agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(lease_tax_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(1);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				lease_tax_U = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Uncertainty_LF_knowledge_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the agents knowledge of the BEV incentives on the uncertainty of the lease agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(lease_know_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				lease_know_U = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	
	/***********************************NEW**********************************/
	
	//*************************FINANCIAL***********************************/


	
	private void add_slider_Satisfaction_NF_cost_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the difference in lease cost between BEV and CI on the satisfaction of the lease agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_cost_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(5);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_cost_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Satisfaction_NF_fuel_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the difference in fuel cost between BEV and CI on the satisfaction of the lease agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(100);
		slider.setValue(new_fuel_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(10);
		slider.setPaintLabels(true);
		slider.setValue(4);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_fuel_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Satisfaction_NF_tax_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the difference in tax cost between BEV and CI on the satisfaction of the lease agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(100);
		slider.setValue(new_tax_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(10);
		slider.setPaintLabels(true);
		slider.setValue(1);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_tax_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Satisfaction_NF_knowledge_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the agents knowledge of the BEV incentives on the satisfaction of the lease agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_know_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_know_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Satisfaction_NF_incentives_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the agents knowledge of the BEV incentives on the uncertainty of the lease agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_incen_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_incen_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	
	private void add_slider_Uncertainty_NF_cost_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the difference in new cost between BEV and CI on the uncertainty of the private new agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_cost_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(8);
		slider.setPaintLabels(true);
		slider.setValue(8);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_cost_U = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Uncertainty_NF_fuel_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the difference in fuel cost between BEV and CI on the uncertainty of the private new agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_fuel_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(0);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_fuel_U = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Uncertainty_NF_tax_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the difference in tax cost between BEV and CI on the uncertainty of the private new agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_tax_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(0);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_tax_U = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Uncertainty_NF_knowledge_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the agents knowledge of the BEV incentives on the uncertainty of the private new agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_know_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(1);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_know_U = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Uncertainty_NF_incentives_diff(JFrame window) {
		JLabel label = new JLabel("Influence of the agents uncertainty about the BEV incentives on the uncertainty of the private new agents:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_incen_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(1);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_incen_U = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	
	//*************************INFRASTRUCTURE***********************************/
	
	private void add_slider_Uncertainty_NI_commute(JFrame window) {
		JLabel label = new JLabel("Uncertainty - Private new agents commute distance:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_commute_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(0);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_commute_U = slider.getValue();
			}

		});
		window.add(slider);
	}

	private void add_slider_Uncertainty_NI_parking(JFrame window) {
		JLabel label = new JLabel("Uncertainty - Private new agents parking ability:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_parking_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(0);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_parking_U = slider.getValue();
			}

		});
		window.add(slider);		
	}

	private void add_slider_Uncertainty_NI_charging(JFrame window) {
		JLabel label = new JLabel("Uncertainty - Private new agents charging ability:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_charging_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(3);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_charging_U = slider.getValue();
			}

		});
		window.add(slider);
	}

	private void add_slider_Uncertainty_NI_knowledge_BEVs(JFrame window) {
		JLabel label = new JLabel("Uncertainty - Private new agents knowledge of BEVS:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_knowledge_BEV_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(4);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_knowledge_BEV_U = slider.getValue();
			}

		});
		window.add(slider);
	}

	private void add_slider_Uncertainty_NI_technological(JFrame window) {
		JLabel label = new JLabel("Uncertainty - Private new agents technical knowledge:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_tech_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(3);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_tech_U = slider.getValue();
			}

		});
		window.add(slider);
	}

	
	private void add_slider_Satisfaction_NI_commute(JFrame window) {
		JLabel label = new JLabel("Satisfaction - Private new agents commute distance:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_commute_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(7);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_commute_S = slider.getValue();
			}

		});
		window.add(slider);
	}

	private void add_slider_Satisfaction_NI_parking(JFrame window) {
		JLabel label = new JLabel("Satisfaction - Private new agents parking ability:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_parking_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(3);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_parking_S = slider.getValue();
			}

		});
		window.add(slider);		
	}

	private void add_slider_Satisfaction_NI_charging(JFrame window) {
		JLabel label = new JLabel("Satisfaction - Private new agents charging ability:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_charging_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_charging_S = slider.getValue();
			}

		});
		window.add(slider);
	}

	private void add_slider_Satisfaction_NI_knowledge_BEVs(JFrame window) {
		JLabel label = new JLabel("Satisfaction - Private new agents knowledge of BEVS:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_knowledge_BEV_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_knowledge_BEV_S = slider.getValue();
			}

		});
		window.add(slider);
	}

	private void add_slider_Satisfaction_NI_technological(JFrame window) {
		JLabel label = new JLabel("Satisfaction - Private new agents technical knowledge:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_tech_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_tech_S = slider.getValue();
			}

		});
		window.add(slider);
	}

	
	//*************************SOCIAL***********************************/
		
	private void add_slider_Uncertainty_NS_parking_availability(JFrame window) {
		JLabel label = new JLabel("Uncertainty - Private new agents parking availability:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_parking_av_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(1);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_parking_av_U = slider.getValue();
			}

		});
		window.add(slider);
	}

	private void add_slider_Uncertainty_NS_conformity(JFrame window) {
		JLabel label = new JLabel("Uncertainty - Private new agents conformity:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_conformity_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(9);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_conformity_U = slider.getValue();
			}

		});
		window.add(slider);		
	}

	private void add_slider_Uncertainty_NS_environmental_concern(JFrame window) {
		JLabel label = new JLabel("Uncertainty - Private new agents environmental concern:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_environment_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_environment_U = slider.getValue();
			}

		});
		window.add(slider);
	}

	
	private void add_slider_Satisfaction_NS_parking_availability(JFrame window) {
		JLabel label = new JLabel("Satisfaction - Private new agents parking availability:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(100);
		slider.setValue(new_parking_av_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(10);
		slider.setPaintLabels(true);
		slider.setValue(28);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_parking_av_S = slider.getValue();
			}

		});
		window.add(slider);
	}

	private void add_slider_Satisfaction_NS_conformity(JFrame window) {
		JLabel label = new JLabel("Satisfaction - Private new agents conformity:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(new_conformity_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(7);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_conformity_S = slider.getValue();
			}

		});
		window.add(slider);		
	}

	private void add_slider_Satisfaction_NS_environmental_concern(JFrame window) {
		JLabel label = new JLabel("Satisfaction - Private new agents environmental concern:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(100);
		slider.setValue(new_environment_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(10);
		slider.setPaintLabels(true);
		slider.setValue(2);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				new_environment_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	
	/***********************************OCCASION**********************************/
	
	//*************************FINANCIAL***********************************/

	private void add_slider_Uncertainty_OF_cost_diff(JFrame window) {
		JLabel label = new JLabel("Occasion Uncertainty- Influence initial investment cost:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_cost_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(8);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_cost_U = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Uncertainty_OF_fuel_diff(JFrame window) {
		JLabel label = new JLabel("Occasion Uncertainty - Influence fuel difference cost:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_fuel_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_fuel_U = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Uncertainty_OF_tax_diff(JFrame window) {
		JLabel label = new JLabel("Occasion Uncertainty- Influence tax difference cost:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_tax_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_tax_U = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Uncertainty_OF_knowledge(JFrame window) {
		JLabel label = new JLabel("Occasion Uncertainty - Influence knowledge BEV:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_know_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(1);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_know_U = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Uncertainty_OF_incentives(JFrame window) {
		JLabel label = new JLabel("Occasion Uncertainty - Influence knowledge incentives:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_incen_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(1);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_incen_U = slider.getValue();
			}

		});
		window.add(slider);
	}
	

	
	private void add_slider_Satisfaction_OF_cost_diff(JFrame window) {
		JLabel label = new JLabel("Occasion Satisfaction- Influence initial investment cost:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_cost_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(4);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_cost_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Satisfaction_OF_fuel_diff(JFrame window) {
		JLabel label = new JLabel("Occasion Satisfaction - Influence fuel difference cost:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(100);
		slider.setValue(occ_fuel_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(10);
		slider.setPaintLabels(true);
		slider.setValue(5);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_fuel_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Satisfaction_OF_tax_diff(JFrame window) {
		JLabel label = new JLabel("Occasion Satisfaction- Influence tax difference cost:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(100);
		slider.setValue(occ_tax_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(10);
		slider.setPaintLabels(true);
		slider.setValue(1);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_tax_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Satisfaction_OF_knowledge(JFrame window) {
		JLabel label = new JLabel("Occasion Satisfaction - Influence knowledge BEV:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_know_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_know_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	private void add_slider_Satisfaction_OF_incentives(JFrame window) {
		JLabel label = new JLabel("Occasion Satisfaction - Influence knowledge incentives:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_incen_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_incen_S = slider.getValue();
			}

		});
		window.add(slider);
	}
	
	
	//*************************INFRASTRUCTURE***********************************/
	
	private void add_slider_Uncertainty_OI_age(JFrame window) {
		JLabel label = new JLabel("Uncertainty - Private new agents age vehicle:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_age_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(2);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_age_U = slider.getValue();
			}

		});
		window.add(slider);
	}

	private void add_slider_Uncertainty_OI_parking(JFrame window) {
		JLabel label = new JLabel("Uncertainty - Private new agents parking ability:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_parking_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_parking_U = slider.getValue();
			}

		});
		window.add(slider);		
	}

	private void add_slider_Uncertainty_OI_battery(JFrame window) {
		JLabel label = new JLabel("Uncertainty - Private new agents battery vehicle:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_battery_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(8);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_battery_U = slider.getValue();
			}

		});
		window.add(slider);
	}


	private void add_slider_Satisfaction_OI_age(JFrame window) {
		JLabel label = new JLabel("Satisfaction - Private new agents age vehicle:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(100);
		slider.setValue(occ_age_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(10);
		slider.setPaintLabels(true);
		slider.setValue(15);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_age_S = slider.getValue();
			}

		});
		window.add(slider);
	}

	private void add_slider_Satisfaction_OI_parking(JFrame window) {
		JLabel label = new JLabel("Satisfaction - Private new agents parking ability:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_parking_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(4);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_parking_S = slider.getValue();
			}

		});
		window.add(slider);		
	}

	private void add_slider_Satisfaction_OI_battery(JFrame window) {
		JLabel label = new JLabel("Satisfaction - Private new agents battery vehicle:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_battery_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(4);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_battery_S = slider.getValue();
			}

		});
		window.add(slider);
	}

	
	//*************************SOCIAL***********************************/
	
	private void add_slider_Uncertainty_OS_status_vehicle(JFrame window) {
		JLabel label = new JLabel("Uncertainty - Private new agents status uncertainty:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_status_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_status_U = slider.getValue();
			}

		});
		window.add(slider);
	}

	private void add_slider_Uncertainty_OS_conformity(JFrame window) {
		JLabel label = new JLabel("Uncertainty - Private new agents conformity:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_conformity_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(7);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_conformity_U = slider.getValue();
			}

		});
		window.add(slider);		
	}

	private void add_slider_Uncertainty_OS_income_acceptance(JFrame window) {
		JLabel label = new JLabel("Uncertainty - Private new agents income acceptance:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_income_acc_U);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(3);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_income_acc_U = slider.getValue();
			}

		});
		window.add(slider);
	}

	
	private void add_slider_Satisfaction_OS_status_vehicle(JFrame window) {
		JLabel label = new JLabel("Satisfaction - Private new agents status uncertainty:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_status_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(6);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_status_S = slider.getValue();
			}

		});
		window.add(slider);
	}

	private void add_slider_Satisfaction_OS_conformity(JFrame window) {
		JLabel label = new JLabel("Satisfaction - Private new agents conformity:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_conformity_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(3);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_conformity_S = slider.getValue();
			}

		});
		window.add(slider);		
	}

	private void add_slider_Satisfaction_OS_income_acceptance(JFrame window) {
		JLabel label = new JLabel("Satisfaction - Private new agents income acceptance:");
		final JSlider slider = new JSlider();
		window.add(label);
		slider.setMinimum(0);
		slider.setMaximum(10);
		slider.setValue(occ_income_acc_S);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(1);
		slider.setPaintLabels(true);
		slider.setValue(1);
		slider.addChangeListener(new ChangeListener() {


			@Override
			public void stateChanged(ChangeEvent e) {
				occ_income_acc_S = slider.getValue();
			}

		});
		window.add(slider);
	}

}
