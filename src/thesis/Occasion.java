package thesis;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import Vehicles.vehicle;
import au.com.bytecode.opencsv.CSVWriter;
import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;

/**
 * @author Dani Sibbel
 */


public class Occasion extends agents {

	/**
	 * Created agent
	 * @param context
	 * @param id
	 * @param weights_list
	 * @param age
	 * @param income
	 * @param days_working_outside_house
	 * @param education
	 * @param household_composition
	 * @param sex
	 * @param province
	 * @param a_work_situation
	 * @param a_prof_situation
	 * @param a_knowledge_BEV
	 * @param parking_home
	 * @param parking_at_work
	 * @param car_combustion_type
	 * @param car_age
	 * @param car_weight
	 * @param distance_traveled
	 * @param CO2_emissions_traveled
	 * @param CO2_emissions_per_km
	 * @param property_type
	 * @param satisfaction_commute
	 * @param reason_to_go_with_car
	 * @param knowledge_EV
	 * @param knowledge_environmental
	 * @param participation_environmental
	 * @param EV_reason
	 * @param choice_without_f_aid
	 * @param EV_positive_surprise
	 * @param EV_negative_surprise
	 * @param used_shared_car
	 * @param reason_to_use_shared_car
	 * @param reason_to_not_use_shared_car
	 * @param priv_loadPole
	 * @param happy_walk
	 * @param batt_suff
	 * @param insuf_batt
	 * @param overview_facilities_used
	 * @param pos_elec
	 * @param neg_elec
	 * @param EV_happiness
	 * @param nbr_peop_organisation
	 * @param work_incentives
	 * @param needs_charging
	 * @param home_location
	 * @param happiness
	 * @param propensity_for_buying_bev
	 * @param satisfaction_utility
	 * @param uncertainty_utility
	 * @param satisfaction_financial
	 * @param uncertainty_financial
	 * @param satisfaction_infrastructure
	 * @param uncertainty_infrastructure
	 * @param satisfaction_social
	 * @param uncertainty_social
	 * @param satisfaction_occasion_utility
	 * @param uncertainty_occasion_utility
	 * @param satisfaction_occasion_financial
	 * @param uncertainty_occasion_financial
	 * @param satisfaction_occasion_infrastructure
	 * @param uncertainty_occasion_infrastructure
	 * @param satisfaction_occasion_social
	 * @param uncertainty_occasion_social
	 * @param satisfaction_financial_lease
	 * @param uncertainty_financial_lease
	 * @param has_replaced_vehicle
	 * @param chosen_vehicle
	 */
	public Occasion(
			Context<Object> context,
			//Grid<Object> grid,
			int id,
			List<Double> weights_list,
			int age,
			int income,
			int days_working_outside_house,
			String education,
			String household_composition,
			String sex,
			String province,
			String a_work_situation,
			String a_prof_situation,
			Boolean a_knowledge_BEV,
			String parking_home,
			String parking_at_work,
			String car_combustion_type,
			int car_age,
			int car_weight,
			float distance_traveled,
			float CO2_emissions_traveled,
			float CO2_emissions_per_km,
			String property_type,
			
			
			String satisfaction_commute,
			String reason_to_go_with_car,
			int knowledge_EV,
			int knowledge_environmental,
			int participation_environmental,
			List<String> EV_reason,
			String choice_without_f_aid,
			String EV_positive_surprise,
			String EV_negative_surprise,
			String used_shared_car,
			List<String> reason_to_use_shared_car,
			List<String> reason_to_not_use_shared_car,
			Boolean priv_loadPole,
			String happy_walk,
			String batt_suff,
			String insuf_batt,
			List<String> overview_facilities_used,
			List<String> pos_elec,
			List<String> neg_elec,
			int EV_happiness,
			int nbr_peop_organisation,
			List<String> work_incentives,
			Boolean needs_charging,
			GridPoint home_location,
			double happiness,
			double propensity_for_buying_bev,
			
			double satisfaction_utility,
			double uncertainty_utility,
			double satisfaction_financial,
			double uncertainty_financial,
			double satisfaction_infrastructure, 
			double uncertainty_infrastructure,
			double satisfaction_social,
			double uncertainty_social,
			double satisfaction_occasion_utility,
			double uncertainty_occasion_utility,
			double satisfaction_occasion_financial,
			double uncertainty_occasion_financial,
			double satisfaction_occasion_infrastructure, 
			double uncertainty_occasion_infrastructure,
			double satisfaction_occasion_social,
			double uncertainty_occasion_social,
			double satisfaction_financial_lease,
			double uncertainty_financial_lease,
			Boolean has_replaced_vehicle,
			vehicle chosen_vehicle
			) {
		super(
				context,
				//grid,
				id,
				weights_list,
				age,
				income,
				days_working_outside_house,
				education,
				household_composition,
				sex,
				province,
				a_work_situation,
				a_prof_situation,
				a_knowledge_BEV,
				parking_home,
				parking_at_work,
				car_combustion_type,
				car_age,
				car_weight,
				distance_traveled,
				CO2_emissions_traveled,
				CO2_emissions_per_km,
				property_type,
				
				satisfaction_commute,
				reason_to_go_with_car,
				knowledge_EV,
				knowledge_environmental,
				participation_environmental,
				EV_reason,
				choice_without_f_aid,
				EV_positive_surprise,
				EV_negative_surprise,
				used_shared_car,
				reason_to_use_shared_car,
				reason_to_not_use_shared_car,
				priv_loadPole,
				happy_walk,
				batt_suff,
				insuf_batt,
				overview_facilities_used,
				pos_elec,
				neg_elec,
				EV_happiness,
				nbr_peop_organisation,
				work_incentives,
				needs_charging,
				home_location,
				happiness,
				propensity_for_buying_bev,
				
				satisfaction_utility,
				uncertainty_utility,
				satisfaction_financial,
				uncertainty_financial,
				satisfaction_infrastructure, 
				uncertainty_infrastructure,
				satisfaction_social,
				uncertainty_social,
				satisfaction_occasion_utility,
				uncertainty_occasion_utility,
				satisfaction_occasion_financial,
				uncertainty_occasion_financial,
				satisfaction_occasion_infrastructure, 
				uncertainty_occasion_infrastructure,
				satisfaction_occasion_social,
				uncertainty_occasion_social,
				satisfaction_financial_lease,
				uncertainty_financial_lease,
				has_replaced_vehicle,
				chosen_vehicle
				);
		this.current_car_type = "Occasion";
	}

	/**
	 * Converted agent
	 * @param context
	 * @param id
	 * @param weights_list
	 * @param age
	 * @param income
	 * @param days_working_outside_house
	 * @param education
	 * @param household_composition
	 * @param sex
	 * @param province
	 * @param a_work_situation
	 * @param a_prof_situation
	 * @param a_knowledge_BEV
	 * @param parking_home
	 * @param parking_at_work
	 * @param car_combustion_type
	 * @param car_age
	 * @param car_weight
	 * @param distance_traveled
	 * @param CO2_emissions_traveled
	 * @param CO2_emissions_per_km
	 * @param property_type
	 * @param satisfaction_commute
	 * @param reason_to_go_with_car
	 * @param knowledge_EV
	 * @param knowledge_environmental
	 * @param participation_environmental
	 * @param EV_reason
	 * @param choice_without_f_aid
	 * @param EV_positive_surprise
	 * @param EV_negative_surprise
	 * @param used_shared_car
	 * @param reason_to_use_shared_car
	 * @param reason_to_not_use_shared_car
	 * @param priv_loadPole
	 * @param happy_walk
	 * @param batt_suff
	 * @param insuf_batt
	 * @param overview_facilities_used
	 * @param pos_elec
	 * @param neg_elec
	 * @param EV_happiness
	 * @param nbr_peop_organisation
	 * @param work_incentives
	 * @param needs_charging
	 * @param home_location
	 * @param happiness
	 * @param propensity_for_buying_bev
	 * @param satisfaction_utility
	 * @param uncertainty_utility
	 * @param satisfaction_financial
	 * @param uncertainty_financial
	 * @param satisfaction_infrastructure
	 * @param uncertainty_infrastructure
	 * @param satisfaction_social
	 * @param uncertainty_social
	 * @param satisfaction_occasion_utility
	 * @param uncertainty_occasion_utility
	 * @param satisfaction_occasion_financial
	 * @param uncertainty_occasion_financial
	 * @param satisfaction_occasion_infrastructure
	 * @param uncertainty_occasion_infrastructure
	 * @param satisfaction_occasion_social
	 * @param uncertainty_occasion_social
	 * @param satisfaction_financial_lease
	 * @param uncertainty_financial_lease
	 * @param has_replaced_vehicle
	 * @param chosen_vehicle
	 * @param agent
	 */
	public Occasion(
			Context<Object> context,
			int id,
			List<Double> weights_list,
			int age,
			int income,
			int days_working_outside_house,
			String education,
			String household_composition,
			String sex,
			String province,
			String a_work_situation,
			String a_prof_situation,
			Boolean a_knowledge_BEV,
			String parking_home,
			String parking_at_work,
			String car_combustion_type,
			int car_age,
			int car_weight,
			float distance_traveled,
			float CO2_emissions_traveled,
			float CO2_emissions_per_km,
			String property_type,
			
			String satisfaction_commute,
			String reason_to_go_with_car,
			int knowledge_EV,
			int knowledge_environmental,
			int participation_environmental,
			List<String> EV_reason,
			String choice_without_f_aid,
			String EV_positive_surprise,
			String EV_negative_surprise,
			String used_shared_car,
			List<String> reason_to_use_shared_car,
			List<String> reason_to_not_use_shared_car,
			Boolean priv_loadPole,
			String happy_walk,
			String batt_suff,
			String insuf_batt,
			List<String> overview_facilities_used,
			List<String> pos_elec,
			List<String> neg_elec,
			int EV_happiness,
			int nbr_peop_organisation,
			List<String> work_incentives,
			Boolean needs_charging,
			GridPoint home_location,
			double happiness,
			double propensity_for_buying_bev,
			
			double satisfaction_utility,
			double uncertainty_utility,
			double satisfaction_financial,
			double uncertainty_financial,
			double satisfaction_infrastructure, 
			double uncertainty_infrastructure,
			double satisfaction_social,
			double uncertainty_social,
			double satisfaction_occasion_utility,
			double uncertainty_occasion_utility,
			double satisfaction_occasion_financial,
			double uncertainty_occasion_financial,
			double satisfaction_occasion_infrastructure, 
			double uncertainty_occasion_infrastructure,
			double satisfaction_occasion_social,
			double uncertainty_occasion_social,
			double satisfaction_financial_lease,
			double uncertainty_financial_lease,
			Boolean has_replaced_vehicle,
			vehicle chosen_vehicle,
			agents agent) {
		super(
				context,
				id,
				weights_list,
				age,
				income,
				days_working_outside_house,
				education,
				household_composition,
				sex,
				province,
				a_work_situation,
				a_prof_situation,
				a_knowledge_BEV,
				parking_home,
				parking_at_work,
				car_combustion_type,
				car_age,
				car_weight,
				distance_traveled,
				CO2_emissions_traveled,
				CO2_emissions_per_km,
				property_type,
				
				satisfaction_commute,
				reason_to_go_with_car,
				knowledge_EV,
				knowledge_environmental,
				participation_environmental,
				EV_reason,
				choice_without_f_aid,
				EV_positive_surprise,
				EV_negative_surprise,
				used_shared_car,
				reason_to_use_shared_car,
				reason_to_not_use_shared_car,
				priv_loadPole,
				happy_walk,
				batt_suff,
				insuf_batt,
				overview_facilities_used,
				pos_elec,
				neg_elec,
				EV_happiness,
				nbr_peop_organisation,
				work_incentives,
				needs_charging,
				home_location,
				happiness,
				propensity_for_buying_bev,
				
				satisfaction_utility,
				uncertainty_utility,
				satisfaction_financial,
				uncertainty_financial,
				satisfaction_infrastructure, 
				uncertainty_infrastructure,
				satisfaction_social,
				uncertainty_social,
				satisfaction_occasion_utility,
				uncertainty_occasion_utility,
				satisfaction_occasion_financial,
				uncertainty_occasion_financial,
				satisfaction_occasion_infrastructure, 
				uncertainty_occasion_infrastructure,
				satisfaction_occasion_social,
				uncertainty_occasion_social,
				satisfaction_financial_lease,
				uncertainty_financial_lease,
				has_replaced_vehicle,
				chosen_vehicle
				);
		this.car_combustion_type 			= agent.car_combustion_type;
		this.id 							= agent.id;
		this.age 							= agent.age; 
		this.income 						= agent.income;
		this.education 						= agent.education;
		this.household_composition 			= agent.household_composition;
		this.sex 							= agent.sex;
		this.province 						= agent.province;
		this.a_work_situation 				= a_work_situation;
		this.a_prof_situation 				= a_prof_situation;
		this.a_knowledge_BEV 				= a_knowledge_BEV;
		this.parking_home 					= parking_home;
		this.parking_at_work 				= parking_at_work;
		this.car_combustion_type 			= car_combustion_type;
		this.car_age 						= car_age;
		this.car_weight 					= car_weight;
		this.distance_traveled 				= distance_traveled;
		this.CO2_emissions_traveled			= CO2_emissions_traveled;
		this.CO2_emissions_per_km 			= CO2_emissions_per_km;
		this.property_type 					= property_type;
		
		this.satisfaction_commute 			= satisfaction_commute;
		this.reason_to_go_with_car 			= reason_to_go_with_car;
		this.knowledge_EV 					= knowledge_EV;
		this.knowledge_environmental 		= knowledge_environmental;
		this.participation_environmental	= participation_environmental;
		this.EV_reason 						= EV_reason;
		this.choice_without_f_aid 			= choice_without_f_aid;
		this.EV_positive_surprise 			= EV_positive_surprise;
		this.EV_negative_surprise 			= EV_negative_surprise;
		this.used_shared_car 				= used_shared_car;
		this.reason_to_use_shared_car 		= reason_to_use_shared_car;
		this.reason_to_not_use_shared_car 	= reason_to_not_use_shared_car;
		this.priv_loadPole 					= priv_loadPole;
		this.happy_walk 					= happy_walk;
		this.batt_suff 						= batt_suff;
		this.insuf_batt 					= insuf_batt;
		this.overview_facilities_used 		= overview_facilities_used;
		this.pos_elec 						= pos_elec;
		this.neg_elec 						= neg_elec;
		this.EV_happiness 					= EV_happiness;
		this.nbr_peop_organisation 			= nbr_peop_organisation;
		this.work_incentives 				= work_incentives;
		this.needs_charging 				= needs_charging;
		this.propensity_for_buying_bev 		= propensity_for_buying_bev;
		this.a_satisfaction_utility			= satisfaction_utility;
		this.a_uncertainty_utility			= uncertainty_utility;
		this.a_satisfaction_financial		= satisfaction_financial;
		this.a_uncertainty_financial		= uncertainty_financial;
		this.a_satisfaction_infrastructure	= satisfaction_infrastructure;
		this.a_uncertainty_infrastructure	= uncertainty_infrastructure;
		this.a_satisfaction_social			= satisfaction_social;
		this.a_uncertainty_social			= uncertainty_social;
		this.a_satisfaction_occasion_utility		= satisfaction_occasion_utility;
		this.a_uncertainty_occasion_utility 		= uncertainty_occasion_utility;
		this.a_satisfaction_occasion_financial 		= satisfaction_occasion_financial;
		this.a_uncertainty_occasion_financial 		= uncertainty_occasion_financial;
		this.a_satisfaction_occasion_infrastructure = satisfaction_occasion_infrastructure;
		this.a_uncertainty_occasion_infrastructure 	= uncertainty_occasion_infrastructure;
		this.a_satisfaction_occasion_social 		= satisfaction_occasion_social;
		this.a_uncertainty_occasion_social 			= uncertainty_occasion_social;
		this.a_satisfaction_financial_lease			= satisfaction_financial_lease;
		this.a_uncertainty_financial_lease 			= uncertainty_financial_lease;
		this.has_replaced_vehicle					= has_replaced_vehicle;
		this.a_chosen_vehicle				= chosen_vehicle;
		
		//Grid<Object> grid = (Grid<Object>) context.getProjection("parkingspacesGrid");
		//System.out.println( grid.getLocation(agent));
		//context.add(this);
		//grid.moveTo(this, grid.getLocation(agent).getX(), grid.getLocation(agent).getY());
		//System.out.println(grid.getLocation(this));
		//context.remove(agent);
		
	//	System.out.println("Added an Occasion");
	}

	/*** GET FUNCTIONS */
	
	public int getOccasion() {
		return context.getObjects(Occasion.class).size();
	}

	public int get_ages_occasion_in_simulation() {
		int car_age = (current_year - this.car_age);
		return car_age;
	}
	
	public double get_occasion_financial_uncertainty() {
		return this.a_uncertainty_occasion_financial;
	}
	
	public double get_occasion_social_uncertainty() {
		return this.a_uncertainty_occasion_social;
	}
	
	public double get_occasion_infrastructure_uncertainty() {
		return this.a_uncertainty_occasion_infrastructure;
	}
	
	public double get_occasion_financial_satisfaction() {
		return this.a_satisfaction_occasion_financial;
	}
	
	public double get_occasion_social_satisfaction() {
		return this.a_satisfaction_occasion_social;
	}
	
	public double get_occasion_infrastructure_satisfaction() {
		return this.a_satisfaction_occasion_infrastructure;
	}

	/*** PRINT FUNCTION */
	
	@ScheduledMethod(start = 1, interval = 1, priority = 8, shuffle = false)
	public void print_to_file() {
		File file = new File("Output/Values_statistics/Occasion_3.csv"); 
		
		String type = "";
		if(this instanceof Occasion)
			type = "Occasion";
		
	    try { 
	        FileWriter outputfile = new FileWriter(file, true); 
	  
	        CSVWriter writer = new CSVWriter(outputfile); 
	  
	        writer.writeNext(new String[] { 
	        		Double.toString(RunEnvironment.getInstance().getCurrentSchedule().getTickCount()),
	        		Integer.toString(this.id),
	        		type,
	        		Double.toString(this.a_satisfaction_occasion_financial),
	        		Double.toString(this.a_uncertainty_occasion_financial),
	        		Double.toString(this.a_satisfaction_occasion_infrastructure),
	        		Double.toString(this.a_uncertainty_occasion_infrastructure),
	        		Double.toString(this.a_satisfaction_occasion_social),
	        		Double.toString(this.a_uncertainty_occasion_social),
	        		Double.toString(this.happiness)}
					); 
	  
	        writer.close(); 
	    } 
	    catch (IOException e) { 
	        e.printStackTrace(); 
	    } 
	}
	
}
