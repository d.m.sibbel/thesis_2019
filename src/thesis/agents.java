package thesis;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import scenarios.scenario;
import Vehicles.Occasion_conventional;
import Vehicles.Occasion_EV;
import Vehicles.EV;
import Vehicles.Conventional;
import Vehicles.vehicle;
import au.com.bytecode.opencsv.CSVWriter;
import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.parameter.Parameters;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import scenarios.scenario;

/**
 * @author Dani Sibbel
 */

public class agents{
	
	Context<Object> context;
	Grid<Object> parkingspacesGrid;
	double nbr_agents_in_simulation	 = 200;
	public static double need_random = 0;
	public static int week 			 = 0;
	int current_year 				 =  (int) (Calendar.getInstance().get(Calendar.YEAR) + Neighbourhood.year);
	Parameters params 				 = RunEnvironment.getInstance().getParameters();
	double mrb_cost	  = mrb_value;

	/**
	 * @Parameter Personal information_____________________________________________
	 */
	protected int id, age, income, days_working_outside_house, num_cars_in_household, knowledge_EV, knowledge_environmental, participation_environmental, nbr_peop_organisation, EV_happiness;

	
	protected String 	education, household_composition, sex;
	public String province;
	protected String a_work_situation;
	protected String a_prof_situation;
	protected String parking_home;
	protected String parking_at_work;
	protected String property_type;
	protected String satisfaction_commute;
	protected String reason_to_go_with_car;
	protected String choice_without_f_aid;
	protected String EV_positive_surprise;
	protected String EV_negative_surprise;
	protected String used_shared_car;
	protected String happy_walk;
	protected String batt_suff;
	protected String insuf_batt;
	protected Boolean a_knowledge_BEV, priv_loadPole, needs_charging;
	protected float distance_traveled, CO2_emissions_traveled, CO2_emissions_per_km;
	protected List<String> EV_reason, overview_facilities_used, reason_to_use_shared_car, reason_to_not_use_shared_car, neg_elec, pos_elec, work_incentives;
	protected List<Double> weights_list;
	GridPoint home_location;
	int inkomen = this.income;
	
	/**
	 * Car information__________________________________________________
	 */
	public String current_car_type;
	protected int car_age, car_weight;
	protected String car_combustion_type;
	

	/** User Panel values **/
	
	static int subsidy_new 		= UserPanel.subsidyValueNew;
	static int subsidy_occasion = UserPanel.subsidyValueOccasion;
	static int mrb_value 		= UserPanel.mrbvalue;
	static int bpm_value 		= UserPanel.bpmvalue;
	static int diesel_tax 		= UserPanel.increaseDieselTax;
	static int gasoline_tax 	= UserPanel.increaseGasolineTax;
	
	
	//NEW, NOTE: UP_N_U_S stand for User Panel private New value for Uncertainty Social
	static double UP_N_U_S = UserPanel.uncertainty_social/10;
	static double UP_N_U_F = UserPanel.uncertainty_financial/10;
	static double UP_N_U_I = UserPanel.uncertainty_infrastructure/10;
	
	static double UP_N_S_S = UserPanel.satisfaction_social/10;
	static double UP_N_S_F = UserPanel.satisfaction_financial/10;
	static double UP_N_S_I = UserPanel.satisfaction_infrastructure/10;
	
	//LEASE
	static double UP_L_U_S = UserPanel.lease_uncertainty_social/10;
	static double UP_L_U_F = UserPanel.lease_uncertainty_financial/10;
	static double UP_L_U_I = UserPanel.lease_uncertainty_infrastructure/10;
	
	static double UP_L_S_S = UserPanel.lease_satisfaction_social/10;
	static double UP_L_S_F = UserPanel.lease_satisfaction_financial/10;
	static double UP_L_S_I = UserPanel.lease_satisfaction_infrastructure/10;
	
	//OCCASION
	static double UP_O_U_S = UserPanel.occasion_uncertainty_social/10;
	static double UP_O_U_F = UserPanel.occasion_uncertainty_financial/10;
	static double UP_O_U_I = UserPanel.occasion_uncertainty_infrastructure/10;
		
	static double UP_O_S_S	= UserPanel.occasion_satisfaction_social/10;
	static double UP_O_S_F	= UserPanel.occasion_satisfaction_financial/10;
	static double UP_O_S_I	= UserPanel.occasion_satisfaction_infrastructure/10;
	

	List<Double> lijst = new ArrayList<Double>();
	
	/*** Personality */
	
	double happiness, propensity_for_buying_bev;
	double average_EV_happiness;
	double average_non_EV_happiness;
	
	protected float ambition;
	protected float uncertainty_tolerance;
	
	/*** satisfaction and uncertainty */
	double a_satisfaction_utility, 				a_uncertainty_utility;
	double a_satisfaction_financial, 			a_uncertainty_financial;
	double a_satisfaction_infrastructure, 		a_uncertainty_infrastructure;
	double a_satisfaction_social, 				a_uncertainty_social;
	
	double a_satisfaction_occasion_utility, 			a_uncertainty_occasion_utility;
	double a_satisfaction_occasion_financial,  			a_uncertainty_occasion_financial;
	double a_satisfaction_occasion_infrastructure, 		a_uncertainty_occasion_infrastructure;
	double a_satisfaction_occasion_social, 				a_uncertainty_occasion_social;
	
	double a_satisfaction_financial_lease, 	a_uncertainty_financial_lease;

	/**
	 * 
	 * The actual costs of each of the fuel types
	 * <p>
	 * For each fuel type I used the actual costs. For the electric vehicle there are 
	 * three types of costs the ones at home, at public charging stations and at fast charging stations.
	 * <p>
	 */
	double diesel_cost				= (1.45 + (double) diesel_tax/100);
	static double gasoline_cost		= 1.69;
	double LPG_cost					= 0.79;
	double PHEV_cost				= 0.098;
	double electric_private_cost	= 0.039;
	double electric_public_cost		= 0.070;
	double electric_fast_cost		= 0.102;
	double electric_cost_average	= 0.054;
	double hydrogen_cost			=  10;

	
	
	
	/**
	 * Values for parking
	 */
	double ev_lp 					= 0.1;
	double not_need_charging_ev_lp 	= 0.03;
	double not_ev_lp 				= 0.1;
	double ev_not_lp 				= 0.016;
	double not_ev_not_lp 			= 0.0016;
	
	/**
	 * Personal update variables
	 */
	List<vehicle> affordable_bev_vehicles 				= new ArrayList<vehicle>();
	List<vehicle> affordable_conventional_vehicles 		= new ArrayList<vehicle>();
	List<vehicle> affordable_occ_bev_vehicles 			= new ArrayList<vehicle>();
	List<vehicle> affordable_occ_conventional_vehicles 	= new ArrayList<vehicle>();
	List<vehicle> affordable_lease_CI_vehicles 			= new ArrayList<vehicle>();
	List<vehicle> affordable_lease_BEV_vehicles 		= new ArrayList<vehicle>();
	
	
	protected vehicle a_chosen_vehicle;
	Boolean has_replaced_vehicle;
	boolean bev_owner_found_cs 					= true;
	boolean bev_owner_parked_correctly 			= true;
	boolean conventional_owner_parked_correctly = true;
	
	vehicle chosen_occasion_bev				= null;
	vehicle chosen_occasion_CI				= null;
	vehicle chosen_new_bev 					= null;
	vehicle chosen_new_CI			 		= null;
	vehicle chosen_lease_CI					= null;
	vehicle chosen_lease_BEV				= null;
	
	LinkedHashMap<agents, vehicle> agents_vehicles_hm 				= new LinkedHashMap<agents, vehicle>();

	
	/**
	 * Constructs the agents
	 * @param context
	 * @param id
	 * @param age
	 * @param income
	 * @param education
	 * @param household_composition
	 * @param sex
	 * @param province
	 * @param a_work_situation
	 * @param a_prof_situation
	 * @param a_knowledge_BEV
	 * @param parking_home
	 * @param parking_at_work
	 * @param car_combustion_type
	 * @param car_age
	 * @param car_weight
	 * @param distance_traveled
	 * @param CO2_emissions_traveled
	 * @param CO2_emissions_per_km
	 * @param property_type
	 * @param satisfaction_commute
	 * @param reason_to_go_with_car
	 * @param knowledge_EV
	 * @param knowledge_environmental
	 * @param participation_environmental
	 * @param EV_reason
	 * @param choice_without_f_aid
	 * @param EV_positive_surprise
	 * @param EV_negative_surprise
	 * @param used_shared_car
	 * @param reason_to_use_shared_car
	 * @param reason_to_not_use_shared_car
	 * @param priv_loadPole
	 * @param happy_walk
	 * @param batt_suff
	 * @param insuf_batt
	 * @param overview_facilities_used
	 * @param pos_elec
	 * @param neg_elec
	 * @param EV_happiness
	 * @param nbr_peop_organisation
	 * @param work_incentives
	 * @param needs_charging
	 * @param home_location
	 * @param happiness
	 * @param propensity_for_buying_bev
	 * @param satisfaction_utility
	 * @param uncertainty_utility
	 * @param satisfaction_financial
	 * @param uncertainty_financial
	 * @param satisfaction_infrastructure
	 * @param uncertainty_infrastructure
	 * @param satisfaction_financial_lease,
	 * @param uncertainty_financial_lease,
	 * @param satisfaction_social
	 * @param uncertainty_social
	 */
	public agents(
			Context<Object> context,
			int 		 id,
			List<Double> weights_list,
			int 		 age,
			int 		 income,
			int 		 days_working_outside_house,
			String 		 education,
			String 		 household_composition,
			String 		 sex,
			String 		 province,
			String 		 a_work_situation,
			String 		 a_prof_situation,
			Boolean 	 a_knowledge_BEV,
			String 		 parking_home,
			String 		 parking_at_work,
			String 		 car_combustion_type,
			int 		 car_age,
			int			 car_weight,
			float 		 distance_traveled,
			float 		 CO2_emissions_traveled,
			float 		 CO2_emissions_per_km,
			String 		 property_type,
			
			String 		 satisfaction_commute,
			String 		 reason_to_go_with_car,
			int 		 knowledge_EV,
			int 		 knowledge_environmental,
			int 		 participation_environmental,
			List<String> EV_reason,
			String 		 choice_without_f_aid,
			String 		 EV_positive_surprise,
			String 		 EV_negative_surprise,
			String 		 used_shared_car,
			List<String> reason_to_use_shared_car,
			List<String> reason_to_not_use_shared_car,
			Boolean 	 priv_loadPole,
			String 		 happy_walk,
			String 		 batt_suff,
			String 		 insuf_batt,
			List<String> overview_facilities_used,
			List<String> pos_elec,
			List<String> neg_elec,
			
			int EV_happiness,
			int nbr_peop_organisation,
			List<String> work_incentives,
			
			Boolean needs_charging,
			
			GridPoint home_location,
			double happiness,
			double propensity_for_buying_bev,
			double satisfaction_utility,
			double uncertainty_utility,
			double satisfaction_financial,
			double uncertainty_financial,
			double satisfaction_infrastructure, 
			double uncertainty_infrastructure,
			double satisfaction_social,
			double uncertainty_social,
			
			double satisfaction_occasion_utility,
			double uncertainty_occasion_utility,
			double satisfaction_occasion_financial,
			double uncertainty_occasion_financial,
			double satisfaction_occasion_infrastructure, 
			double uncertainty_occasion_infrastructure,
			double satisfaction_occasion_social,
			double uncertainty_occasion_social,
			double satisfaction_financial_lease,
			double uncertainty_financial_lease,
			Boolean has_replaced_vehicle,
			vehicle chosen_vehicle
			) {
		
		this.context = context;
		
		/*** GENERAL INFO FROM DATA */
		
		this.id 							= id;
		this.weights_list					= weights_list;
		this.age 							= age;
		this.income 						= income;
		this.days_working_outside_house		= days_working_outside_house;
		this.household_composition 			= household_composition;
		this.sex 							= sex;
		this.province 						= province;
		this.a_work_situation 				= a_work_situation;
		this.a_prof_situation 				= a_prof_situation;
		this.a_knowledge_BEV 				= a_knowledge_BEV;
		this.parking_home 					= parking_home;
		this.parking_at_work 				= parking_at_work;
		this.car_combustion_type 			= car_combustion_type;
		this.car_age 						= car_age;
		this.car_weight						= car_weight;
		this.distance_traveled 				= distance_traveled;
		this.CO2_emissions_traveled 		= CO2_emissions_traveled;
		this.CO2_emissions_per_km			= CO2_emissions_per_km;
		this.property_type 					= property_type;
		this.satisfaction_commute 			= satisfaction_commute;
		this.reason_to_go_with_car 			= reason_to_go_with_car;
		this.knowledge_EV 					= knowledge_EV;
		this.knowledge_environmental 		= knowledge_environmental;
		this.participation_environmental 	= participation_environmental;
		this.EV_reason 						= EV_reason;
		this.choice_without_f_aid 			= choice_without_f_aid;
		this.EV_positive_surprise 			= EV_positive_surprise;
		this.EV_negative_surprise 			= EV_negative_surprise;
		this.used_shared_car 				= used_shared_car;
		this.reason_to_use_shared_car 		= reason_to_use_shared_car;
		this.reason_to_not_use_shared_car 	= reason_to_not_use_shared_car;
		this.priv_loadPole 					= priv_loadPole;
		this.happy_walk 					= happy_walk;
		this.batt_suff 						= batt_suff;
		this.insuf_batt 					= insuf_batt;
		this.overview_facilities_used		= overview_facilities_used;
		this.pos_elec 						= pos_elec;
		this.neg_elec 						= neg_elec;
		this.EV_happiness					= EV_happiness;
		this.nbr_peop_organisation			= nbr_peop_organisation;
		this.work_incentives				= work_incentives;
		this.needs_charging					= needs_charging;
		
		this.home_location					= home_location;
		this.happiness						= happiness;
		
		
		this.propensity_for_buying_bev 		= propensity_for_buying_bev;
		
		
		/*** PERSONALITY */
		
		// NEW Satisfaction
		this.a_satisfaction_utility 		= satisfaction_utility;
		this.a_satisfaction_infrastructure	= satisfaction_infrastructure;
		this.a_satisfaction_financial		= satisfaction_financial;
		this.a_satisfaction_social			= satisfaction_social;
		
		// NEW Uncertainty
		this.a_uncertainty_financial		= uncertainty_financial;		
		this.a_uncertainty_infrastructure	= uncertainty_infrastructure;		
		this.a_uncertainty_social			= uncertainty_social;
		this.a_uncertainty_utility			= uncertainty_utility;
		
		// OCCASION Satisfaction
		this.a_satisfaction_occasion_utility 		= satisfaction_utility;
		this.a_satisfaction_occasion_infrastructure	= satisfaction_occasion_infrastructure;
		this.a_satisfaction_occasion_financial		= satisfaction_occasion_financial;
		this.a_satisfaction_occasion_social			= satisfaction_occasion_social;
				
		// OCCASION Uncertainty
		this.a_uncertainty_occasion_financial		= uncertainty_occasion_financial;		
		this.a_uncertainty_occasion_infrastructure	= uncertainty_occasion_infrastructure;		
		this.a_uncertainty_occasion_social			= uncertainty_occasion_social;
		this.a_uncertainty_occasion_utility			= uncertainty_occasion_utility;
		
		//LEASE Satisfaction
		this.a_satisfaction_financial_lease			= satisfaction_financial_lease;
		
		//LEASE Uncertainty
		this.a_uncertainty_financial_lease			= uncertainty_financial_lease;
		
		//ALL AGENTS
		this.has_replaced_vehicle		= has_replaced_vehicle;
		this.a_chosen_vehicle 			= chosen_vehicle;
		
	}

	//---------------------------------------------------------------
	/*** 					ACTIONS 								*/
	//---------------------------------------------------------------
	
	/**
	 * Implements the selected subsidy scenario
	 * Wherein either the subsidy is limited to 20000 or wherein the subsidy is gradually decreased until it is 0 in 2025
	 */
	@ScheduledMethod(start = 1, interval = 1, priority = 1, shuffle = true)
	public void implementation_subsidy_scenario() {

		double subsidy_used = Neighbourhood.get_used_subsidy_occasions() + Neighbourhood.get_used_subsidy_new() + Neighbourhood.get_used_subsidy_lease();
		if(scenario.is_subsidy_scenario_selected()) {
			if(scenario.is_limited_subsidy_scenario()) {
				if(subsidy_used >= 20000) {
					System.out.println("Subsidy limit reached!");
					subsidy_new 		= 0;
					subsidy_occasion 	= 0;
				}
			}
			if(scenario.is_decreased_subsidy_scenario()) {
				if(subsidy_new <= 0 && subsidy_occasion <= 0) {
					subsidy_new 		= 0;
					subsidy_occasion 	= 0;
					return;
				}
				int y = current_year - 2025;
				if(current_year >= 2025) {
						subsidy_new 		= UserPanel.subsidyValueNew - y * 800;
						subsidy_occasion 	= UserPanel.subsidyValueOccasion - y * 400;
				}
			}
		}		
	}
	
	/**
	 * Makes the agent go to work
	 * The agent will leave the spot wherein they are currently parked
	 */
	@ScheduledMethod(start = 1, interval = 1, priority = 2, shuffle = true)
	public void goToWork() {
		current_year =  (int) (Calendar.getInstance().get(Calendar.YEAR) + Neighbourhood.year);
		Grid<Object> grid = (Grid<Object>) context.getProjection("parkingspacesGrid");
		
		int width = grid.getDimensions().getWidth();
		int height = grid.getDimensions().getHeight();

		// Move to another spot which is not a ParkingSpace
		int x = RandomHelper.nextIntFromTo(0, width );
		int y = RandomHelper.nextIntFromTo(0, height );

		while (grid.getObjectAt(x, y) != null) {
			x = RandomHelper.nextIntFromTo(0, width);
			y = RandomHelper.nextIntFromTo(0, height);
		}
		GridPoint pt = grid.getLocation(this);
		for (Object obj : grid.getObjectsAt(pt.getX(), pt.getY())) {
			if (obj instanceof Parking_Space) {
				((Parking_Space) obj).setOccupied(false);
			}
		}
		return;
	}
	
	/***
	 * Parks the car according to combustion necessities. 
	 * We retrieve one day of the week wherein we observe the agents difficulty to park
	 * The agents try to park close to home within an appropriate spot. 
	 * If the agent has private parking they will park in their home location
	 */
	@ScheduledMethod(start = 1, interval = 1, priority = 4, shuffle = true)
	public void Park_Car() {
		Grid<Object> grid = (Grid<Object>) context.getProjection("parkingspacesGrid");
		if (this.parking_home == "private")
			grid.moveTo(this, this.home_location.getX(), this.home_location.getY());
		else {
			Object sp = closest_parkingSpot(grid);
			GridPoint closest_parking_space = grid.getLocation(sp);
			grid.moveTo(this, closest_parking_space.getX(), closest_parking_space.getY());
			((Parking_Space) sp).setOccupied(true);
		}
	}
		
	/**
	 * Agents will deliberate whether to substitute their old vehicle. 
	 * If it decides to substitute its vehicle it will reason about which vehicle to acquire: its ownership type and fuel type.
	 * @param car_age the age of the car someone owns
	 * @param car_combustion_type wether it is diesel, gasoline or elektrically powered
	 * @see {@link agents#calculate_type_of_fuel} and {@link agents#determine_propensity_new_car} and 
	 * {@link agents#acquire_private_new} and {@link agents#acquire_occasion} and {@link agents#acquire_lease}
	 */
	@ScheduledMethod(start = 1, interval = 1, priority = 1, shuffle = true)
	public void AcquireNewCar() {
				
		double car_age = ((double) current_year - (double)this.car_age)/10;
		double ready = this.weights_list.get(0);
		String combustion_type = null;
		
		double age_influence_on_readyness = Math.tanh(car_age);
		ready = (ready + age_influence_on_readyness)/2;
		
		if (ready > 0.99731) {
			determine_affordable_vehicles(this.income);						
			update_internal_values();
			String car_ownership_type = null;
			
			if(this.property_type.equals("Occasion")|| this.property_type.equals("occasion")) {
				combustion_type = decide_occasion_bev_conventional();
				determine_chosen_vehicle(combustion_type, this.income, "Occasion");
				car_ownership_type = "occasion";
				acquire_vehicle(this, car_ownership_type, combustion_type);
			}
			else {		
				car_ownership_type	= decide_car_ownership();
				if(car_ownership_type.equals("private_lease") || this.property_type.equals("private_lease")) {
					update_uncertainty_lease();
					update_satisfaction_lease();
					combustion_type = decide_between_lease_BEV_or_lease_CI();
					determine_chosen_vehicle(combustion_type, this.income, "private_lease");
					this.has_replaced_vehicle = true;	
				}
				else {
					combustion_type = decide_between_bev_or_conventional();
					determine_chosen_vehicle(combustion_type, this.income, "private_new");
				}
			this.has_replaced_vehicle = true;
			acquire_vehicle(this, car_ownership_type, combustion_type);
			}
			this.weights_list.set(0, (double) 0);
		}
		else {
			this.weights_list.set(0, this.weights_list.get(0) + 0.001);
		} 
	}	

	/**
	 * Makes the agent acquire a vehicle with the type of fuel it preselected in @see {@link agents#decide_between_bev_or_conventional} and the type of ownership it preselected.
	 * Updates the used subsidy @see {@link Neighbourhood#add_to_total_subsidy_lease}
	 * @param vehicle_type
	 * @param current_car_type
	 * @param car_ownership_type
	 */
	private void acquire_vehicle(agents agent, String car_ownership_type, String combustion_type) {
		if (car_ownership_type == "private_lease") {
			acquire_lease(agent, combustion_type);
			if(combustion_type.equals("Electric"))
				Neighbourhood.add_to_total_subsidy_lease(subsidy_new);
		}
		
		if(car_ownership_type == "Occasion" || car_ownership_type == "occasion") {
			acquire_occasion(agent, combustion_type);
			if(combustion_type.equals("Electric")) {
				Neighbourhood.add_to_total_subsidy_occasion(subsidy_occasion);
			}
		}
		
		if(car_ownership_type == "private_new") {
			acquire_private_new(agent, combustion_type);
			if(combustion_type.equals("Electric"))
				Neighbourhood.add_to_total_subsidy_new(subsidy_new);
		}
	}

	/**
	 * Makes the agent buy a private vehicle
	 * @param obj
	 */
	private void acquire_private_new(Object obj, String vehicle_type) { 
		Grid<Object> grid = (Grid<Object>) context.getProjection("parkingspacesGrid");
		Private_new private_new = new Private_new(context,
				id,
				weights_list,
				age,
				income,
				days_working_outside_house,
				education,
				household_composition,
				sex,
				province,
				a_work_situation,
				a_prof_situation,
				a_knowledge_BEV,
				parking_home,
				parking_at_work,
				vehicle_type,
				car_age,
				car_weight,
				distance_traveled,
				CO2_emissions_traveled,
				CO2_emissions_per_km,
				property_type,
				satisfaction_commute,
				reason_to_go_with_car,
				knowledge_EV,
				knowledge_environmental,
				participation_environmental,
				EV_reason,
				choice_without_f_aid,
				EV_positive_surprise,
				EV_negative_surprise,
				used_shared_car,
				reason_to_use_shared_car,
				reason_to_not_use_shared_car,
				priv_loadPole,
				happy_walk,
				batt_suff,
				insuf_batt,
				overview_facilities_used,
				pos_elec,
				neg_elec,
				EV_happiness,
				nbr_peop_organisation,
				work_incentives,
				needs_charging,
				home_location,
				happiness,
				propensity_for_buying_bev,
				a_satisfaction_utility,
				a_uncertainty_utility,
				a_satisfaction_financial,
				a_uncertainty_financial,
				a_satisfaction_infrastructure, 
				a_uncertainty_infrastructure,
				a_satisfaction_social,
				a_uncertainty_social,
				
				a_satisfaction_occasion_utility,
				a_uncertainty_occasion_utility,
				a_satisfaction_occasion_financial,
				a_uncertainty_occasion_financial,
				a_satisfaction_occasion_infrastructure, 
				a_uncertainty_occasion_infrastructure,
				a_satisfaction_occasion_social,
				a_uncertainty_occasion_social,
				a_satisfaction_financial_lease,
				a_uncertainty_financial_lease,
				has_replaced_vehicle,
				a_chosen_vehicle,
				(agents) obj);
		context.add(private_new);
		grid.moveTo(private_new, grid.getLocation(obj).getX(), grid.getLocation(obj).getY());
		context.remove(this);
		//System.out.println("From Occasion to Lease");
		return;
}

	/**
	 * Makes the agent acquire an occasion
	 */
	private void acquire_occasion(Object obj, String vehicle_type) { 
	Grid<Object> grid = (Grid<Object>) context.getProjection("parkingspacesGrid");
	Occasion occasion = new Occasion(context,
			id,
			weights_list,
			age,
			income,
			days_working_outside_house,
			education,
			household_composition,
			sex,
			province,
			a_work_situation,
			a_prof_situation,
			a_knowledge_BEV,
			parking_home,
			parking_at_work,
			vehicle_type,
			car_age,
			car_weight,
			distance_traveled,
			CO2_emissions_traveled,
			CO2_emissions_per_km,
			property_type,
			satisfaction_commute,
			reason_to_go_with_car,
			knowledge_EV,
			knowledge_environmental,
			participation_environmental,
			EV_reason,
			choice_without_f_aid,
			EV_positive_surprise,
			EV_negative_surprise,
			used_shared_car,
			reason_to_use_shared_car,
			reason_to_not_use_shared_car,
			priv_loadPole,
			happy_walk,
			batt_suff,
			insuf_batt,
			overview_facilities_used,
			pos_elec,
			neg_elec,
			
			EV_happiness,
			nbr_peop_organisation,
			work_incentives,
			needs_charging,
			home_location,
			happiness,
			propensity_for_buying_bev,
			a_satisfaction_utility,
			a_uncertainty_utility,
			a_satisfaction_financial,
			a_uncertainty_financial,
			a_satisfaction_infrastructure, 
			a_uncertainty_infrastructure,
			a_satisfaction_social,
			a_uncertainty_social,
			
			a_satisfaction_occasion_utility,
			a_uncertainty_occasion_utility,
			a_satisfaction_occasion_financial,
			a_uncertainty_occasion_financial,
			a_satisfaction_occasion_infrastructure, 
			a_uncertainty_occasion_infrastructure,
			a_satisfaction_occasion_social,
			a_uncertainty_occasion_social,
			a_satisfaction_financial_lease,
			a_uncertainty_financial_lease,
			has_replaced_vehicle,
			a_chosen_vehicle,
			
			(agents) obj);
	
	context.add(occasion);
	grid.moveTo(occasion, grid.getLocation(obj).getX(), grid.getLocation(obj).getY());
	context.remove(this);
	return;
}

	/**
	 * Makes the agent acquire a lease vehicle
	 * @param obj
	 */
	private void acquire_lease(Object obj, String vehicle_type) { 
		//System.out.println("we got here, vehicle: " + vehicle_type + " a_chosen_vehicle: " + a_chosen_vehicle);
	Grid<Object> grid = (Grid<Object>) context.getProjection("parkingspacesGrid");
	Lease lease = new Lease(context,
			id,
			weights_list,
			age,
			income,
			days_working_outside_house,
			education,
			household_composition,
			sex,
			province,
			a_work_situation,
			a_prof_situation,
			a_knowledge_BEV,
			parking_home,
			parking_at_work,
			vehicle_type,
			car_age,
			car_weight,
			distance_traveled,
			CO2_emissions_traveled,
			CO2_emissions_per_km,
			property_type,
			satisfaction_commute,
			reason_to_go_with_car,
			knowledge_EV,
			knowledge_environmental,
			participation_environmental,
			EV_reason,
			choice_without_f_aid,
			EV_positive_surprise,
			EV_negative_surprise,
			used_shared_car,
			reason_to_use_shared_car,
			reason_to_not_use_shared_car,
			priv_loadPole,
			happy_walk,
			batt_suff,
			insuf_batt,
			overview_facilities_used,
			pos_elec,
			neg_elec,
			
			EV_happiness,
			nbr_peop_organisation,
			work_incentives,
			needs_charging,
			home_location,
			happiness,
			propensity_for_buying_bev,
			a_satisfaction_utility,
			a_uncertainty_utility,
			a_satisfaction_financial,
			a_uncertainty_financial,
			a_satisfaction_infrastructure, 
			a_uncertainty_infrastructure,
			a_satisfaction_social,
			a_uncertainty_social,
			
			a_satisfaction_occasion_utility,
			a_uncertainty_occasion_utility,
			a_satisfaction_occasion_financial,
			a_uncertainty_occasion_financial,
			a_satisfaction_occasion_infrastructure, 
			a_uncertainty_occasion_infrastructure,
			a_satisfaction_occasion_social,
			a_uncertainty_occasion_social,
			a_satisfaction_financial_lease,
			a_uncertainty_financial_lease,
			has_replaced_vehicle,
			a_chosen_vehicle,
			
			(agents) obj);
	context.add(lease);
	grid.moveTo(lease, grid.getLocation(obj).getX(), grid.getLocation(obj).getY());
	context.remove(this);
	return;
}


	//---------------------------------------------------------------
	/*** 	UPDATE CHOSEN VEHICLE							*/
	//---------------------------------------------------------------
	
	/**
	 * Updates the values of the agents concerning their acquired vehicle 
	 * Updates the model memory on the acquire vehicle brands
	 * @param choice
	 */
	private void set_chosen_vehicle(String choice) {
		if(choice.equals("occasion_bev")) {
			this.a_chosen_vehicle 			= chosen_occasion_bev;
			this.car_age 					= this.a_chosen_vehicle.getYear();
			this.car_combustion_type 		= this.a_chosen_vehicle.getFuel_type();
			this.CO2_emissions_traveled 	= 0;
			this.CO2_emissions_per_km	 	= 0;
			this.a_knowledge_BEV 			= true;
			this.knowledge_EV				= 3;
			set_hashmap_agents_car_brands(this, this.a_chosen_vehicle);
		}
		if(choice.equals("occasion_conventional")) {
			this.a_chosen_vehicle 			= chosen_occasion_CI;
			this.car_age 					= this.a_chosen_vehicle.getYear();
			this.car_combustion_type 		= this.a_chosen_vehicle.getFuel_type();
			this.CO2_emissions_per_km	 	= (float) chosen_occasion_CI.getEmissions();
			set_hashmap_agents_car_brands(this, this.a_chosen_vehicle);
		}
		if(choice.equals("new_bev")) {
			this.a_chosen_vehicle 			= chosen_new_bev;
			this.car_age 					= current_year;
			this.car_combustion_type 		= this.a_chosen_vehicle.getFuel_type();
			this.CO2_emissions_traveled 	= 0;
			this.CO2_emissions_per_km	 	= 0;
			this.a_knowledge_BEV 			= true;
			this.knowledge_EV				= 3;
			
			String model 					= chosen_new_bev.getModel();
			
			if(model.equals("SMALL"))		this.car_weight	= 950;
			if(model.equals("MEDIUM"))		this.car_weight	= 1150;
			if(model.equals("LARGE"))		this.car_weight	= 1350;
			if(model.equals("LUXURY"))		this.car_weight	= 1550;
			if(model.equals("EXECUTIVE"))	this.car_weight	= 1600;
			
			set_hashmap_agents_car_brands(this, chosen_new_bev);
		}
		if(choice.equals("new_conventional")) {
			this.a_chosen_vehicle 			= chosen_new_CI;
			this.car_age 					= current_year;
			this.car_combustion_type 		= this.a_chosen_vehicle.getFuel_type();
			this.CO2_emissions_per_km	 	= (float) chosen_new_CI.getEmissions();
			String model 					= chosen_new_CI.getModel();
			
			if(model.equals("SMALL"))		this.car_weight	= 950;
			if(model.equals("MEDIUM"))		this.car_weight	= 1150;
			if(model.equals("LARGE"))		this.car_weight	= 1350;
			if(model.equals("LUXURY"))		this.car_weight	= 1550;
			if(model.equals("EXECUTIVE"))	this.car_weight	= 1600;
			
			set_hashmap_agents_car_brands(this, this.a_chosen_vehicle);
		}
		
		if(choice.equals("lease_bev")) {
			this.a_chosen_vehicle 			= chosen_lease_BEV;
			this.car_age 					= current_year;
			this.car_combustion_type 		= this.a_chosen_vehicle.getFuel_type();
			this.CO2_emissions_traveled 	= 0;
			this.CO2_emissions_per_km	 	= 0;
			this.a_knowledge_BEV 			= true;
			this.knowledge_EV				= 3;
		}
		if(choice.equals("lease_conventional")) {
			this.a_chosen_vehicle 			= chosen_lease_CI;
			this.car_age 					= current_year;
			this.car_combustion_type 		= this.a_chosen_vehicle.getFuel_type();
			this.CO2_emissions_per_km	 	= (float) a_chosen_vehicle.getEmissions();
		}
	}
	
	/**
	 * Determines the vehicle within the ownership type with the specified fueltype that the agent can afford.
	 * Updates the model memory @see {@link Neighbourhood#add_bev_occasion_bought} and @see {@link Neighbourhood#add_occasion_ev_buyers}
	 * @param fueltype
	 * @param income
	 * @param ownership_type
	 */
	private void determine_chosen_vehicle(String fueltype, double income, String ownership_type) {
		
		if(ownership_type.equals("Occasion")) {
			if(fueltype == "Electric") {
				chosen_occasion_bev = chosen_occasion_bev();
				set_chosen_vehicle("occasion_bev");
				Neighbourhood.add_occasion_ev_buyers(this);
				Neighbourhood.add_bev_occasion_bought(this.a_chosen_vehicle);
			}
			
			if(fueltype == "Gasoline") {
				chosen_occasion_CI 	= chosen_occasion_CI();
				set_chosen_vehicle("occasion_conventional");					
				Neighbourhood.add_occasion_conventional_buyers(this);
			}
		}
		if(ownership_type.equals("private_new")) {
			if(fueltype == "Electric") {
				chosen_new_bev = chosen_new_bev();
				set_chosen_vehicle("new_bev");
				Neighbourhood.add_ev_buyers(this);
			}
			
			if(fueltype == "Gasoline") {
				chosen_new_CI = chosen_new_CI();
				set_chosen_vehicle("new_conventional");
				Neighbourhood.add_conventional_buyers(this);
			}
		}
		
		if(ownership_type.equals("private_lease")) {
			if(fueltype == "Electric") {
				chosen_lease_BEV = chosen_lease_BEV();
				set_chosen_vehicle("lease_bev");
				Neighbourhood.add_lease_ev_buyers(this);
			}
			
			if(fueltype == "Gasoline") {
				chosen_lease_CI = chosen_lease_CI();
				set_chosen_vehicle("lease_conventional");
				Neighbourhood.add_lease_conventional_buyers(this);
			}
		}
	}

	/**
	 * Select a BEV occasion vehicle within the agents price range, with the lowest battery uncertainty.
	 * The function returns the best ranked BEV Occasion vehicle
	 * @return chosen_occasion_bev
	 */
	public vehicle chosen_occasion_bev() {
		
		Random rand = new Random();
		LinkedHashMap<vehicle, Double> hm 				= new LinkedHashMap<vehicle, Double>();
		LinkedHashMap<vehicle, Double> the_chosen_ones 	= new LinkedHashMap<vehicle, Double>();
		LinkedHashMap<vehicle, Double> vehicle_rankings = new LinkedHashMap<vehicle, Double>();
		LinkedHashMap<vehicle, Double> vehicle_ages 	= new LinkedHashMap<vehicle, Double>();
		LinkedHashMap<vehicle, Double> top_pick 		= new LinkedHashMap<vehicle, Double>();

		List<Double> uncertainties = new ArrayList<Double>();

		for (vehicle occasion_bev : affordable_occ_bev_vehicles) {

			double age_occasion = compute_age_bev_occasion(occasion_bev);
			double battery_degradation_uncertainty = compute_battery_bev_uncertainty(occasion_bev);
			double age_uncertainty = (double) 1 / 11 * age_occasion;

			hm.put((Occasion_EV) occasion_bev, battery_degradation_uncertainty);
			uncertainties.add(battery_degradation_uncertainty);
		}
		hm = sort_Hashmap(hm, false);
		int i = 0;

		Iterator<Map.Entry<vehicle, Double>> it1 = hm.entrySet().iterator();
		while (it1.hasNext() && i < 6) {
			Entry<vehicle, Double> mapEntry = it1.next();
			the_chosen_ones.put(mapEntry.getKey(), mapEntry.getValue());

			vehicle_ages.put(mapEntry.getKey(), (double) (current_year - mapEntry.getKey().getYear()));
			vehicle_rankings.put(mapEntry.getKey(), (double) mapEntry.getKey().getRanking());
			i++;
		}
		vehicle_ages = sort_Hashmap(vehicle_ages, false);
		vehicle_rankings = sort_Hashmap(vehicle_rankings, false);
		int j = 0;
		Iterator<Map.Entry<vehicle, Double>> it2 = vehicle_rankings.entrySet().iterator();
		while (it2.hasNext() && j < 3) {
			Entry<vehicle, Double> mapEntry = it2.next();
			top_pick.put(mapEntry.getKey(), (double) (current_year - mapEntry.getKey().getYear()));
			j++;
		}
		top_pick = sort_Hashmap(top_pick, false);

		Iterator<Map.Entry<vehicle, Double>> it3 = top_pick.entrySet().iterator();
		if (it3.hasNext()) {
			Entry<vehicle, Double> mapEntry = it3.next();
			chosen_occasion_bev = mapEntry.getKey();
		}
		return chosen_occasion_bev;
	}
	
	/**
	 * Select an Occasion vehicle within the agents' price range
	 * Chooses one random vehicle from amongst the vehicles within its price range
	 * @see {@link agents#determine_affordable_vehicles}
	 * @return chosen_occasion_CI
	 */
	public vehicle chosen_occasion_CI() {
		vehicle chosen_occasion_CI = null;
		Random rand = new Random();
		determine_affordable_vehicles(this.income);
		
		if(affordable_occ_conventional_vehicles.isEmpty())
			System.out.println("No affordable occasions CIs found");
		
		int x = rand.nextInt(affordable_occ_conventional_vehicles.size());
		chosen_occasion_CI = affordable_occ_conventional_vehicles.get(x);
		return chosen_occasion_CI;
	}
	
	/**
	 * Select a new BEV vehicle within the agents price range
	 * The function chooses one random vehicle from amongst the vehicles within its price range
	 * @see {@link agents#determine_affordable_vehicles}
	 * @return chosen_new_bev
	 */
	public vehicle chosen_new_bev() {
		vehicle chosen_new_bev = null;
		Random rand = new Random();
		determine_affordable_vehicles(this.income);
		
		if(affordable_bev_vehicles.isEmpty())
			System.out.println("No affrodable new BEV found, their income is: " + this.income);
		
		int x = rand.nextInt(affordable_bev_vehicles.size());
		chosen_new_bev = affordable_bev_vehicles.get(x);
		System.out.println(chosen_new_bev);
		return chosen_new_bev;
	}
	
	/**
	 * Select a new ICE vehicle within the agents price range
	 * The function chooses one random vehicle from amongst the vehicles within its price range
	 * @see {@link agents#determine_affordable_vehicles}
	 * @return chosen_new_CI
	 */
	public vehicle chosen_new_CI() {
		Random rand = new Random();
		determine_affordable_vehicles(this.income);
		
		if(affordable_conventional_vehicles.isEmpty())
			System.out.println("No affordable new CI found " + this.income);
		
		int x = rand.nextInt(affordable_conventional_vehicles.size());
		chosen_new_CI = affordable_conventional_vehicles.get(x);
		return chosen_new_CI;
	}
	
	/**
	 * Select a lease BEV vehicle within the agents price range
	 * The function chooses one random vehicle from amongst the vehicles within its price range
	 * @see {@link agents#determine_affordable_vehicles}
	 * @return chosen_lease_bev
	 */
	public vehicle chosen_lease_BEV() {
		Random rand = new Random();
		determine_affordable_vehicles(this.income);
		
		if(affordable_lease_BEV_vehicles.isEmpty())
			System.out.println("No affordable lease BEV found " + this.income);
		
		int x = rand.nextInt(affordable_lease_BEV_vehicles.size());
		chosen_lease_BEV = affordable_lease_BEV_vehicles.get(x);
		return chosen_lease_BEV;
	}
	
	/**
	 * Select a lease ICE vehicle within the agents price range
	 * The function chooses one random vehicle from amongst the vehicles within its price range
	 * @see {@link agents#determine_affordable_vehicles}
	 * @return chosen_new_CI
	 */
	public vehicle chosen_lease_CI() {
		Random rand = new Random();
		determine_affordable_vehicles(this.income);
		
		if(affordable_lease_CI_vehicles.isEmpty()) {
			System.out.println("No affordable lease CI found " + this.income + " acquiring an occasion instead");	
		}
		int x = rand.nextInt(affordable_lease_CI_vehicles.size());
		chosen_lease_CI = affordable_lease_CI_vehicles.get(x);
		return chosen_lease_CI;
	}
	
	
	public void determine_affordable_vehicles(Integer income) {
		double monthly_income = income/52;
		double cheapest_price_BEV = Double.MAX_VALUE;
		double cheapest_price_CI = Double.MAX_VALUE;
		vehicle cheapest_lease_BEV = null;
		vehicle cheapest_lease_CI = null;
		double min_affordable_price				= income * 10/100; // 10 % of ones yearly income
		double max_affordable_price				= income * 30/100; // 30 % of ones yearly income
		double max_affordable_price_BEV			= income * 45/100; // 30 % of ones yearly income
		
		//LEASE
		double min_affordable_lease_price		= monthly_income * 10/100; // 10 % of ones monthly income
		double max_affordable_lease_price		= monthly_income * 30/100; // 30 % of ones monthly income
		double max_affordable_lease_price_BEV	= monthly_income * 45/100; // 30 % of ones monthly income
		
		double price 			= 0;
		double minim_price		= 0;
		//Occasion CI
		for(Object car: context.getObjects(Occasion_conventional.class)) {
			price = ((Occasion_conventional) car).getPrice();
			if(price > min_affordable_price && price < max_affordable_price) {
				affordable_occ_conventional_vehicles.add((vehicle) car);
			}
		}
		
		//Occasion BEV
		for(Object car: context.getObjects(Occasion_EV.class)) {
			price 			= ((Occasion_EV) car).getPrice();// - subsidy_occasion;
			minim_price		= ((Occasion_EV) car).getPrice() - subsidy_occasion;
			if(price > min_affordable_price && price < max_affordable_price_BEV) {
				affordable_occ_bev_vehicles.add((vehicle) car);
			}
		}
		
		
		//new CI and Lease CI
		for(Object car: context.getObjects(Conventional.class)) {
			price = ((Conventional) car).getPrice();
			if(price > min_affordable_price && price < max_affordable_price) {
				affordable_conventional_vehicles.add((vehicle) car);
			}
			
			//LEASE
			price = ((Conventional) car).getLeasePrice();
			if(price > min_affordable_lease_price &&  price < max_affordable_lease_price) {
				affordable_lease_CI_vehicles.add((vehicle) car);
			}
			if(price < cheapest_price_CI) {
				cheapest_price_CI = price;
				cheapest_lease_CI = (vehicle) car;
			}
			
			//System.out.println("Lease price: " + lease_price + " monthly income: " + monthly_income + " 30% monthly income " + max_affordable_lease_price);
		}
		
		//new BEV and Lease BEV
		for(Object car: context.getObjects(EV.class)) {
			price 		= ((EV) car).getPrice();// - subsidy_new;
			minim_price = price - subsidy_new;
			if(minim_price > min_affordable_price && price < max_affordable_price_BEV) {
				affordable_bev_vehicles.add((vehicle) car);
			}
			
			//LEASE
			price 		= ((EV) car).getLeasePrice();// 
			minim_price = price - subsidy_new/48;
			if(minim_price > min_affordable_lease_price &&  price < max_affordable_lease_price_BEV) {
				affordable_lease_BEV_vehicles.add((vehicle) car);
				
			}
			if(price < cheapest_price_BEV) {
				cheapest_price_BEV = price;
				cheapest_lease_BEV = (vehicle) car;
			}
			
			
			//System.out.println("Monthly_income " + monthly_income + );
		}
		
		if(affordable_lease_CI_vehicles.isEmpty())		affordable_lease_CI_vehicles.add(cheapest_lease_CI);
		if(affordable_lease_BEV_vehicles.isEmpty()) 	affordable_lease_BEV_vehicles.add(cheapest_lease_BEV);
		//System.out.println("chosen BEV: " + affordable_bev_vehicles.size() + " CI: " + affordable_conventional_vehicles.size() + " occ CI: " + affordable_occ_bev_vehicles.size() + " occ CI: " + affordable_occ_conventional_vehicles.size());
	}

	/**
	 * Updates the linkedHashMap with an ordered hashMap from the smallest to the largest
	 * @param hm
	 */
	private LinkedHashMap<vehicle, Double> sort_Hashmap(LinkedHashMap<vehicle, Double> hm, Boolean yes) {

		List<Map.Entry<vehicle, Double>> entr = new ArrayList<Map.Entry<vehicle, Double>>( hm.entrySet() );
		Collections.sort(entr, new Comparator<Map.Entry<vehicle, Double>>(){
			@Override
		    public int compare(Entry<vehicle, Double> entry1, Entry<vehicle, Double> entry2) {                
		        return Double.compare(entry1.getValue(), entry2.getValue());
		    }
		});
		hm.clear();
		 
		for(Map.Entry<vehicle, Double> entry : entr){
			hm.put(entry.getKey(), entry.getValue());
		}
		
		if(yes) {
		Iterator<Map.Entry<vehicle, Double>> it = hm.entrySet().iterator();
		if(it.hasNext()) {
			Entry<vehicle, Double> mapEntry = it.next();
			vehicle key = mapEntry.getKey();
			Double value = mapEntry.getValue();
			System.out.println("value: " + value + " key: " + key);
		}
		else {
			vehicle key = null;
			Double value = 0.0;
			System.out.println("value: " + value + " key: " + key);
		}
		}
		return hm;
	}

	/*************** END ***************/
	
	
	//---------------------------------------------------------------
	/*** 	DECISION ALGORITHMS										*/
	//---------------------------------------------------------------
	
	/**
	 * Makes the agent decide between a new vehicle or an occasion vehicle dependent on their financial means and status goals
	 * @return "new" or "lease"
	 */
	private String decide_car_ownership() {
		double average_price = 0;
		for(Object v : context.getObjects(Conventional.class)) {
			average_price += ((vehicle) v).getPrice();
		}
		average_price /= context.getObjects(Conventional.class).size();
		
		if(this.property_type == "private_lease")	return "private_lease";
		if(this.income > 54000 && this.income <= 80000 && this.property_type == "private_new" && this.age < 35) 	return "private_lease";
		else  	return "private_new";													
	}
	
	/**
	 * Makes the agent decide between buying a bev or a conventional vehicle dependent on their satisfaction and their uncertainties
	 * @return "Electric" or "Gasoline"
	 */
	private String decide_between_bev_or_conventional() {
		double satisfaction_infrastructure 	= this.a_satisfaction_infrastructure;
		double satisfaction_financial		= this.a_satisfaction_financial;
		double satisfaction_social 			= this.a_satisfaction_social;
		
		double uncertainty_infrastructure 	= this.a_uncertainty_infrastructure;
		double uncertainty_financial 		= this.a_uncertainty_financial;
		double uncertainty_social 			= this.a_uncertainty_social;
		
		double satisfaction = UP_N_S_F * satisfaction_financial + UP_N_S_I * satisfaction_infrastructure + UP_N_S_S * satisfaction_social;  		
		double uncertainty	= UP_N_U_F * uncertainty_financial  + UP_N_U_I * uncertainty_infrastructure  + UP_N_U_S * uncertainty_social;
		
		//System.out.println("Satisfaction: " + satisfaction + " satisfaction_financial: " + satisfaction_financial);
		//System.out.println();
		
		if(this.car_combustion_type == "Electric") 		
			return "Electric";
		if(satisfaction > 0.5 && uncertainty < 0.5)		
			return "Electric";
		else  											
			return "Gasoline";
	}
	
	/**
	 * Makes the agent decide between buying an occasion bev or a occasion ICE vehicle dependent on their satisfaction and their uncertainties
	 * @return "Electric" or "Gasoline"
	 */
	private String decide_occasion_bev_conventional() {
		double satisfaction_occasion_infrastructure 	= this.a_satisfaction_occasion_infrastructure;
		double satisfaction_occasion_financial			= this.a_satisfaction_occasion_financial;
		double satisfaction_occasion_social 			= this.a_satisfaction_occasion_social;
		
		double uncertainty_occasion_infrastructure 	= this.a_uncertainty_occasion_infrastructure;
		double uncertainty_occasion_financial 		= this.a_uncertainty_occasion_financial;
		double uncertainty_occasion_social 			= this.a_uncertainty_occasion_social;

		double satisfaction =  UP_O_S_F * satisfaction_occasion_financial + UP_O_S_I * satisfaction_occasion_infrastructure + UP_O_S_S * satisfaction_occasion_social;	
		double uncertainty	=  UP_O_U_F * uncertainty_occasion_financial  + UP_O_U_I * uncertainty_occasion_infrastructure  + UP_O_U_S * uncertainty_occasion_social;
		
		if(this.car_combustion_type == "Electric")		return "Electric";
		if(satisfaction > 0.5 && uncertainty < 0.5) {
			return "Electric";
		}
		else {
			return "Gasoline";
		} 											
	}

	/**
	 * Makes the agent decide between buying an lease bev or a lease ICE vehicle dependent on their satisfaction and their uncertainties
	 * @return "Electric" or "Gasoline"
	 */
	private String decide_between_lease_BEV_or_lease_CI() {
		double satisfaction_infrastructure 	= this.a_satisfaction_infrastructure;
		double satisfaction_financial		= this.a_satisfaction_financial_lease;
		double satisfaction_social 			= this.a_satisfaction_social;
		
		double uncertainty_infrastructure 	= this.a_uncertainty_infrastructure;
		double uncertainty_financial		= this.a_uncertainty_financial_lease;
		double uncertainty_social 			= this.a_uncertainty_social;
		
		double satisfaction_lease   =  UP_L_U_F * satisfaction_financial + UP_L_U_I * satisfaction_infrastructure + UP_L_U_S * satisfaction_social;
		double uncertainty_lease	=  UP_L_S_F * uncertainty_financial  + UP_L_S_I * uncertainty_infrastructure  + UP_L_S_S * uncertainty_social;
		
		if(this.car_combustion_type == "Electric") 		
			return "Electric";
		if(satisfaction_lease > 0.5 && uncertainty_lease < 0.5)
			return "Electric";
		else 
			return "Gasoline";
	}
	
	
	/*************** END ***************/

	

	
	//---------------------------------------------------------------
	/*** 	UPDATE INTERNAL VALUES									*/
	//---------------------------------------------------------------
	
	/**
	 * Updates the internal values of the agents: the uncertainty and the satisfaction and each of their subcomponents
	 * @see {@link agents#update_uncertainty_new} 		and @see {@link agents#update_satisfaction_new}
	 * @see {@link agents#update_uncertainty_occasion} 	and @see {@link agents#update_satisfaction_occasion}
	 * @see {@link agents#update_uncertainty_lease} 	and @see {@link agents#update_satisfaction_lease}
	 */
	public void update_internal_values() {
		if(this.property_type.equals("private_new")) {
			update_uncertainty_new();
			update_satisfaction_new();
		}
		if(this.property_type.equals("occasion")) {
			update_uncertainty_occasion();
			update_satisfaction_occasion();
		}
		if(this.property_type.equals("private_lease")) {
			update_uncertainty_lease();
			update_satisfaction_lease();
		}
	}
	
	/*********************************NEW BUYERS******************************************************/
	
	/** 
	  * This method computes the uncertainty of each agent dependent on: infrastructure, financial and social
	  * @return the uncertainty of the agent concerning new bev's
	  */
	public void update_uncertainty_new() {
		this.a_uncertainty_infrastructure 	= compute_infrastructure_uncertainty();
		this.a_uncertainty_financial		= compute_financial_uncertainty();
		this.a_uncertainty_social			= compute_social_uncertainty();
	}
	
	/**
	 * This method computes the satisfaction of each agent dependent on: infrastructure, financial and social
	  * @return the satisfaction of the agent concerning new bev's
	 */
	public void update_satisfaction_new() {
		this.a_satisfaction_infrastructure 	= compute_infrastructure_satisfaction();
		this.a_satisfaction_financial		= compute_financial_satisfaction();
		this.a_satisfaction_social			= compute_social_satisfaction();
	}
	
	
	/*********************************OCCASION BUYERS******************************************************/
	
	/** 
	  * This method computes the uncertainty when buying an occasion of each agent dependent on: infrastructure, financial and social
	  * @return the uncertainty of the agent concerning occasion bev's
	  */
	public void update_uncertainty_occasion() {
		chosen_occasion_bev();
		if(chosen_occasion_bev == null)
			this.a_uncertainty_occasion_financial = 0.5;
		else
			this.a_uncertainty_occasion_financial	= compute_bev_occasion_financial_uncertainty(); 
		this.a_uncertainty_occasion_infrastructure 	= compute_bev_occasion_infrastructure_uncertainty();
		this.a_uncertainty_occasion_social			= compute_bev_occasion_social_uncertainty(this.chosen_occasion_bev);
	}
	
	/**
	 * This method computes the uncertainty when buying an occasion of each agent dependent on: infrastructure, financial and social
	  * @return the uncertainty of the agent concerning occasion bev's
	 */
	public void update_satisfaction_occasion() {
		chosen_occasion_bev();
		this.a_satisfaction_occasion_financial		= compute_occasion_bev_financial_satisfaction();
		this.a_satisfaction_occasion_infrastructure = compute_occasion_bev_infrastructure_satisfaction();
		this.a_satisfaction_occasion_social			= compute_occasion_bev_social_satisfaction(this.chosen_occasion_bev);
	}
	

	/*********************************LEASERS**********************************************************/
	
	/** 
	  * This method computes the uncertainty when leasing dependent on: infrastructure, financial and social
	  * @return the uncertainty of the agent concerning leasing bev's
	  */
	public void update_uncertainty_lease() {
		this.a_uncertainty_financial_lease		= compute_lease_financial_uncertainty(); 
	}
	
	/**
	 * This method computes the uncertainty when leasing dependent on: infrastructure, financial and social
	  * @return the uncertainty of the agent concerning leasing bev's
	 */
	public void update_satisfaction_lease() {
		this.a_satisfaction_financial_lease		= compute_lease_financial_satisfaction();
	}
	

	/*************** END ***************/
	
	//---------------------------------------------------------------
	/***					 FINANCIAL								*/
	//---------------------------------------------------------------
	
	/*********************************LEASE*********************************************************/
	
	/**
	 * Computes the financial satisfaction of the lease agents
	 * The satisfaction is computed using the commute costs @see {agents#compute_commute_cost_difference}, difference in taxes @see {agents#compute_difference_taxes}
	 * the difference in lease costs @see {agents#compute_difference_lease_cost_BEV_CI} and the incentives uncertainty
	 * @return the lease agents satisfaction about leasing a BEVs
	 */
	private double compute_lease_financial_satisfaction() {
		double monthly_income = this.income/12;
		double weekly_income  = ((this.income/52)*0.1);
		double lease_price_BEV_vs_CI	= compute_difference_lease_cost_BEV_CI(this);
		double cost_fuel_BEV_vs_CI 		= compute_commute_cost_difference()/weekly_income;
		double tax_considerations		= compute_difference_taxes();
		double normalized_tax			= tax_considerations/19215.0;
		double knowledge_of_financial_incentives		= compute_incentive_uncertainty();
		double satisfaction_monthly_price = (1 + Math.tanh(10 * lease_price_BEV_vs_CI))/2;
		double financial_satisfaction 	= 0.6*satisfaction_monthly_price + 0.1*cost_fuel_BEV_vs_CI + 0.2*normalized_tax + 0.1 * knowledge_of_financial_incentives;
		
		return financial_satisfaction;
	}
	
	/**
	 * Computes the financial uncertainty of the lease agents
	 * The uncertainty is computed using the lease costs @see {agents#compute_difference_lease_cost_BEV_CI} and the knowledge of financial incentives @see {agents#compute_incentive_uncertainty}
	 * @return the lease agents uncertainty about leasing a BEVs
	 */
	private double compute_lease_financial_uncertainty() {
		double monthly_income = this.income/12;
		double weekly_income = ((this.income/52)*0.1);
		double lease_price_BEV_vs_CI 		= compute_difference_lease_cost_BEV_CI(this);
		double satisfaction_monthly_price 	= (1 - Math.tanh(10 * lease_price_BEV_vs_CI))/2;
		double cost_fuel_BEV_vs_CI 			= compute_commute_cost_difference()/weekly_income;
		double tax_considerations			= compute_difference_taxes();
		double normalized_tax				= tax_considerations/19215.0;
		double knowledge_of_financial_incentives	= compute_incentive_uncertainty();
		double financial_uncertainty 			= 0.8*satisfaction_monthly_price + 0.2*knowledge_of_financial_incentives;
		
		return financial_uncertainty;
	}

	/**
	 * Determines the average normalized costs of leasing a BEV 
	 * @param agent
	 * @see {agents#km_lease_package_price}
	 * @return the normalized difference in costs
	 */
	private double compute_difference_lease_cost_BEV_CI(agents agent) {	
		double subsidy_lease	= subsidy_new/48;
		double monthly_income	= income/52;
		
		double average_price_gasoline 	= 0;		double average_price_bev 		= 0;
		double sum_lease_CI_prices		= 0;		double sum_lease_bev_prices		= 0;
		
		
		for(Object v : affordable_lease_CI_vehicles)		{ 	sum_lease_CI_prices 	+= ((vehicle) v).getLeasePrice();}
		for(Object v : affordable_lease_BEV_vehicles) 		{ 	sum_lease_bev_prices	+= ((vehicle) v).getLeasePrice();}
		
		average_price_gasoline 	= sum_lease_CI_prices/affordable_lease_CI_vehicles.size();
		average_price_bev 		= sum_lease_bev_prices/affordable_lease_BEV_vehicles.size();
		

		int monthly_cost_CI			= (int)(average_price_gasoline 				+ km_lease_package_price("Gasoline"));
		int monthly_cost_BEV		= (int)(average_price_bev - subsidy_lease 	+ km_lease_package_price("Electric")); // Volkswagen
		
		List<Integer> lijst 		= Arrays.asList(monthly_cost_BEV, monthly_cost_CI);
		double max 					= findMax(lijst);
		
		double difference 			= monthly_cost_CI - monthly_cost_BEV;
		double normalize_difference = difference/max;
		
		return normalize_difference;
	}
	
	/**
	 * Determines the monthly cost of leasing a BEV dependent on the agents monthly commute distance and fuel usage
	 * @param bev_or_CI
	 * @return cost of lease package
	 */
	private double km_lease_package_price(String bev_or_CI) {
		double km_per_month	= this.get_commute_distance()*52; 
		double cost_package = 0;
		if (bev_or_CI.equals("Electric")) {
			if (km_per_month > 10000 && km_per_month < 15001) 	cost_package = 25;
			if (km_per_month > 15000 && km_per_month < 20001) 	cost_package = 50;
			if (km_per_month > 20000 && km_per_month < 25001) 	cost_package = 75;
			if (km_per_month > 25000 && km_per_month < 30001) 	cost_package = 100;
		}
		if (bev_or_CI.equals("Gasoline")) {
			if (km_per_month > 10000 && km_per_month < 15001) 	cost_package = 20;
			if (km_per_month > 15000 && km_per_month < 20001) 	cost_package = 40;
			if (km_per_month > 20000 && km_per_month < 25001) 	cost_package = 60;
			if (km_per_month > 25000 && km_per_month < 30001) 	cost_package = 80;
		}
		return cost_package;
	}

	
	/*********************************NEW BUYS******************************************************/
	
	
	/**
	 * This function computes the financial uncertainty of the agent according to their income and their knowledge of financial incentives
	 * @see {@link agents#initial_investment_uncertainty} and {@link agents#compute_incentive_knowledge}
	 * @return financial_uncertainty
	 */
	private double compute_financial_uncertainty() {
		double difference_price_vehicles			= compute_difference_prices_conventional_bev(this, subsidy_new, "New");
		double investment_uncertainty 				= (1 - Math.tanh(20 * difference_price_vehicles + 3))/2;
		double knowledge_of_financial_incentives	= compute_incentive_uncertainty();
		double difference_commute_cost 				= -compute_commute_cost_difference()/((this.income/52)*0.1);
		double financial_uncertainty = 0.6*investment_uncertainty + 0.02*knowledge_of_financial_incentives + 0.38 * difference_commute_cost;
		
		return financial_uncertainty;
	}
	

	/**
	 * Returns whether the person is familiar with all 3 of the bev measures discussed in the data.
	 * The function returns -1 if it knows all 3 measures 1 if it knows 0 incentives. If someone owns a bev it also returns -1
	 * @return incentive_uncertainty 
	 */
	private double compute_incentive_uncertainty() {
		double incentive_uncertainty = 1-1/3*this.knowledge_EV;
		if(this.a_knowledge_BEV) incentive_uncertainty = 0;
		return incentive_uncertainty;
	}
	
	
	/**
	 * Computes the financial satisfaction dependent on the cost of a trip, the difference in TCO and the price difference between the 2 types of fuel
	 * The commute_cost and total_CO difference between conventional and bev contribute positively while the price_difference between bev and conventional contributes negatively
	 * @see {@link agents#compute_difference_prices_conventional_bev}, and {@link agents#compute_difference_TCO}, and {@link agents#compute_difference_taxes}
	 * @return financial_satisfaction
	 */
	public double compute_financial_satisfaction() {
		double difference_commute_cost 	= compute_commute_cost_difference()/((this.income/52)*0.01);
		double difference_price_vehicles= compute_difference_prices_conventional_bev(this, subsidy_new, "New");
		double investment_satisfaction	= (1 + Math.tanh(15 * difference_price_vehicles + 3))/2;
		double tax_considerations		= compute_difference_taxes();
		double normalized_tax			= tax_considerations/19215.0;
		double financial_satisfaction = 0.5*investment_satisfaction + 0.4*difference_commute_cost + 0.1*normalized_tax;

		return financial_satisfaction;
	}
	
		
	/**
	 * Computes the difference between the TCO of a conventional gasoline vehicle and a bev vehicle CO_gasoline - CO_electric
	 * The bigger the difference in costs the more satisfied someone is about acquiring a bev
	 * @see {agents#calculate_cost_of_trip} and @see {agents#get_commute_distance}
	 * @return cost_trip_comparison
	 */
	public double compute_commute_cost_difference() {	
		double nr_km_per_week = this.get_commute_distance();
		double cost_trip_comparison = calculate_cost_of_trip(nr_km_per_week, "Gasoline") - calculate_cost_of_trip(nr_km_per_week, "Electric");
		
		return cost_trip_comparison;
	}
	

	/*********************************OCCASIONS******************************************************/
	
	
	/**
	 * This function computes the financial uncertainty of the agent according to their income and their knowledge of financial incentives when buying an occasions
	 * @see {@link agents#compute_incentive_uncertainty} and {@link agents#compute_difference_prices_conventional_bev} and @see {agents#compute_difference_taxes} and @see {agents#compute_commute_cost_difference}
	 * @return financial_uncertainty
	 */
	private double compute_bev_occasion_financial_uncertainty() {
		double difference_price_vehicles		 = compute_difference_prices_conventional_bev(this, subsidy_occasion, "Occasion");
		double investment_uncertainty 			 = (1 - Math.tanh(10 * difference_price_vehicles + 1))/2;
		double knowledge_of_financial_incentives = compute_incentive_uncertainty();
		double tax_considerations				 = compute_difference_taxes();
		double normalized_tax					 = -tax_considerations/19215.0;
		double difference_commute_cost 			 = -compute_commute_cost_difference()/((this.income/52)*0.1); 
		double occasion_financial_uncertainty 	 = 0.75*investment_uncertainty + 0.1*knowledge_of_financial_incentives + 0.1*difference_commute_cost + 0.05* normalized_tax;
		
		return occasion_financial_uncertainty;
		
	}
	
	/**
	 * Computes the price discrepancy between the gasoline occasion vehicles one can afford and the bev occasions one can afford
	 * @param agent
	 * @return normalized price difference between the two fuel types
	 */
	private double compute_difference_prices_conventional_bev(agents agent, double subsidy, String occ_or_new) {
		
		double average_price_gasoline 		= 0;
		double average_price_bev 			= 0;
		double sum_conventional_prices		= 0;
		double sum_bev_prices				= 0;		
		double sum_conventional_occ_prices	= 0;		
		double sum_bev_occ_prices			= 0;
		
		for(Object v : affordable_conventional_vehicles){ 	sum_conventional_prices += ((vehicle) v).getPrice();}
		for(Object v : affordable_bev_vehicles) 		{ 	sum_bev_prices			+= ((vehicle) v).getPrice();}
		
		for(Object v : affordable_occ_conventional_vehicles){ 	sum_conventional_occ_prices += ((vehicle) v).getPrice();}
		for(Object v : affordable_occ_bev_vehicles) 		{ 	sum_bev_occ_prices			+= ((vehicle) v).getPrice();}
		
		if(occ_or_new.equals("Occasion")) {
			if(affordable_occ_conventional_vehicles.isEmpty())
				System.out.println("No affordable conventional vehicle found!!! ");
			if(affordable_occ_bev_vehicles.isEmpty())
				System.out.println("No affordable conventional vehicle found!!! ");
			
			average_price_gasoline = sum_conventional_occ_prices/affordable_occ_conventional_vehicles.size();
			average_price_bev 		= sum_bev_occ_prices/affordable_occ_bev_vehicles.size();
		}
		if(occ_or_new.equals("New")) {
			average_price_gasoline = sum_conventional_prices/affordable_conventional_vehicles.size();
			average_price_bev		= sum_bev_prices/affordable_bev_vehicles.size();
		}
		
		List<Integer> lijst 		= Arrays.asList((int) average_price_gasoline, (int) (average_price_bev - subsidy));
		double max 					= findMax(lijst);
		double difference = (average_price_gasoline - (average_price_bev - subsidy));
		double normalize_difference = difference/max;

		return normalize_difference;
	}
	
	/**
	 * Computes the financial satisfaction of the agents about occasion BEVs by evaluating TCO
	 * The commute_cost bev contributes positively while the price_difference between bev and conventional contributes negatively
	 * {@link agents#compute_commute_cost_difference}, and {@link agents#compute_difference_prices_occasion_conventional_bev} and @see {agents#compute_difference_taxes}
	 * @return financial_satisfaction
	 */
	public double compute_occasion_bev_financial_satisfaction() {
		determine_affordable_vehicles(this.income);
		vehicle chosen_occ_bev = chosen_occasion_bev();
		if(chosen_occ_bev == null)
			return 0.5;
		double difference_commute_cost 				= compute_commute_cost_difference()/((this.income/52)*0.1); 
		double difference_price_vehicles			= compute_difference_prices_conventional_bev(this, subsidy_occasion, "Occasion");
		double investment_satisfaction				= (1 + Math.tanh(8 * difference_price_vehicles + 1))/2;
		double tax_considerations					= compute_difference_taxes();
		double normalized_tax						= tax_considerations/19215.0;
		double financial_satisfaction_occasion_bev 	= 0.8*investment_satisfaction + 0.1*difference_commute_cost + 0.15*normalized_tax;
		need_random += difference_price_vehicles;
		
		return financial_satisfaction_occasion_bev;
	}
	
	
	//---------------------------------------------------------------
	/***					 INFRASTRUCTURE							*/
	//---------------------------------------------------------------

	
	/*********************************NEW BUYS******************************************************/

	/**
	 * Incorporates the 3 aspects of the infrastructure uncertainty: technological, knowledge, charging uncertainty
	 * @return infrastructure_uncertainty
	 */
	private double compute_infrastructure_uncertainty() {

		double technological_uncertainty 	= determine_uncertainty_technological_knowledge();
		double knowledge_uncertainty		= determine_knowledge_BEV();
		double charging_uncertainty			= determine_charging_uncertainty(); 
		double infrastructure_uncertainty	= 0.31 * technological_uncertainty - 0.01 * knowledge_uncertainty + 0.7 * charging_uncertainty;
		
		return infrastructure_uncertainty;
	}

	/**
	 * Takes into account the range anxiety, their technical aptitude
	 * @return technological_uncertainty
	 */
	private double determine_uncertainty_technological_knowledge() {
		double technological_uncertainty 	= 0.5;
		double average_range 				= determine_range();
		double daily_distance 				= this.distance_traveled/this.days_working_outside_house;
		double range_anxiety				= 0;
		range_anxiety = Math.tanh(3/4 * daily_distance/average_range);
		
		if(this.a_prof_situation == "Construction/industry")
			technological_uncertainty -= 0.05;
		
		if(this.a_prof_situation == "Commercial service")
			technological_uncertainty -= 0.03;
		
		return (0.4 * technological_uncertainty + 0.6 * range_anxiety);
	}

	/**
	 * Returns whether someone has ever used a bev either through a shared car or in some other way
	 * @param a_knowledge_BEV,
	 * @return 1 or 0 
	 */
	private double determine_knowledge_BEV() {
		if(this.a_knowledge_BEV)
			return 1;
		else return 0;
	}
	
	/**
	 * Gives the agents charging normalized uncertainty based on availability CS and walking distance to nearest LP
	 * if someone has their own driveway return 0.5 if someone does not have a driveway the distance to the nearest loadPole and the parking facilities at work and home are taken into account
	 * @see {@link agents#closest_lp} and {@link agents#pressure_on_lp}
	 * @return normalized_charging_uncertainty
	 */	
	public double determine_charging_uncertainty(){
		
		Grid<Object> grid 	= (Grid<Object>) context.getProjection("parkingspacesGrid");
		Object closest_lp 	= closest_lp(grid, this);
		GridPoint home 		= this.home_location;
		int pressure 		= calculate_pressure_on_lp(grid, this);
		double walking_distance = 0;
		double charging_options = 0;
		double distance_nearest_lp = grid.getDistance(home, grid.getLocation(closest_lp));
		
		
		// At home
		if(this.parking_home == "private")
			return 0.5;
		if(this.parking_home == "neighbourhood paid" || this.parking_home == "neighbourhood free")
			charging_options -= 0.01;
		
		// At work
		if(this.parking_at_work == "Paid, on private property at work" || this.parking_at_work == "Free, on private property at work")
			charging_options -= 0.02;
		
		if(this.parking_home != "private")
			walking_distance = Math.tanh(distance_nearest_lp/10);
		
		double pressure_lp = Math.tanh(pressure/3);
		double charging_uncertainty = 0.6 * pressure_lp + 0.4 * walking_distance + 0.2 * charging_options;
		double normalized_charging_uncertainty = Math.tanh(2 * charging_uncertainty);

		return normalized_charging_uncertainty;
	}

	/**
	 * Based on income determine the model of the car one can afford and take the average of their ranges.
	 * The method then returns this range
	 * @param a_income
	 * @return range
	 */
	private double determine_range() {
		double average_range = 0;
		for(vehicle v: affordable_occ_bev_vehicles) {
			average_range += ((vehicle) v).getRange();
		}
		if(affordable_occ_bev_vehicles.size() != 0)
			return average_range/affordable_occ_bev_vehicles.size();
		else 
			return 190;
	}
	
	/**
	 * Calculated the infrastructure satisfaction of the agents based on BEV and technological knoweldeg as well as the agents' parking satisfaction
	 * @see {agents#determine_knowledge_BEV}, @see {agents#determine_satisfaction_technological_knowledge}, and @see {agents#parking_satisfaction} 
	 * @return infrastructure_satisfaction
	 */
	public double compute_infrastructure_satisfaction() {
		double infrastructure_satisfaction = 0.05*determine_knowledge_BEV() + 0.45*determine_satisfaction_technological_knowledge() + 0.5*parking_satisfaction();
		return infrastructure_satisfaction;
	}

	/**
	 * Takes into account the range anxiety, their technical aptitude and the charging time of the bev
	 * @return technological_satisfaction
	 */
	private double determine_satisfaction_technological_knowledge() {
		double technological_satisfaction 	= 0.4;
		double average_range 				= determine_range();
		double daily_distance 				= this.distance_traveled/this.days_working_outside_house;
		double range_anxiety				= 0;
		
		// Range anxiety
		range_anxiety = 1 - Math.tanh(3/4 * daily_distance/average_range);
		
		if(this.a_prof_situation == "Construction/industry")
			technological_satisfaction += 0.05;
		
		if(this.a_prof_situation == "Commercial service")
			technological_satisfaction += 0.03;
		
		return (0.4 * technological_satisfaction + 0.6 * range_anxiety);
	}

	
	/**
	 * Computes the satisfaction someone has with their parking situation both at home and at work
	 * takes into account the parking pressure and walking distance to nearest LP
	 * @see {agents#closest_lp} and @see {agents#calculate_pressure_on_lp}
	 */
	public double parking_satisfaction() {
		
		Grid<Object> grid 			= (Grid<Object>) context.getProjection("parkingspacesGrid");
		Object closest_lp 			= closest_lp(grid, this);
		GridPoint home 				= this.home_location;
		double distance_nearest_lp	= grid.getDistance(home, grid.getLocation(closest_lp));
		double walking_distance 	= 0;
		double charging_options 	= 0;
		int pressure = calculate_pressure_on_lp(grid, this); 
		
		// At home
		if(this.parking_home == "private")
			return 0.4;
		if(this.parking_home == "neighbourhood paid" || this.parking_home == "neighbourhood free")
			charging_options += 0.02;
		// At work
		if(this.parking_at_work == "Paid, on private property at work" || this.parking_at_work == "Free, on private property at work")
			charging_options += 0.04;
		
		double pressure_lp = 1 - Math.tanh(pressure/6);
		if(this.parking_home != "private") 		
			walking_distance = 1 - Math.tanh(distance_nearest_lp/20);
		
		double charging_satisfaction = 0.3 * pressure_lp + 0.6 * walking_distance + 0.1 * charging_options;
		double normalized_parking_satisfaction = Math.tanh(2 * charging_satisfaction);

		return normalized_parking_satisfaction;
		
	}
	
	/*********************************OCCASIONS******************************************************/

	/**
	 * Computes the occasion agents' infrastructure uncertainty
	 * Takes into account the age, battery degradation and societal uncertainty about occasion BEVs
	 * @see {agents#compute_age_bev_occasion_uncertainty}, @see {agents#compute_battery_bev_uncertainty}, and @see {agents#compute_infrastructure_uncertainty}
	 * @return infrastrcuture_satisfaction
	 */
	private double compute_bev_occasion_infrastructure_uncertainty() {
		
		if(chosen_occasion_bev != null) {
		double uncertainty_age_chosen_vehicle	 		= compute_age_bev_occasion_uncertainty(chosen_occasion_bev);
		double battery_degradation_uncertainty 			= compute_battery_bev_uncertainty(chosen_occasion_bev);
		double standard_uncertainty						= compute_infrastructure_uncertainty();
		double infrastructure_occasion_uncertainty 		=  0.4 * uncertainty_age_chosen_vehicle + 0.6 * battery_degradation_uncertainty + 0.7 * standard_uncertainty;

		return infrastructure_occasion_uncertainty;
		}
		else return 0.5;
	}
	
	/**
	 * The degradation decreases depending on the amount of km the car has driven. After 75 000 km the battery looses approximately 5% of its capacity. The uncertainty increases dependent on someones daily commute distance
	 * and the implication the degradation has on the electric car range
	 * After 150 000 km this will be 12%, 225 000 20 %, 300 000 30% and 45% 375 000
	 * @param occasion_bev
	 * @return degradation_uncertainty
	 */
	private double compute_battery_bev_uncertainty(vehicle occasion_bev) {
		double km 				= ((Occasion_EV) occasion_bev).getKm()/1000;
		double initial_range 	= ((Occasion_EV) occasion_bev).getRange();
		double percentage_battery_degradation 	= 0.35714+0.0435*km+1.968*Math.pow(10, -4)*Math.pow(km, 2);
		double range_now		= initial_range  * (1 - percentage_battery_degradation/100);
		double daily_commute_distance = this.distance_traveled/this.days_working_outside_house;
		
		if(percentage_battery_degradation > 100)
			return 1;
		
		if(daily_commute_distance > range_now)
			return 1;
		
		double normalized_range_loss = range_now/initial_range;
		double km_of_commute_loss = (1-normalized_range_loss) * daily_commute_distance;
		double degradation_uncertainty 	= Math.tanh(km_of_commute_loss/6);
		
		return degradation_uncertainty;
	}

	/**
	 * Returns the age of the occasion vehicle
	 * @param occasion_bev
	 * @return vehicle_age
	 */
	private double compute_age_bev_occasion(vehicle occasion_bev) {
		int vehicle_age = current_year - occasion_bev.getYear();
		return vehicle_age;
	}

	/**
	 * Computes the agents uncertainty about the Occasion vehicles age dependent on its age
	 * @param chosen_occasion_bev
	 * @see {agents#compute_age_bev_occasion}
	 * @return uncertainty_age
	 */
	private double compute_age_bev_occasion_uncertainty(vehicle chosen_occasion_bev) {
		double age_vehicle = compute_age_bev_occasion(chosen_occasion_bev);
		double uncertainty_age = 1 - 1/(1+ Math.exp(age_vehicle)/100);
		
		return uncertainty_age;
	}
	
	/**
	 * The satisfaction depends on the chosen vehicle whether it is able to safely cover the commute_distance whether it is not too old as well as the standard satisfaction 
	 * @see {agents#compute_infrastructure_satisfaction}, @see {agents#compute_age_bev_occasion_satisfaction}, and @see {agents#compute_infrastructure_satisfaction}
	 * @return infrastructure_occasion_satisfaction
	 */
	public double compute_occasion_bev_infrastructure_satisfaction() {
		
		double standard_satisfaction = compute_infrastructure_satisfaction();
		vehicle chosen_occ_bev 		 = chosen_occasion_bev();
		
		if(chosen_occ_bev == null) {
			return 0;
		}
		
		double battery_satisfaction = compute_battery_bev_satisfaction(chosen_occ_bev);
		double age_satisfaction 	= compute_age_bev_occasion_satisfaction(chosen_occ_bev);
		double infrastructure_occasion_satisfaction =  0.5 * battery_satisfaction + 0.1 * age_satisfaction + 0.4*standard_satisfaction ;
		
		return infrastructure_occasion_satisfaction;
	}

	/**
	 * The degradation decreases depending on the amount of km the car has driven. After 75 000 km the battery looses approximately 5% of its capacity. The uncertainty increases dependent on someones daily commute distance
	 * and the implication the degradation has on the electric car range
	 * After 150 000 km this will be 12%, 225 000 20 %, 300 000 30% and 45% 375 000
	 * @param occasion_bev
	 * @return degradation_uncertainty
	 */
	private double compute_battery_bev_satisfaction(vehicle occasion_bev) {
		double km = ((Occasion_EV) occasion_bev).getKm()/1000;
		double daily_commute_distance 	= this.distance_traveled/this.days_working_outside_house;
		double initial_range 			= ((Occasion_EV) occasion_bev).getRange();
		double percentage_battery_degradation 	= 0.35714+0.0435*km+1.968*Math.pow(10, -4)*Math.pow(km, 2);
		double range_now				= initial_range  * (1 - percentage_battery_degradation/100);
		
		if(percentage_battery_degradation > 100)
			return 0;
		
		if(daily_commute_distance > range_now)
			return 0;
		
		double normalized_range_loss 	= range_now/initial_range;
		double km_of_commute_loss 		= (1-normalized_range_loss) * daily_commute_distance;
		double degradation_uncertainty 	= (1 - Math.tanh(km_of_commute_loss/9))/2;
		
		return degradation_uncertainty;
	}

	/**
	 * Determines the agents' satisfaction with the age of the occasion BEV vehicle
	 * @see {agents#compute_age_bev_occasion}
	 * @param chosen_occasion_bev
	 * @return satisfaction_age
	 */
	private double compute_age_bev_occasion_satisfaction(vehicle chosen_occasion_bev) {
		double age_vehicle = compute_age_bev_occasion(chosen_occasion_bev);
		double satisfaction_age = (1/(1+ Math.exp(age_vehicle)/500))/2;
		
		return satisfaction_age;
	}
	
	
	//---------------------------------------------------------------
	/***					 SOCIAL									*/
	//---------------------------------------------------------------
	
	/*********************************NEW BUYS******************************************************/	
	
	/**
	 * Updates social uncertainty composed of the social acceptance and the perceived contempt of bev owners
	 * @see {agents#get_average_BEV_uncertainty}
	 * @return social_uncertainty
	 */
	public double compute_social_uncertainty() {
		List<Double> happiness_list 		=  Neighbourhood.happines_CI_BEV;
		double total_nbr_agents				= context.getObjects(agents.class).size();		
		double diff_happiness_ci_bev		= (happiness_list.get(0) - happiness_list.get(1))*100; 
		double percentage_ev 				= (get_nbr_EV()/total_nbr_agents)*100;
		double acceptance_dependent_perc 	= 1 - Math.tanh(percentage_ev/15);
		double average_uncertainty 			= get_average_BEV_uncertainty();
		double social_uncertainty 	= 0.8 * acceptance_dependent_perc + 0.1 * diff_happiness_ci_bev + 0.1 * average_uncertainty;

		return social_uncertainty;
	}
	
	/**
	 * Determines the social satisfaction of the agent by determining the influence of others and the agents perceived parking happiness as well as the parking happiness of other BEV agents
	 * @see {agents#get_environmental_concern} and @see {agents#get_average_BEV_satisfaction}
	 * @return social_satisfaction
	 */
	public double compute_social_satisfaction() {
		List<Double> happiness_list 		= Neighbourhood.happines_CI_BEV;
		double total_nbr_new				= context.getObjects(agents.class).size();	
		double diff_happiness_bev_ci		= (happiness_list.get(1) - happiness_list.get(0))*100;
		double percentage_ev 				= (get_nbr_EV()/total_nbr_new)*100;
		double acceptance_dependent_perc 	= Math.tanh(percentage_ev/10);
		double environmental_concern 		= get_environmental_concern(this);
		double average_satisfaction			= get_average_BEV_satisfaction();
		double social_satisfaction 	= 0.7 * acceptance_dependent_perc + 0.08 * diff_happiness_bev_ci + 0.02 * environmental_concern + 0.2 * average_satisfaction;

		return social_satisfaction;
	}
	

	
	/**
	 * The concern someone has with the environment based on their participation and knowledge of environmental activities
	 * @param participation_environmental
	 * @param knowledge_environmental
	 * @return concern
	 */
	public double get_environmental_concern(agents agent) {
		double participation 	= agent.participation_environmental;
		double knowledge		= agent.knowledge_environmental;
		double concern = 0.2*knowledge/6 + 0.8*participation/10;
		
		return concern;
	}
	
	/*********************************OCCASION******************************************************/
	
	/**
	 * Determines the agents social uncertainty concerning BEVs by determining the parking happiness of BEV owners, the social acceptance of BEVs and the status of occasions
	 * @see {agents#get_amount_bev_occasions} and @see {agents#compute_social_uncertainty}
	 * @return social_uncertainty_occasion
	 */
	public double compute_bev_occasion_social_uncertainty(vehicle vehicle) {

		List<Double> happiness_list 			=  Neighbourhood.happines_CI_BEV;
		double diff_happiness_ci_bev			= (happiness_list.get(0) - happiness_list.get(1))*100;
		double nbr_bev_occasions				= get_amount_bev_occasions();
		double total_nbr_occasions				= context.getObjects(Occasion.class).size();
		double percentage_ev_occasions			= (nbr_bev_occasions/total_nbr_occasions)*100;
		double acceptance_dependent_occ_perc	= 1 - Math.tanh(percentage_ev_occasions/8);

		double social_uncertainty_occasion = 0;
		if(vehicle == null)
			return 1;
		else {
			double x = this.income/1000;
			double income_acceptance_occasion = 1 - Math.tanh(x/90);
			social_uncertainty_occasion = 0.9 * acceptance_dependent_occ_perc + 0.03 * income_acceptance_occasion + 0.07* diff_happiness_ci_bev; 
			social_uncertainty_occasion = 0.8 * social_uncertainty_occasion + 0.2 * compute_social_uncertainty();
			return social_uncertainty_occasion;
		}
	}
	
	/**
	 * Determines the agents social satisfaction concerning BEVs by determining the parking happiness of BEV owners, the social acceptance of BEVs and the status of occasions
	 * @see {agents#get_amount_bev_occasions} and @see {agents#compute_social_uncertainty}
	 * @return social_satisfaction_occasion
	 */
	public double compute_occasion_bev_social_satisfaction(vehicle vehicle) {
		List<Double> happiness_list 			=  Neighbourhood.happines_CI_BEV;
		double diff_happiness_bev_ci			= (happiness_list.get(1) - happiness_list.get(0))*100;
		double nbr_bev_occasions				= get_amount_bev_occasions();
		double total_nbr_occasions				= context.getObjects(Occasion.class).size();
		double social_satisfaction_occasions 	= 0;
		double percentage_ev_occasions			= (nbr_bev_occasions/total_nbr_occasions)*100;
		double acceptance_dependent_occ_perc	=  Math.tanh(percentage_ev_occasions/8);
		if(vehicle == null)
			return 1;
		else {
			double status_vehicle = 1 - Math.tanh(2*vehicle.getRanking()/5);
			double x = this.income/1000;
			double income_acceptance_occasion = Math.tanh(x/90);
			social_satisfaction_occasions = 0.9* acceptance_dependent_occ_perc + 0.03 * income_acceptance_occasion + 0.06 * status_vehicle + 0.01* diff_happiness_bev_ci;
			
			return social_satisfaction_occasions;
		}
	}
	

	/** END UPDATE INTERNAL VALUES */
	

	


	//---------------------------------------------------------------
	/***				COMPUTATIONS							*/
	//---------------------------------------------------------------

	/**
	 * Function of the Tax considerations the agent performs
	 * <p>
	 * Such as the MRB, BPM costs associated with their vehicle.
	 * <p>
	 * @see {@link agents#calculate_BPM_cost}, and {@link agents#calculate_MRB_cost}, and {@link agents#calculate_cost_of_trip}
	 */
	public Double compute_difference_taxes() {
		
		double diff_taxes_bpm 	= 0;
		double diff_taxes_mrb 	= 0;
		double mrb_tax_for_bev	= 0;
		double mbr_tax_gasoline = 0;
		double bpm_tax_for_bev 	= 0;
		double bpm_tax_gasoline = 0;
		
		vehicle gasoline_vehicle = null;
		String vehicle_decision  = this.property_type;

		if(vehicle_decision.equals("occasion")|| vehicle_decision.equals("private_lease")) {
			if(chosen_occasion_CI != null)
				gasoline_vehicle 	= chosen_occasion_CI;
			else {
				gasoline_vehicle 	= chosen_occasion_CI();
			}
		}
		if (vehicle_decision.equals("private_new") ){
			if(chosen_new_CI != null)
				gasoline_vehicle 	= chosen_new_CI;
			else {
				gasoline_vehicle 	= chosen_new_CI();
			}
		}
		
		//SCENARIO
		if(scenario.is_MRB_BPM_scenario_selected()) {
			diff_taxes_bpm = calculate_BPM_cost(gasoline_vehicle);
			diff_taxes_mrb = calculate_MRB_cost(gasoline_vehicle);
		}
		else { 
			if(current_year >= 2025) 
				bpm_tax_for_bev = 360;
			
			bpm_tax_gasoline	= calculate_BPM_cost(gasoline_vehicle);
			mbr_tax_gasoline 	= calculate_MRB_cost(gasoline_vehicle);
		
			if(current_year == 2025) 
				mrb_tax_for_bev = mbr_tax_gasoline * 25/100;
			
			if(current_year == 2026) 
				mrb_tax_for_bev = mbr_tax_gasoline * 75/100;
			
			if(current_year > 2026) 
				mrb_tax_for_bev = mbr_tax_gasoline;
			
			diff_taxes_mrb = mbr_tax_gasoline - mrb_tax_for_bev;
			diff_taxes_bpm = bpm_tax_gasoline - bpm_tax_for_bev;
		}

		return (diff_taxes_bpm + diff_taxes_mrb);
	}
	
	/**
	 * Depending on the car CO2 emissions per km and the type of fuel it consumes the BPM is computed.
	 * @param CO2 the amount of CO2 the vehicle emits per km
	 * @return The actual cost of BPM tax 
	 */
	public double calculate_BPM_cost(vehicle vehicle) {
		if (vehicle.getFuel_type() == "PHEV") {
			int[] I 	= new int[] {0, 30, 50};
			int[] IV 	= new int[] {27, 113, 271};
			int[] III 	= new int[] {0, 810, 3070};
			
			int i = 0;
			double result = 0;
			double CO2 = vehicle.getEmissions();
			
			
			if (CO2 < 30)
				i = 0;
			if (CO2 >= 30 && CO2 < 50)
				i = 1;
			if (CO2 >= 50)
				i = 2;
			
			result = (CO2 - I[i])*(IV[i]) + III[i];
			return result;
		}
		int[] I 	= new int[] {0, 71, 95, 139, 156};
		int[] IV 	= new int[] {2, 60, 131, 215, 429};
		int[] III 	= new int[] {366, 502, 1942, 7706, 11361};
		
		
	    for(int i = 0; i<IV.length ; ++i)
	    	IV[i]=IV[i] + bpm_value;
	    
		int i = 0;
		double result = 0;
		double diesel_toeslag = 88.43;
		double CO2 = vehicle.getEmissions();  
		
		
		if (CO2 <71)
			i = 0;
		if (CO2 >= 71 && CO2 < 95)
			i = 1;
		if (CO2 >= 95 && CO2 < 139)
			i = 2;
		if (CO2 >= 139 && CO2 < 156)
			i = 3;
		if (CO2 >= 156)
			i = 4;
		
		if (vehicle.getFuel_type().equals("Gasoline")) {
			result = (CO2 - I[i])*(IV[i]) + III[i];
		}
		
		if (vehicle.getFuel_type().equals("Diesel")) {
			double toeslag = (CO2 - 59)*diesel_toeslag;
			result = (CO2 - I[i])*(IV[i]) + III[i] + toeslag;
		}
		return result;

	}
	
	/**
	 * Calculates the MRB tax an individual has to pay according to the province they live in 
	 * @return The MRB tax
	 */
	public double calculate_MRB_cost(vehicle vehicle) {
		double mrb = 0.0;
		
		List<String> mrb_gasoline 	= EnvironmentBuilder.mrb_gasoline_list;
		
		List<String> provincies = Arrays.asList("Drenthe", "Flevoland", "Friesland", "Gelderland", "Groningen", "Limburg", "Noord-Brabant", "Noord-Holland", "Overijssel", "Utrecht", "Zeeland", "Zuid-Holland");
		
		int i = provincies.indexOf(this.province);
			
		if(vehicle.getFuel_type().equals("Gasoline"))
			mrb = Integer.parseInt(mrb_gasoline.get(i))	+ mrb_cost;
		return (mrb)+ mrb_value;
	}
	
	/**
	 * Determines the propensity someone has to lease or buy a new or occasion vehicle
	 * @param car_age
	 * @param income
	 * @param current_car_type
	 * @return type_owner which is either occasion, lease and priv_new
	 */
	public String calculate_propensity_new_car(int car_age, int income, String current_car_type) {
		
		 double priv_new 	= 0;
		 double occasion 	= 0;
		 double lease	 	= 0;                                                                                                                                                                                                                        
		 String type_owner	= null;
		 
		 if (income > 24250) {
			 priv_new += 0.12;
			 occasion += 0.43;
			 lease	  += 0.1;
		 }
		 else {
			 occasion += 0.2;
			 priv_new -= 0.1;
			 lease	  -= 0.05;
		 }
		 
		 if (this.age > 27) {
			 priv_new 	+= 0.2;
			 lease		+= 0.1;
		 }
		 
		 if (this.a_work_situation == "fulltime/parttime") {
			 priv_new	+= 0.3;
			 lease		+= 0.2;
			 occasion	+= 0.05;
		 }
		 
		
		 if(car_age > 8)
			  current_car_type = "Occasion";
		 else current_car_type = "Private_new";
		 

		 if (this.current_car_type == "Private_lease" || this.current_car_type == "Company_lease")
			 lease += 0.2;

		 List<Double> ratios = Arrays.asList(occasion, lease , priv_new);
		 double max = 0;
		 int u = 0;
		 for (int i = 0; i < ratios.size(); i++) 
             if (ratios.get(i) > max) {
            	 max = ratios.get(i);
            	 u += i;
             }
              if (u == 0) type_owner = "Occasion" ;
              if (u == 1) type_owner = "Lease";
              if (u == 2) type_owner = "Private_new";
              
		 return type_owner;
	}

	/**
	 * Determines whether someone buys a conventional or a bev car
	 * @param inkomen
	 * @see {@link agents#calculate_gasoline_vs_diesel}
	 * @return combustion_type of their choice
	 */
	public String calculate_type_of_fuel(int inkomen) {
		
		String combustion_type 	= null;
		double propensity_EV 	= this.propensity_for_buying_bev;
		String fuel				= calculate_gasoline_vs_diesel(inkomen);
		
		double i = new Random().nextDouble();
		
		if(propensity_EV > i) 
			combustion_type = "Electric";	
		else 
			combustion_type = fuel;
		return combustion_type;
	}

	/**
	 * Determine whether someone will buy a gasoline or a diesel vehicle
	 * @param inkomen
	 * @see {@link agents#calculate_trip_cost_gasoline} and @see {@link agents#calculate_trip_cost_diesel}
	 * @return combustion type of their choice
	 */
	public String calculate_gasoline_vs_diesel(int inkomen) {
		double propensity 		= 0.5;
		

		double 	distance 	= get_commute_distance();
		
		double cost_on_gasoline 	= calculate_trip_cost_gasoline(distance);
		double cost_on_diesel		= calculate_trip_cost_diesel(distance);
		
		String combustion = this.car_combustion_type;
		// mensen houden rekening met het omslagpunt tussen diesel en gasoline
		// 13.500 voor lichtere autos en 14.500 voor zwaardere autos

		if(cost_on_gasoline < cost_on_diesel)	propensity += 0.4; 
		else 									propensity -= 0.3;							
		//System.out.println(cost_on_gasoline + " ; " + cost_on_diesel);
		double i = new Random().nextDouble();
		//System.out.println(propensity);
		if (i < propensity) 
			combustion 	= "Diesel";
		else
			combustion 	= "Gasoline";
		return combustion;
	}
	
	/**
	 * Determines the cost of a trip for each of the different combustion types
	 * @see {@link agents#calculate_trip_cost_diesel} and {@link agents#calculate_trip_cost_gasoline} and {@link agents#calculate_trip_cost_LPG}
	 * and {@link agents#calculate_trip_cost_PHEV} and {@link agents#calculate_trip_cost_Electric} and {@link agents#calculate_trip_cost_Hydrogen}
	 * @param nr_km_per_week
	 * @return
	 */
	public double calculate_cost_of_trip(double nr_km_per_week, String fuel_type_of_car) {
		double fuel_cost_trip = 0;
		
		if (fuel_type_of_car == "Diesel")
			fuel_cost_trip = calculate_trip_cost_diesel(nr_km_per_week);
	
		if (fuel_type_of_car == "Gasoline")
			fuel_cost_trip = calculate_trip_cost_gasoline(nr_km_per_week);
	
		if (fuel_type_of_car == "LPG")
			fuel_cost_trip = calculate_trip_cost_LPG(nr_km_per_week);
	
		if (fuel_type_of_car == "PHEV")
			fuel_cost_trip = calculate_trip_cost_PHEV(nr_km_per_week);
	
		if (fuel_type_of_car == "Electric")
			fuel_cost_trip = calculate_trip_cost_Electric(nr_km_per_week);
	
		if (fuel_type_of_car == "Hydrogen")
			fuel_cost_trip = calculate_trip_cost_Hydrogen(nr_km_per_week);
		
		return fuel_cost_trip;
	}
	
	/**
	 * Determines an approximation of the cost of the trip with a gasoline vehicle
	 * @param nr_km_per_week
	 * @return fuel_cost_trip
	 */
	public double calculate_trip_cost_gasoline(double nr_km_per_week){
		double fuel_cost_trip	= 0;
		scenario.is_fuel_tax_increase_selected();//SCENARIO
		double gasoline = gasoline_cost + Neighbourhood.gasoline_increase;
		if (this.car_weight <= 1200)
				fuel_cost_trip		= gasoline * nr_km_per_week/13;
		else 	fuel_cost_trip		= gasoline * nr_km_per_week/11;
	
		return fuel_cost_trip;
	}

	/**
	 * Determines an approximation of the cost of the trip with a diesel vehicle
	 * @param nr_km_per_week
	 * @return fuel_cost_trip
	 */
	public double calculate_trip_cost_diesel(double nr_km_per_week) {
		double fuel_cost_trip	= 0;
		
		if (this.car_weight <= 1300)
			 fuel_cost_trip		= diesel_cost * nr_km_per_week/19;
		else fuel_cost_trip		= diesel_cost * nr_km_per_week/16;

		return fuel_cost_trip;
	}
		
	/**
	 * Determines an approximation of the cost of the trip with a LPG vehicle
	 * @param nr_km_per_week
	 * @return fuel_cost_trip
	 */
	public double calculate_trip_cost_LPG(double nr_km_per_week) {
		double fuel_cost_trip		= LPG_cost * nr_km_per_week/10;
		return fuel_cost_trip;
	}

	/**
	 * Determines an approximation of the cost of the trip with a PHEV vehicle
	 * @param nr_km_per_week
	 * @return fuel_cost_trip
	 */
	public double calculate_trip_cost_PHEV(double nr_km_per_week) {
		double fuel_cost_trip		= PHEV_cost * nr_km_per_week;
		return fuel_cost_trip;
	}

	/**
	 * Determines an approximation of the cost of the trip with a Hydrogen vehicle
	 * @param nr_km_per_week
	 * @return fuel_cost_trip
	 */
	public double calculate_trip_cost_Hydrogen(double nr_km_per_week) {
		double fuel_cost_trip		= hydrogen_cost * nr_km_per_week*5.6/600;
		return fuel_cost_trip;
	}

	/**
	 * Determines an approximation of the cost of the trip with a Electric vehicle with the type of charging the agent will have available
	 * @param nr_km_per_week
	 * @return fuel_cost_trip
	 */
	public double calculate_trip_cost_Electric(double nr_km_per_week) {
		
		int amount_home 	= 0;
		int amount_work 	= 0;
		int amount_public 	= 0;
		int amount_fast 	= 0;
		int amount_other	= 0;
		
		for (String i : overview_facilities_used) {
			String[] nmbr = i.split(";");
		
			if (nmbr[0].equals("Own private loadPole")) 	amount_home 	= Integer.parseInt(nmbr[1]);
			
			if (nmbr[0].equals("Public loadPole")) 			amount_public 	= Integer.parseInt(nmbr[1]);
			
			if (nmbr[0].equals("Fast charger on highway")) 	amount_fast 	= Integer.parseInt(nmbr[1]);
			
			if (nmbr[0].equals("At work")) 					amount_work 	= Integer.parseInt(nmbr[1]);
			
			if (nmbr[0].equals("Other")) 					amount_other 	= Integer.parseInt(nmbr[1]);
			
		}
		double fuel_cost_trip = 
			   (amount_home 	* electric_private_cost +
				amount_work 	* electric_private_cost +
				amount_fast 	* electric_fast_cost 	+
				amount_public 	* electric_public_cost	+
				amount_other 	* electric_cost_average)/100 * nr_km_per_week;
		
		return 0.054 * nr_km_per_week;
	}

	/**
	 * This method returns the number of bevs dependent on a the loadPole nearest to a particular agent
	 * @param grid
	 * @see {@link agents#closest_lp}
	 * @return number_of_bev_dependent_on_this_lp
	 */
	public int calculate_pressure_on_lp(Grid<Object> grid, agents person) {
		List<Object> list_of_dependent_bev = new ArrayList<Object>();
		for(Object agent : context.getObjects(agents.class)) {
			if (((agents) agent).car_combustion_type.equals("Electric")) {
				Object lp = closest_lp(grid, agent);
				list_of_dependent_bev.add(lp);
			}
		}
		Object lp = closest_lp(grid, person);
		int number_of_bev_dependent_on_this_lp = Collections.frequency(list_of_dependent_bev, lp);
		return number_of_bev_dependent_on_this_lp;
	}
	
	/**
	 * The happiness of each bev owner is between 1 and 10 and is obtained from the data
	 * @return average_happiness
	 */
	public int calculate_average_EV_happiness() {
		int average_happiness = 0;
		List<Integer> happiness_list = new ArrayList<Integer>();
		for(Object obj : context.getObjects(agents.class)) {
				int a = ((agents) obj).EV_happiness;
				average_happiness += a;
				if (a != 0) {
					happiness_list.add(a);
				}
		}
		return average_happiness;
	}
	
	//---------------------------------------------------------------
	/*** 					PARKING 								*/
	//---------------------------------------------------------------
		
	/**
	 * Determines the closest non occupied parkingSpot dependent on the combustion type of the vehicle the agent owns
	 * @returns closest_parking_sp
	 */
	public Object closest_parkingSpot(Grid<Object> grid) {
		
		Object closest_parking_sp = null;
		
		GridPoint home 					= this.home_location;
		double minDistance 				= Double.MAX_VALUE;
		double park_distance_to_home 	= 0;
		
		List<Object> empty_parking_spaces 		= new ArrayList<Object>();
		List<Object> empty_lp_parking_spaces 	= new ArrayList<Object>();
		// Get list of all empty parking spaces
		for (Object obj : context.getObjects(Parking_Space.class)) {
			if (!((Parking_Space) obj).getOccupied()) {
				empty_parking_spaces.add(obj);
				if (((Parking_Space) obj).getType() == "with_loadPole")
					empty_lp_parking_spaces.add(obj);
			}
		}
			
		// If you have an electric car or other parking spots are full
		if((this.car_combustion_type == "Electric" && this.needs_charging && !empty_lp_parking_spaces.isEmpty())||
				(this.car_combustion_type != "Electric" && empty_parking_spaces.isEmpty()	)) {
			for(Object lp_ps : empty_lp_parking_spaces) {
				park_distance_to_home = grid.getDistance(home, grid.getLocation(lp_ps));
				if(park_distance_to_home < minDistance) {
				minDistance	= park_distance_to_home;
				closest_parking_sp = lp_ps;
				this.needs_charging = false;
				}
			}
		}
		else {
			for(Object ps : empty_parking_spaces) {
			park_distance_to_home = grid.getDistance(home, grid.getLocation(ps));
				if(park_distance_to_home < minDistance) {
					minDistance	= park_distance_to_home;
					closest_parking_sp = ps;
				}
			}
		}

		this.happiness = update_happiness(grid, closest_parking_sp);
		return closest_parking_sp;
	}

	/**
	 * Determines the closest parking spot with charging availability, and returns the closest lp to someone's home independent on the combustion type of someone's vehicle
	 * @param grid, agent
	 * @return closest_parking_lp
	 */
	public Object closest_lp(Grid<Object> grid, Object agent) {

		Object closest_parking_lp = null;
		double minDistance = Double.MAX_VALUE;
		double park_distance_to_home = 0;
		GridPoint home = ((agents) agent).home_location;
		
		for (Object lp : context.getObjects(Parking_Space.class)) {
			if (((Parking_Space) lp).getType().equals("with_loadPole")) {
				park_distance_to_home = grid.getDistance(home, grid.getLocation(lp));

				if (park_distance_to_home < minDistance) {
					minDistance = park_distance_to_home;
					closest_parking_lp = lp;
				}
			}
		}
		return closest_parking_lp;
	}
	
	/**
	 * Updates the happiness according to their charging necessity and the distance the agent has to walk from their parking spot to their home
	 * @param home_location and @param happiness
	 * @return new_happiness
	 */	
	public double update_happiness(Grid<Object> grid, Object ps) {
		double new_happiness 		= this.happiness;
		GridPoint parking_location 	= grid.getLocation(ps);
		GridPoint home_location 	= this.home_location;
		
		double park_distance 	= grid.getDistance(home_location, parking_location);
		double max_distance 	= grid.getDimensions().getWidth();
		boolean lp = false;
		boolean ev = false;
		
		if(this.car_combustion_type.equals("Electric") && this.parking_home.equals("private"))
			return 1;

		new_happiness = 0.5*Math.sin(Math.PI/max_distance*(park_distance +(max_distance/2)));

		if(((Parking_Space) ps).getType() == "with_loadPole")
			lp = true;
		if(((Parking_Space) ps).getType() != "with_loadPole")
			lp = false;
		if(this.car_combustion_type == "Electric" )
			ev = true;
		if(this.car_combustion_type == "Gasoline" )
			ev = false;
			
		if(ev && lp && this.needs_charging) {
			new_happiness += ev_lp;
			bev_owner_found_cs = true;
			}
		if(ev && lp && !this.needs_charging) {
			new_happiness -= not_need_charging_ev_lp;
			bev_owner_found_cs = false;
			}
		if(ev && !lp && this.needs_charging) {
			new_happiness -= ev_not_lp;
			bev_owner_found_cs = false; 
			}
		if(ev && !lp && !this.needs_charging) {
			new_happiness += not_ev_not_lp;
			bev_owner_found_cs = true; 
			}
		if(!ev && !lp) {
			new_happiness += not_ev_not_lp;
			conventional_owner_parked_correctly = true;
			}
		if(!ev && lp) {
			new_happiness -= not_ev_lp;
			conventional_owner_parked_correctly = false;
		}

		return new_happiness;
	}

	
	//---------------------------------------------------------------
	/*** 					GET FUNCTIONS							*/
	//---------------------------------------------------------------
	

	public double get_commute_distance(){
		return this.distance_traveled;
	}
	
	public String get_car_combustion_type() {
		return this.car_combustion_type;
	}
	
	public List<String> get_overview_facilities_used() {
		return this.overview_facilities_used;
	}
	
	public double get_happiness() {
		if (this.car_combustion_type.equals("Gasoline"))
			return this.happiness;
		return 0.5;
	}
	
	public double get_EV_Happiness() {
		if (this.car_combustion_type.equals("Electric")) {
			return this.happiness;
		}
		else return get_average_happiness().get(1);
		
	}
	
	public double get_non_EV_Happiness() {
		if (this.car_combustion_type.equals("Gasoline")) {
			return this.happiness;
			}
		else
			return get_average_happiness().get(0);
	}
	
	public List<Double> get_average_happiness(){
		double averg_CI 	= 0;
		double averg_BEV 	= 0;
		
		for(Object a : context.getObjects(agents.class)) {
			if(((agents) a).car_combustion_type.equals("Electric")) {
				averg_BEV += ((agents) a).happiness;
			}
			if(((agents) a).car_combustion_type.equals("Gasoline")) {
				averg_CI += ((agents) a).happiness;
			}
		}
		List<Double> happiness_list = Arrays.asList(averg_CI/get_nbr_nonEV(), averg_BEV/get_nbr_EV());
		return happiness_list;
	}
	
	@ScheduledMethod(start = 1, interval = 1, priority = 2, shuffle = true)
	public void update_average_happiness() {
		
		if (this.car_combustion_type.equals("Electric")) {		Neighbourhood.happiness_BEV_over_time.add(this.happiness);	}
		if (this.car_combustion_type.equals("Gasoline")) { 		Neighbourhood.happiness_CI_over_time.add(this.happiness);		}
		Neighbourhood.determine_average();
	}
	
	public double get_income() {
		return this.income;
	}
	
	/**
	 * Function that determines the average happiness of the non BEV owners or BEv owners
	 * @param agent
	 * @param evs
	 * @param nonevs
	 * @return average
	 */
	public double get_average(agents agent, boolean evs, boolean nonevs) {
		double ev_happiness 	= 0;
		double non_ev_happiness = 0;
		double nbr_evs 			= 0;
		double nbr_non_evs 		= 0;
		double average 			= 0;
		
		for(Object obj : context.getObjects(agents.class)) {
			if (((agents) obj).car_combustion_type.equals("Electric")) {
				ev_happiness += ((agents) obj).get_happiness();
				nbr_evs ++;
			}
			else {
				non_ev_happiness +=((agents) obj).get_happiness();
				nbr_non_evs ++;
			}
		} 
		if(agent.get_car_combustion_type().equals("Electric")) {
			average = ev_happiness/nbr_evs;
		}
		else {
			average = non_ev_happiness/nbr_non_evs;
		}
		
		if(evs)		return nbr_evs;
		if(nonevs)	return nbr_non_evs;
		return average;
		
	}
	
	/**
	 * Gets the number of bev owners in the simulation
	 * @see {agents#get_average}
	 * @return number of evs
	 */
	public double get_nbr_EV() {
		return get_average(this, true, false);
	}
	
	/**
	 * Gets the total number of non ev owners in the simulation
	 * @see {agents#get_average}
	 * @return total number of gasoline and diesel
	 */
	public double get_nbr_nonEV() {
		return get_average(this, false, true);
	}
	
	/**
	 * Gets the total number of agents in the simulation
	 * @see {agents#get_nbr_EV} and @see {agents#get_nbr_nonEV}
	 * @return total
	 */
	public double get_total_nbr_agents() {
		double evs 		= get_nbr_EV();
		double nonevs 	= get_nbr_nonEV();
		double total	= evs + nonevs;
		
		return total;
	}


	/***GET VALUES FOR FUNCTION GRAPHS*/

	// FINANCIAL
	public double get_financial_satisfaction_agents() {
		if(this instanceof Lease)
			return a_satisfaction_financial_lease;
		if(this instanceof Occasion)
			return a_satisfaction_occasion_financial;
		if(this instanceof Private_new)
			return a_satisfaction_financial;
		return 0;
	}
	
	public double get_financial_uncertainty_agents() {
		if(this instanceof Lease)
			return a_uncertainty_financial_lease;
		if(this instanceof Occasion)
			return a_uncertainty_occasion_financial;
		if(this instanceof Private_new)
			return a_uncertainty_financial;
		return 0;
	}
	
	// SOCIAL
	public double get_social_satisfaction_agents() {
		if(this instanceof Lease)
			return a_satisfaction_social;
		if(this instanceof Occasion)
			return a_satisfaction_occasion_social;
		if(this instanceof Private_new)
			return a_satisfaction_social;
		return 0;
	}
	
	public double get_social_uncertainty_agents() {
		if(this instanceof Lease)
			return a_uncertainty_social;
		if(this instanceof Occasion)
			return a_uncertainty_occasion_social;
		if(this instanceof Private_new)
			return a_uncertainty_social;
		return 0;
	}
	
	//INFRASTRUCTURE
	public double get_infrastructure_satisfaction_agents() {
		if(this instanceof Lease)
			return a_satisfaction_infrastructure;
		if(this instanceof Occasion)
			return a_satisfaction_occasion_infrastructure;
		if(this instanceof Private_new)
			return a_satisfaction_infrastructure;
		return 0;
	}
	
	public double get_infrastructure_uncertainty_agents() {
		if(this instanceof Lease)
			return a_uncertainty_infrastructure;
		if(this instanceof Occasion)
			return a_uncertainty_occasion_infrastructure;
		if(this instanceof Private_new)
			return a_uncertainty_infrastructure;
		return 0;
	}

	
	/*** HYSTOGRAM FUNCTIONS*/

	public void set_hashmap_agents_car_brands(agents agent, vehicle chosen_vehicle) {
		agents_vehicles_hm.put(agent, chosen_vehicle);
	}
	
	public int get_brands_bev_occasions() {
		Iterator<Map.Entry<agents, vehicle>> it1 = agents_vehicles_hm.entrySet().iterator();
		while (it1.hasNext()) {
			Entry<agents, vehicle> mapEntry = it1.next();
			if(mapEntry.getKey().equals(this)) {
				vehicle chosen_vehicle = mapEntry.getValue();
				if(chosen_vehicle instanceof Occasion_EV) {
					if(chosen_vehicle.getBrand().equals("Tesla(model S)")) 			return 0;
					if(chosen_vehicle.getBrand().equals("Volkswagen(e-Golf)"))		return 1;
					if(chosen_vehicle.getBrand().equals("Renault(Zoe)"))			return 2; 
					if(chosen_vehicle.getBrand().equals("BMW(i3)"))					return 3;
					if(chosen_vehicle.getBrand().equals("Nissan(Leaf)"))			return 4;
					if(chosen_vehicle.getBrand().equals("Jaguar(I-Pace)"))			return 5;
					if(chosen_vehicle.getBrand().equals("Toyota(iQ)"))				return 6;
				}
			}
		}
		return 7;
	}
	
	public int get_brands_conventional_occasions() {
		Iterator<Map.Entry<agents, vehicle>> it1 = agents_vehicles_hm.entrySet().iterator();
		while (it1.hasNext()) {
			Entry<agents, vehicle> mapEntry = it1.next();
			if(mapEntry.getKey().equals(this)) {
				vehicle chosen_vehicle = mapEntry.getValue();
				if(chosen_vehicle instanceof Occasion_conventional) {
					if(chosen_vehicle.getBrand().equals("Volkswagen(Golf)")) 	return 0;
					if(chosen_vehicle.getBrand().equals("Audi(A3)")) 			return 1;
					if(chosen_vehicle.getBrand().equals("BMW(3-serie)")) 		return 2;
					if(chosen_vehicle.getBrand().equals("Opel(Astra)")) 		return 3;
				}
			}
		}
		return 4;
	}
	
	public int get_brands_conventional_new() {			
		Iterator<Map.Entry<agents, vehicle>> it1 = agents_vehicles_hm.entrySet().iterator();
		while (it1.hasNext()) {
			Entry<agents, vehicle> mapEntry = it1.next();
			if(mapEntry.getKey().equals(this)) {
				vehicle chosen_vehicle = mapEntry.getValue();
				if(chosen_vehicle instanceof Conventional) {
					System.out.println("Gelukt! Het werkt nu");
					if(chosen_vehicle.getModel().equals("SMALL")) 				return 0;
					if(chosen_vehicle.getModel().equals("MEDIUM")) 				return 1;
					if(chosen_vehicle.getModel().equals("LARGE")) 				return 2;
					if(chosen_vehicle.getModel().equals("EXECUTIVE")) 			return 3;
					if(chosen_vehicle.getModel().equals("LUXURY")) 				return 4;
				} 
			}
		}
	return 5;
	}
	
	public int get_brands_bev_new() {	
		Iterator<Map.Entry<agents, vehicle>> it1 = agents_vehicles_hm.entrySet().iterator();
		while (it1.hasNext()) {
			Entry<agents, vehicle> mapEntry = it1.next();
			if(mapEntry.getKey().equals(this)) {
				vehicle chosen_vehicle = mapEntry.getValue();
				if(chosen_vehicle instanceof EV) {
					if(chosen_vehicle.getBrand().equals("Tesla(model 3)")) 			return 0;
					if(chosen_vehicle.getBrand().equals("Tesla(model S)")) 			return 1;
					if(chosen_vehicle.getBrand().equals("Nissan(Leaf)")) 			return 2;
					if(chosen_vehicle.getBrand().equals("Volkswagen(Golf)")) 		return 3;
					if(chosen_vehicle.getBrand().equals("BMW(i3)")) 				return 4;
					if(chosen_vehicle.getBrand().equals("Hyundai(KONA)")) 			return 5;
					if(chosen_vehicle.getBrand().equals("Renault(ZOE)")) 			return 6;
					if(chosen_vehicle.getBrand().equals("Tesla(Model X)")) 			return 7;
					if(chosen_vehicle.getBrand().equals("Juaguar(i-pace)")) 		return 8;
					if(chosen_vehicle.getBrand().equals("Hyundai(IONIQ)")) 			return 9;
					if(chosen_vehicle.getBrand().equals("Reanult(Twingo ZE)"))		return 10;
					if(chosen_vehicle.getBrand().equals("Volkswage(e-Up)"))			return 11;
					if(chosen_vehicle.getBrand().equals("Skoda(CITIGOeiV)"))		return 12;
				}
			}
		} 	 
	return 10;
	}
	
	public int get_ages_vehicles_in_simulation() {
		int car_age = (current_year - this.car_age);
		return car_age;
	}
	
	public int get_parking_correctly_bev() {
		if(this.parking_home.equals("private") && this.car_combustion_type == "Electric")
			return 0;
		if(this.parking_home.equals("private") && this.car_combustion_type == "Gasoline")
			return 2;
		if(this.car_combustion_type == "Electric") {
			if(bev_owner_found_cs)
				return 0;
			if(!bev_owner_found_cs)
				return 1;
		}
		else {
			if(conventional_owner_parked_correctly)
				return 2;
			if(!conventional_owner_parked_correctly) {
				return 3;
			}
		}
		return -1;
	}

	public int get_cars_bought_year() {
		if(this.a_chosen_vehicle == null)
			return 0;
		else {
			if(this.a_chosen_vehicle instanceof Occasion_EV)			return 1;
			if(this.a_chosen_vehicle instanceof EV)						return 2;
			if(this.a_chosen_vehicle instanceof Occasion_conventional)	return 3;
			if(this.a_chosen_vehicle instanceof Conventional)			return 4;
		}
		return 0;
	}
	
	public int total_EV() {
		
		List<Object> lijst = new ArrayList<Object>();
		
		int ev = 0;
		for(Object obj : context.getObjects(agents.class)) {
			if (((agents) obj).car_combustion_type.equals("Electric")) {
				ev +=1;
				lijst.add(obj);
			}
		}
		return ev;
		
	}
		
	public int total_Diesel() {
		List<Object> lijst = new ArrayList<Object>();
		int diesel = 0;
		for(Object obj : context.getObjects(agents.class)) {
			if (((agents) obj).car_combustion_type.equals("Diesel")) {
				diesel +=1;
				lijst.add(obj);
			}
		}
		return diesel;
	}
		
	public int total_PHEV() {
		int phev = 0;
		for(Object obj : context.getObjects(agents.class)) {
			if (((agents) obj).car_combustion_type.equals("PHEV")) {
				phev +=1;
			}
		}
		return phev;
	}
	
	public int total_Hybrid() {
		int hyb = 0;
		for(Object obj : context.getObjects(agents.class)) {
			if (((agents) obj).car_combustion_type.equals("Hybride")) {
				hyb +=1;
			}
		}
		return hyb;
	}
	
	public int total_Hydrogen() {
		int hyd = 0;
		for(Object obj : context.getObjects(agents.class)) {
			if (((agents) obj).car_combustion_type.equals("Hydrogen")) {
				hyd +=1;
			}
		}
		return hyd;
	}
	
	public int total_Gasoline() {
		int gaso = 0;
		for(Object obj : context.getObjects(agents.class)) {
			if (((agents) obj).car_combustion_type.equals("Gasoline")) {
				gaso +=1;
			}
		}
		return gaso;
	}
	
	public String give_nr_cars() {
		int phev = total_PHEV();
		int hyb = total_Hybrid();
		int hyd = total_Hydrogen();
		int gaso = total_Gasoline();
		int ev = total_EV();
		int dies = total_Diesel();
		String s = 
				" nr of phev: 		"	+ phev 	+
				" nr of hybrid: 	" 	+ hyb 	+
				" nr of hydrogen: 	" 	+ hyd	+
				" nr of gasoline: 	" 	+ gaso	+
				" nr of bev: 		"	+ ev	+
				" nr of diesel: 	"	+ dies
				;
		System.out.println(s);
		return s;
	}
	
	public int getId() {
		return this.id;
	}
	
	public double getIncome() {
		return this.income;
	}
	
	public double getAge() {
		return this.age;
	}

	public String getCarFuel() {
		return this.car_combustion_type;
	}
	
	public String getProffession() {
		return this.a_prof_situation;
	}
	
	public int get_amount_occasions() {
		int nbr = 0;
		for(Object a: context.getObjects(agents.class)) {
			if(a instanceof Occasion)
				nbr++;
		}
		return nbr;
	}
	
	public int get_amount_bev_occasions() {
		int nbr = 0;
		for(Object a: context.getObjects(agents.class)) {
			if(a instanceof Occasion && (((agents) a).get_car_combustion_type()).equals("Electric")) { 
				nbr++;
			}
		}
		return nbr;
	}
	
	public String get_province() {
		return this.province;
	}
	
	public double percentage_bev() {
		double total_ev = total_EV();
		double total_agents = 200;
		double perc_BEV = (total_ev/total_agents)*100;
		return perc_BEV;
	}
	
	public double percentage_ice() {
		double total_ice = total_Gasoline();
		double total_agents = 200;
		double perc_BEV = (total_ice/total_agents)*100;
		return perc_BEV;
	}

	public static Integer findMin(List<Integer> list) 
    { 
        List<Integer> sortedlist = new ArrayList<>(list); 
        Collections.sort(sortedlist); 
        return sortedlist.get(0); 
    } 
  
    public static Integer findMax(List<Integer> list) 
    {  
        List<Integer> sortedlist = new ArrayList<>(list); 
        Collections.sort(sortedlist); 
        return sortedlist.get(sortedlist.size() - 1); 
    } 
	
	//---------------------------------------------------------------
	/*** 					SET FUNCTIONS 							*/
	//---------------------------------------------------------------
	
	public  void setHomeLocation(Residence home) {
		this.home_location = home.getLocation();
	}

	public static void setnbrzero() {
		need_random = 0;
	}


//GET VALUES
	public double get_average_BEV_uncertainty() {
		return get_values_bev_agents(false, true);
	}
	
	public double get_average_BEV_satisfaction() {
		return get_values_bev_agents(true, false);
	}
	
	public double get_values_bev_agents(Boolean sa, Boolean un) {
		double u = 0;
		double s = 0;
		
		for(Object a : context.getObjects(agents.class)) {
			if(((agents) a).getCarFuel().equals("Electric")) {
				s += ((agents) a).a_satisfaction_social;
				u += ((agents) a).a_uncertainty_social;
			}
		}
		if(sa)
			return s/total_EV();
		else 
			return u/total_EV();
		}

}

