package thesis;

import java.awt.Color;

import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;

/**
 * 
 * @author Dani Sibbel
 *
 */
public class ParkingSpaceStyle2D extends DefaultStyleOGL2D {

	/**
	 * Try to override the color of the agents in the grid was not successful
	 */
	@Override
	public Color getColor(Object o) {
		System.out.println("its not working!!!!!!!!!!!!!!!");
		Parking_Space prk = (Parking_Space)o;
		if(prk.getType().equals("without_loadPole"))
			return Color.RED;
		else 
			return Color.GREEN;
	}

}
