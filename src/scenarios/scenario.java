package scenarios;

import java.util.Arrays;
import java.util.List;

import Vehicles.vehicle;
import thesis.EnvironmentBuilder;
import thesis.Neighbourhood;
import thesis.UserPanel;
import thesis.agents;

/**
 * 
 * @author Dani Sibbel
 *
 */

public class scenario {

	public scenario() {
		
	}

	/*** FUNCTION DEFINITION OF THE DIFFERENT SCENARIOS */

	//SUBSIDY
	
	/**
	 * Depending on which subsidy scenario is selected by the user the function will be used in the agents class to influence the agents
	 * @return true or false
	 */
	public static boolean is_subsidy_scenario_selected() {
		String scenario = UserPanel.scenario;
		int[] multi_scenario = UserPanel.multiple_scenarios;
		if(		scenario.equals("Implementeer eindige hoeveelheid subsidy") || 
				scenario.equals("Implement limited amount of subsidy")||
				scenario.equals("Subsidie verminderen tot 0 in 2025") ||
				scenario.equals("Gradually decreasing the BEV subsidy till 0 in 2025" ) ||
			contains(multi_scenario, 0)	 || contains(multi_scenario, 1))
			return true;
		else return false;
	}

	/**
	 * If the limited subsidy scenario is selected by the user the function returns true else it returns false
	 * @return true or false
	 */
	public static boolean is_limited_subsidy_scenario() {
		int[] multi_scenario = UserPanel.multiple_scenarios;
		String scenario = UserPanel.scenario;
		if(contains(multi_scenario, 0) || scenario.equals("Implementeer eindige hoeveelheid subsidy") || scenario.equals("Implement limited amount of subsidy")) {
			return true;
		}
		else return false;
	}
	
	/**
	 * If the decrease subsidy scenario is selected by the user the function will return true
	 * @return true or false
	 */
	public static boolean is_decreased_subsidy_scenario() {
		int[] multi_scenario = UserPanel.multiple_scenarios;
		String scenario = UserPanel.scenario;
		if(contains(multi_scenario, 1) || scenario.equals("Subsidie verminderen tot 0 in 2025") || scenario.equals("Gradually decreasing the BEV subsidy till 0 in 2025")) {
			return true;
		}
		else return false;
	}

	
	//LOADPOLES
	
	/**
	 * If the increased LP scenario is selected by the user the function will return true
	 * @return true or false
	 */
	public static Boolean is_increase_lp_selelected() {
		String scenario = UserPanel.scenario;
		int[] multi_scenario = UserPanel.multiple_scenarios;
		if(scenario.equals("Verhoogde aanbouw laadpalen") || scenario.equals("Increase the rate at which CS are added") || contains(multi_scenario, 4)) {
			return true;
		}
		else return false;
	}
	
	/**
	 * There can be a maximum of 2 agents dependent on one LP
	 * @return 2
	 */
	public static int lp_convergion_rate() {
		return 2; 
	}
	
	//MRB AND BPM
	
	/**
	 * If the MRB and BPM scenraio is selected by the user the function will return true
	 * @return true or false
	 */
	public static boolean is_MRB_BPM_scenario_selected() {
		String scenario = UserPanel.scenario;
		int[] multi_scenario = UserPanel.multiple_scenarios;
		
		if(contains(multi_scenario, 3) || scenario.equals("MRB en BPM voordeel voor BEVs behouden") || scenario.equals("Do not abolish tax exemptions for BEVs"))
			return true;
		else return false;
	}
	
	//TAX INCREASE
	
	/**
	 * If the fuel tax increase scenario is selected by the user the function will return true
	 * @see {Neighbourhood#increase_gasoline_fuel_cost_from_scenario}
	 * @return true or false
	 */
	public static Boolean is_fuel_tax_increase_selected() {
		String scenario = UserPanel.scenario;
		int[] multi_scenario = UserPanel.multiple_scenarios;

		if(contains(multi_scenario, 2) || scenario.equals("Increase taxes for Gasoline vehicles each year") || scenario.equals("Verhoging acijns voor Benzine autos")) {
			Neighbourhood.increase_gasoline_fuel_cost_from_scenario();
			return true;
		}
		else return false;
	}

	
	// BEVS TECHNOLOGY
	
	/**
	 * If the user has selected the technology improvement scenario the function returns true
	 * @return true or false
	 */
	public static boolean is_technology_improvement_selected() {
		String scenario = UserPanel.scenario;
		int[] multi_scenario = UserPanel.multiple_scenarios;
		if(scenario.equals("Each year the range of BEVs increases") ||
				scenario.equals("Elektrische range wordt elk jaar groter") ||
				scenario.equals("Each year BEVs become cheaper") || 
				scenario.equals("Elektrische auto wordt elk jaar goedkoper") ||
				contains(multi_scenario, 5) ||
				contains(multi_scenario, 6))
			return true;
		else return false;
	}
	
	/**
	 * If the range increase scenario is selected by the user the function returns true
	 * @return true or false
	 */
	public static boolean is_range_technological_selected() {
		String scenario = UserPanel.scenario;
		int[] multi_scenario = UserPanel.multiple_scenarios;
		
		if(contains(multi_scenario, 6))
			System.out.println("It worked for: range" );
		if(	scenario.equals( "Each year the range of BEVs increases") ||
			scenario.equals("Elektrische range wordt elk jaar groter") ||
			contains(multi_scenario, 6)) 
			return true;
		else return false;
	}
	
	/**
	 * If the user has selected the price decrease scenario the function returns true
	 * @return true or false
	 */
	public static boolean is_price_technological_selected() {
		String scenario = UserPanel.scenario;
		int[] multi_scenario = UserPanel.multiple_scenarios;
		
		if(contains(multi_scenario, 5))
			System.out.println("It worked for: price"  );
		
		if(	scenario.equals("Each year BEVs become cheaper") || 
			scenario.equals("Elektrische auto wordt elk jaar goedkoper") ||
			contains(multi_scenario, 5))
			return true;
		else return false;
	}
	
	/**
	 * If the scenario n is contained within the list of selected scenarios return true else return false
	 * @param list
	 * @param n
	 * @return true or false
	 */
	public static boolean contains(int[] list,  int n) {
		for(int i = 0; i < list.length; i++) {
			if(list[i] == n) {
				return true;
			}
		}
		return false;
	}
	
	
}

