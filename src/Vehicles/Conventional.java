package Vehicles;

/**
 * 
 * @author Dani Sibbel
 *
 */

public class Conventional extends vehicle{

	private double 	v_range;
	private double 	v_price;
	private double	v_emissions;
	private String	v_fuelType;
	private String	v_type;	
	private double	v_lease_price; 
	
	/**
	 * Initialise Conventional vehicles
	 * @param fuel_type
	 * @param model
	 * @param brand
	 * @param price
	 * @param emissions
	 * @param year
	 * @param range
	 * @param type
	 * @param lease_price
	 */
	public Conventional(String fuel_type, String model, String brand, double price, double emissions, int year, double range, String type, Double lease_price) {
		super(fuel_type, model, brand, price, emissions, year, range, type, lease_price);
		
		v_range 		= range;
		v_price 		= price;
		v_emissions 	= emissions;
		v_fuelType		= "Gasoline";
		v_type			= type;
		v_lease_price 	= lease_price;
	}
	
	/*** GET FUNCTIONS */
	
	public double 	getRange()			{ return v_range; 			}
	public String	getv_fuelType()		{ return v_fuelType;		}
	public double 	getPrice()			{ return v_price; 			}
	public double 	getEmissions()		{ return v_emissions;		}
	public String	getType()			{ return v_type;			}
	public double 	getLeasePrice() 	{ return v_lease_price;		}
}

