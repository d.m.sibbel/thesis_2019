package Vehicles;

import thesis.Neighbourhood;

/**
 * 
 * @author Dani Sibbel
 *
 */

public class Occasion_conventional extends vehicle{
	private double	v_emissions;
	private double 	v_km;
	private double 	v_price;
	private int 	v_year;
	private String	v_brand;
	private String	v_fuel_type;
	private double	v_range;
	private String	v_type;
	private double  v_lease_price;
	
	/**
	 * Initialise occasion ICEV
	 * @param fuel_type
	 * @param model
	 * @param brand
	 * @param year
	 * @param price
	 * @param km
	 * @param emissions
	 * @param range
	 * @param type
	 * @param lease_price
	 */
	public Occasion_conventional(String fuel_type, String model, String brand, int year, double price, double km, double emissions, double range, String type, double lease_price) {

		
		super(fuel_type, model, brand, price, emissions, year, range, type, lease_price);


		v_emissions 	= emissions;
		v_km 			= km;
		v_price 		= price;
		v_year 			= year;
		v_brand 		= brand;
		v_fuel_type		= "Gasoline";
		v_range			= range;
		v_type			= type;
		v_lease_price	= lease_price;
		}
		
	/*** GET FUNCTIONS */
		public double 	getEmissions()			{ return v_emissions; 							}
		public double 	getKm()					{ return v_km; 									}
		public double 	getPrice()				{ return v_price; 								}
		public int 		getYear()				{ return v_year + Neighbourhood.year; 			}
		public String	getBrand()				{ return v_brand;								}
		public String	get_FuelType()			{ return v_fuel_type;							}
		public double	getRange()				{ return v_range;								}
		public String	getType()				{ return v_type;								}
		public double	getLeasePrice()			{ return v_lease_price;							}


}
