package Vehicles;

/**
 * 
 * @author Dani Sibbel
 *
 */

public class EV extends vehicle{

	private int 	v_charging_time;
	private double 	v_range;
	private String 	v_brand;
	private String 	v_model;
	private int 	v_year;
	private double 	v_price;
	private double 	v_emissions;
	private String 	v_fuelType;
	private String	v_type;
	private double v_lease_price;
	private double initial_price;
	private double initial_lease_price;
	private double initial_range;
	
	/**
	 * Initialise BEVs
	 * @param fuel_type
	 * @param model
	 * @param brand
	 * @param price
	 * @param emissions
	 * @param charging_time
	 * @param year
	 * @param range
	 * @param type
	 * @param lease_price
	 */
	public EV(String fuel_type, String model, String brand, double price, double emissions, int charging_time, int year, double range, String type, Double lease_price) {
		super(fuel_type, model, brand, price, emissions, year, range, type, lease_price);
		
		v_charging_time 	= charging_time;
		v_model				= model;
		v_range				= range;
		v_brand				= brand;
		v_price 			= price;
		v_fuelType			= "Electric";
		v_type				= type;
		v_lease_price		= lease_price;
		initial_price		= price;
		initial_range		= range;
		initial_lease_price = lease_price;
	}

	/*** GET FUNCTIONS */
	public int 		getCharging_time()		{ return v_charging_time; 	}
	public String	getFuelType()			{ return v_fuelType;		}
	public double 	getRange()				{ return v_range; 			}
	public String 	getBrand()				{ return v_brand; 			}
	public String 	getModel()				{ return v_model; 			}
	public double 	getPrice()				{ return v_price; 			}
	public double 	getEmissions()			{ return v_emissions;		}
	public String	getType()				{ return v_type;			}
	public double 	getLeasePrice() 		{ return v_lease_price;		}
	
	/*** UPDATE FUNCTIONS FROM SCENARIO*/
	
	public void updatePrice(double year) {
		double limit = 0.6 * initial_price;
		double limit_lease = 0.6 * initial_lease_price;
		if((v_price >= limit ) && year != 0) {
			v_price -= (0.03 * initial_price);
		}
		if((v_lease_price >= limit_lease ) && year != 0) {
			v_lease_price -= (0.03 * initial_lease_price);
		}
	}

	public void updateRange(double year) {
		if(year != 0) {
			v_range += 0.07 * v_range;
		}
		
	}
}
