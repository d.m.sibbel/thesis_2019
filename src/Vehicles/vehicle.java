package Vehicles;

import thesis.Neighbourhood;

/**
 * @author Dani Sibbel
 */

public class vehicle {
	
	/*** VEHICLE CHARACTERISTTICS */
	
	protected String v_model;
	protected String v_brand;
	protected double v_price;
	protected double v_emissions;
	protected String v_type;
	protected int	 v_year;
	protected double v_range;
	protected double v_lease_price;

	/**
	 * Construct the vehicles from data
	 * @param fuel_type
	 * @param model
	 * @param brand
	 * @param price
	 * @param emissions
	 * @param year
	 * @param range
	 * @param type
	 * @param lease_price
	 */
	public vehicle(String fuel_type,  String model, String brand, double price, double emissions, int year, double range, String type, Double lease_price) {
		v_type 			= fuel_type;
		v_model 		= model;
		v_price 		= price;
		v_emissions 	= emissions;
		v_year			= year + Neighbourhood.year;
		v_brand			= brand;
		v_range			= range;
		v_type			= type;
		v_lease_price 	= lease_price;
		//v_OwnershipType = ownershipType;
	}

	/*** VEHICLE RETRIEVE FUNCTIONS */
	
	public double getRange() 	{ return v_range;	}
	public String getType()		{ return v_type;	}
	
	public String 	getFuel_type()		{
		if(v_type.equals("ELECTRIC") || v_type.equals("ELECTRIC_OCCASION"))
			return "Electric";
		else return "Gasoline";			
	}
	public String 	getModel()			{ return v_model; 							}
	public String 	getBrand()			{ return v_brand; 							}
	public double 	getPrice()			{ return v_price; 							}
	public double 	getEmissions()		{ return v_emissions; 						}
	public int 		getYear()			{ return (v_year + Neighbourhood.year );	}
	public int 		getRanking()		{ 
		if (v_brand.equals("Tesla(model S)"))		return 1; 
		if (v_brand.equals("Volkswagen(e-Golf)"))	return 2; 
		if (v_brand.equals("Renault(Zoe)"))			return 3;
		if (v_brand.equals("BMW(i3)"))				return 4;
		if (v_brand.equals("Nissan(Leaf)"))			return 5;
		if (v_brand.equals("Jaguar(I-Pace)"))		return 6;
		if (v_brand.equals("Toyota(iQ)"))			return 7;
		if( v_brand.equals("UNKNOWN") && v_model.equals("LUXURY"))				return 1;
		if( v_brand.equals("UNKNOWN") && v_model.equals("EXECUTIVE"))			return 2; 
		if( v_brand.equals("UNKNOWN") && v_model.equals("LARGE"))				return 3; 
		if( v_brand.equals("UNKNOWN") && v_model.equals("MEDIUM"))				return 4; 
		if( v_brand.equals("UNKNOWN") && v_model.equals("SMALL"))				return 5;
		if (v_brand.equals("Volkswagen(Golf)"))			return 1;
		if (v_brand.equals("Audi(A3)"))					return 2;
		if (v_brand.equals("BMW(3-serie)"))				return 3;
		if (v_brand.equals("Opel(Astra)"))				return 4;
		return 0;
	}
	public double 	getLeasePrice() 	{	return v_lease_price;	}
	

}
