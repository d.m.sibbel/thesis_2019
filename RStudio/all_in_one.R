################### Percentage BEVs#########################

lijst <- seq(0,1092,by=52)
lent <- length(lijst)

color <- c("Forest Green")
segments <- c("BEV")

y <- read.csv("C:/Users/danis/git/thesis_2019/Output/Percentage_BEV")

p <- qplot(y[,2],y[,1], geom='smooth', span =0.5, main = "Percentage BEV owners in simulation")
p + scale_x_continuous(name="years", breaks = lijst, labels = c(1:lent)) + scale_y_continuous(name = "percentage")+ theme_bw()+
  theme(text = element_text(size=15))

################### Percentage BEVs per segment#########################

y <- read.csv("C:/Users/danis/git/thesis_2019/Output/Percentage_BEV_each_segment")
p <- ggplot(y) +
  ggtitle("Percentage BEV owners in each segment") +
  geom_line(aes(x = y[,1], y = y[,4], group = 1, color = "Navy"), lwd = 2) +
  geom_line(aes(x = y[,1], y = y[,3], color = "Forest Green"), lwd = 2) +
  geom_line(aes(x = y[,1], y = y[,2], color = "Dark Red"), lwd = 2) +
  scale_x_continuous(name="Years", breaks = lijst, labels = c(1:lent)) + scale_y_continuous(name = "Percentage")+ theme_bw() +
  scale_colour_manual(name = "labels",   values = c("Navy", "Forest Green","Dark Red" ), labels = c("New", "Occasion", "Lease")) +
  theme(text = element_text(size=15))
p




################## SUBSIDY USED ######################
library(ggplot2)
lijst <- seq(0,1092,by=52)
lent <- length(lijst)

kleur <- c("Navy", "Dark Red", "Orange")
segments <- c("Occasion", "New", "Lease")



subsidy <- read.csv("C:/Users/danis/git/thesis_2019/Output/SubsidyUsed")
subsidy_altered <- subsidy[,-1]
sub <- cbind(subsidy_altered, total = rowSums(subsidy_altered))
total <- as.data.frame(cbind(subsidy[,1],sub[,4]))
#total <- total[1092]

p <- ggplot(subsidy) +
  ggtitle("Amount of Subsidy used by the agents in the simulation") +
  geom_line(aes(x = subsidy[,1], y = subsidy[,2], group = 1, color = kleur[2]), lwd = 2) +
  geom_line(aes(x = subsidy[,1], y = subsidy[,3], color = kleur[1]), lwd = 2) +
  geom_line(aes(x = subsidy[,1], y = subsidy[,4], color = kleur[3]), lwd = 2) +
  scale_x_continuous(name="years", breaks = lijst, labels = c(1:lent)) + scale_y_continuous(name = "Amount of subsidy")+ theme_bw() +
  scale_colour_manual(name = "labels",   values = kleur, labels = segments) +
  theme(text = element_text(size=15))
p



###########plot complete subsidy
p <- ggplot(total) +
  ggtitle("Amount of Subsidy used by the agents in the simulation") +
  geom_line(aes(x = total[,1], y = total[,2], group = 1), lwd = 2, colour = "Navy") +
  scale_x_continuous(name="years", breaks = lijst, labels = c(1:lent)) + scale_y_continuous(name = "Amount of subsidy")+ theme_bw() +
  theme(text = element_text(size=15))
p
################## Agent Values ######################
lijst <- seq(0,1092,by=52)
lent <- length(lijst)

color <- c("Red", "Dark Red","Lawn Green", "Dark Green", "Dodger Blue", "Navy")
values_names <- c("Satisfaction Financial", "Uncertainty Financial", "Satisfaction Social", "Uncertainty Social", "Satisfaction Infrastructure", "Uncertainty Infrastructure")
names <- c("Satisfaction", "Uncertainty")


color_satisfaction <-c ("Red","Lawn Green", "Dodger Blue")
color_uncertainty <-c ( "Dark Red", "Dark Green",  "Navy")

values <- read.csv(file = "C:/Users/danis/git/thesis_2019/Output/Values_agents" )#, nrows=200




par(mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)
kleinste_getal = min(min(values[,3]), min(values[,4]), min(values[,5]), min(values[,6]), min(values[,7]), min(values[,8]))
grootste_getal = max(max(values[,3]),max(values[,4]), max(values[,5]), max(values[,6]), max(values[,7]), max(values[,8]))


plot( values[,21],values[,3],type="l",ylim = c(kleinste_getal,grootste_getal),col = color[1], lwd = 3,
      xaxt = "n",
      xlab = "years",
      ylab = "Values of the agents",
      main = "Values of the agents with Occasion vehicle"
)
lines(values[,21],values[,4],type="l",col = color[2], lwd = 3)
lines(values[,21],values[,5],type="l",col = color[3], lwd = 3)
lines(values[,21],values[,6],type="l",col = color[4], lwd = 3)
lines(values[,21],values[,7],type="l",col = color[5], lwd = 3)
lines(values[,21],values[,8],type="l",col = color[6], lwd = 3)

legend("topright", inset = c(-0.3,0), legend=names, fill=color[1:2], title="Financial")
legend("bottomright", inset = c(-0.3,0), legend=names, fill=color[3:4], title="Infrastructure")
legend("right", inset = c(-0.3,0), legend=names, fill=color[5:6], title="Social")

axis(1, at=lijst, labels=c(1:lent))


par(mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)
kleinste_getal = min(min(min(values[,9]), values[,10]), min(values[,11]), min(values[,12]), min(values[,13]), min(values[,14]))
grootste_getal = max(max(values[,9]), max(values[,10]), max(values[,11]), max(values[,12]), max(values[,13]), max(values[,14]))

plot( values[,21],values[,9],type="l",ylim = c(kleinste_getal,grootste_getal),col = color[1], lwd = 3,
      xaxt = "n",
      xlab = "years",
      ylab = "Values of the agents",
      main = "Values of the agents with New bought vehicle"
)
lines(values[,21],values[,10],type="l",col = color[2], lwd = 3)
lines(values[,21],values[,11],type="l",col = color[3], lwd = 3)
lines(values[,21],values[,12],type="l",col = color[4], lwd = 3)
lines(values[,21],values[,13],type="l",col = color[5], lwd = 3)
lines(values[,21],values[,14],type="l",col = color[6], lwd = 3)

legend("topright", inset = c(-0.3,0), legend=names, fill=color[1:2], title="Financial")
legend("bottomright", inset = c(-0.3,0), legend=names, fill=color[3:4], title="Infrastructure")
legend("right", inset = c(-0.3,0), legend=names, fill=color[5:6], title="Social")

axis(1, at=lijst, labels=c(1:lent))
