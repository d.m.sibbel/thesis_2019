################## Agent Values ######################
lijst <- seq(0,1092,by=52)
lent <- length(lijst)

color <- c("Red", "Dark Red","Lawn Green", "Dark Green", "Dodger Blue", "Navy")
values_names <- c("Satisfaction Financial", "Uncertainty Financial", "Satisfaction Social", "Uncertainty Social", "Satisfaction Infrastructure", "Uncertainty Infrastructure")
names <- c("Satisfaction", "Uncertainty")


color_satisfaction <-c ("Red","Lawn Green", "Dodger Blue")
color_uncertainty <-c ( "Dark Red", "Dark Green",  "Navy")

values <- read.csv(file = "C:/Users/danis/git/thesis_2019/Output/Values_agents" )#, nrows=200


#LEASE ####
names <- c("Satisfaction", "Uncertainty")
#kleinste_getal = min(min(values[,1]), min(values[,2]), min(values[,9]), min(values[,10]), min(values[,11]), min(values[,12]))
#grootste_getal = max(max(values[,1]), max(values[,2]), max(values[,9]), max(values[,10]), max(values[,11]), max(values[,12]))
kleinste_getal = min(min(values[,1]), min(values[,2]))
grootste_getal = max(max(values[,1]), max(values[,2]))


par(mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)

plot( values[,21],values[,1],type="l",ylim = c(kleinste_getal,grootste_getal),col = color[1], lwd = 5,
      xaxt = "n",
      xlab = "years",
      ylab = "Values of the agents",
      main = "Values of the agents with Leased vehicle"
)
lines(values[,21],values[,2],type="l",col = color[2], lwd = 5)
#lines(values[,21],values[,9],type="l",col = color[3], lwd = 5)
#lines(values[,21],values[,10],type="l",col = color[4], lwd = 5)
#lines(values[,21],values[,11],type="l",col = color[5], lwd = 5)
#lines(values[,21],values[,12],type="l",col = color[6], lwd = 5)

legend("topright", inset = c(-0.3,0), legend=names, fill=color[1:2], title="Financial")
#legend("bottomright", inset = c(-0.3,0), legend=names, fill=color[3:4], title="Infrastructure")
legend("right", inset = c(-0.3,0), legend=names, fill=color[5:6], title="Social")

axis(1, at=lijst, labels=c(1:lent))

#OCCASION ####
par(mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)
#kleinste_getal = min(min(values[,3]), min(values[,4]), min(values[,7]), min(values[,8]))
#grootste_getal = max(max(values[,3]),max(values[,4]), max(values[,7]), max(values[,8]))
#kleinste_getal = min(min(values[,3]), min(values[,4]), min(values[,5]), min(values[,6]), min(values[,7]), min(values[,8]))
#grootste_getal = max(max(values[,3]),max(values[,4]), max(values[,5]), max(values[,6]), max(values[,7]), max(values[,8]))

kleinste_getal = min(min(values[,3]), min(values[,4]))
grootste_getal = max(max(values[,3]),max(values[,4]))


plot( values[,21],values[,3],type="l",ylim = c(kleinste_getal,grootste_getal),col = color[1], lwd = 3,
      xaxt = "n",
      xlab = "years",
      ylab = "Values of the agents",
      main = "Values of the agents with Occasion vehicle"
)
lines(values[,21],values[,4],type="l",col = color[2], lwd = 3)
#lines(values[,21],values[,5],type="l",col = color[3], lwd = 3)
#lines(values[,21],values[,6],type="l",col = color[4], lwd = 3)
#lines(values[,21],values[,7],type="l",col = color[5], lwd = 3)
#lines(values[,21],values[,8],type="l",col = color[6], lwd = 3)

legend("topright", inset = c(-0.3,0), legend=names, fill=color[1:2], title="Financial")
#legend("bottomright", inset = c(-0.3,0), legend=names, fill=color[3:4], title="Infrastructure")
#legend("right", inset = c(-0.3,0), legend=names, fill=color[5:6], title="Social")

axis(1, at=lijst, labels=c(1:lent))

#NEW####
par(mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)


#kleinste_getal = min(min(min(values[,9]), values[,10]))
#grootste_getal = max(max(values[,9]), max(values[,10]))

#kleinste_getal = min(min(min(values[,9]), values[,10]), min(values[,13]), min(values[,14]))
#grootste_getal = max(max(values[,9]), max(values[,10]), max(values[,13]), max(values[,14]))
kleinste_getal = min(min(min(values[,9]), values[,10]), min(values[,11]), min(values[,12]), min(values[,13]), min(values[,14]))
grootste_getal = max(max(values[,9]), max(values[,10]), max(values[,11]), max(values[,12]), max(values[,13]), max(values[,14]))

plot( values[,21],values[,9],type="l",ylim = c(kleinste_getal,grootste_getal),col = color[1], lwd = 3, 
      xaxt = "n",
      xlab = "years",
      ylab = "Values of the agents",
      main = "Values of the agents with New bought vehicle"
)
lines(values[,21],values[,10],type="l",col = color[2], lwd = 3)
lines(values[,21],values[,11],type="l",col = color[5], lwd = 3)
lines(values[,21],values[,12],type="l",col = color[6], lwd = 3)
lines(values[,21],values[,13],type="l",col = color[3], lwd = 3)
lines(values[,21],values[,14],type="l",col = color[4], lwd = 3)

legend("topright", inset = c(-0.3,0), legend=names, fill=color[1:2], title="Financial")
legend("bottomright", inset = c(-0.3,0), legend=names, fill=color[3:4], title="Infrastructure")
legend("right", inset = c(-0.3,0), legend=names, fill=color[5:6], title="Social")

axis(1, at=lijst, labels=c(1:lent))


################## Agent Values SEPARATED!!!!!!!!!!!!!!!!!!!!!######################
lijst <- seq(0,1092,by=52)
lent <- length(lijst)

kleur <- c("Red", "Dark Red","Lawn Green", "Dark Green", "Dodger Blue", "Navy")
values_names <- c("Satisfaction Financial", "Uncertainty Financial", "Satisfaction Social", "Uncertainty Social", "Satisfaction Infrastructure", "Uncertainty Infrastructure")
names <- c("Satisfaction", "Uncertainty")

values <- read.csv(file = "C:/Users/danis/git/thesis_2019/Output/Values_agents" )#, nrows=200


#LEASE ####

kleinste_getal = min(min(values[,1]), min(values[,2]))
grootste_getal = max(max(values[,1]), max(values[,2]))

#financial
par(mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)

plot( values[,21],values[,1],type="l",ylim = c(kleinste_getal,grootste_getal),col = kleur[1], lwd = 5,
      xaxt = "n",
      xlab = "years",
      ylab = "Values of the agents",
      main = "Financial Values of the Lease agents"
)
lines(values[,21],values[,2],type="l",col = kleur[2], lwd = 5)


legend("topright", 
       inset = c(-0.3,0), legend=names, fill=kleur[1:2], title="Financial")
       

axis(1, at=lijst, labels=c(1:lent))


#OCCASION ####

#financial
par(mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)
kleinste_getal_1 = min(min(values[,3]), min(values[,4]))
grootste_getal_1 = max(max(values[,3]), max(values[,4]))
kleinste_getal_2 = min(min(values[,5]), min(values[,6]))
grootste_getal_2 = max(max(values[,5]), max(values[,6]))
kleinste_getal_3 = min(min(values[,7]), min(values[,8]))
grootste_getal_3 = max(max(values[,7]), max(values[,8]))

plot( values[,21],values[,3],type="l",ylim = c(kleinste_getal_1,grootste_getal_1),col = kleur[1], lwd = 5,
      xaxt = "n",
      xlab = "years",
      ylab = "Values of the agents",
      main = "Financial Values of the Occasion agents"
)
lines(values[,21],values[,4],type="l",col = kleur[2], lwd = 5)


legend("topright", 
       inset = c(-0.3,0), legend=names, fill=kleur[1:2], title="Financial")


axis(1, at=lijst, labels=c(1:lent))

#Infrastructure
par(mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)

plot( values[,21],values[,5],type="l",ylim = c(kleinste_getal_2,grootste_getal_2),col = color[5], lwd = 3,
      xaxt = "n",
      xlab = "years",
      ylab = "Values of the agents",
      main = "Infrastructure Values of the Occasion agents"
)
lines(values[,21],values[,6],type="l",col = kleur[6], lwd = 3)

legend("topright", inset = c(-0.3,0), legend=names, fill=kleur[5:6], title="Infrastructure")

axis(1, at=lijst, labels=c(1:lent))

#Social
par(mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)

plot( values[,21],values[,7],type="l",ylim = c(kleinste_getal_3,grootste_getal_3),col = kleur[3], lwd = 3,
      xaxt = "n",
      xlab = "years",
      ylab = "Values of the agents",
      main = "Social Values of the Occasion agents"
)
lines(values[,21],values[,8],type="l",col = kleur[4], lwd = 3)

legend("topright", inset = c(-0.3,0), legend=names, fill=kleur[3:4], title="Social")

axis(1, at=lijst, labels=c(1:lent))

#NEW####
kleinste_getal_1 = min(min(values[,9]), min(values[,10]))
grootste_getal_1 = max(max(values[,9]), max(values[,10]))
kleinste_getal_2 = min(min(values[,11]), min(values[,12]))
grootste_getal_2 = max(max(values[,11]), max(values[,12]))
kleinste_getal_3 = min(min(values[,13]), min(values[,14]))
grootste_getal_3 = max(max(values[,13]), max(values[,14]))
#financial
par(mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)

plot( values[,21],values[,9],type="l",ylim = c(kleinste_getal_1,grootste_getal_1),col = kleur[1], lwd = 5,
      xaxt = "n",
      xlab = "years",
      ylab = "Values of the agents",
      main = "Financial Values of the Occasion agents"
)
lines(values[,21],values[,10],type="l",col = kleur[2], lwd = 5)


legend("topright", 
       inset = c(-0.3,0), legend=names, fill=kleur[1:2], title="Financial")


axis(1, at=lijst, labels=c(1:lent))

#Social
par(mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)

plot( values[,21],values[,11],type="l",ylim = c(kleinste_getal_2,grootste_getal_2),col = kleur[5], lwd = 3,
      xaxt = "n",
      xlab = "years",
      ylab = "Values of the agents",
      main = "Social Values of the New agents"
)
lines(values[,21],values[,12],type="l",col = kleur[6], lwd = 3)

legend("topright", inset = c(-0.3,0), legend=names, fill=kleur[5:6], title="Social")

axis(1, at=lijst, labels=c(1:lent))

#Infrastructure
par(mar=c(5.1, 4.1, 4.1, 8.1), xpd=TRUE)

plot( values[,21],values[,13],type="l",ylim = c(kleinste_getal_3,grootste_getal_3),col = kleur[3], lwd = 3,
      xaxt = "n",
      xlab = "years",
      ylab = "Values of the agents",
      main = "Infrastructure Values of the New agents"
)
lines(values[,21],values[,14],type="l",col = kleur[4], lwd = 3)

legend("topright", inset = c(-0.3,0), legend=names, fill=kleur[3:4], title="Infrastructure")

axis(1, at=lijst, labels=c(1:lent))


      




